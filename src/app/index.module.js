(function() {
  'use strict';

  angular
    .module('app', [
      'ngAnimate', 
      'ngCookies', 
      'ngTouch', 
      'ngSanitize', 
      'ngMessages', 
      'ngAria', 
      'ui.router', 
      'toastr',
      'ui.bootstrap',
      'vcRecaptcha',
      'ngStorage',
      'mgo-angular-wizard',
      'moment-picker',
      'darthwade.dwLoading',
      'validation.match',
      'jlareau.bowser',
      'angularjs-dropdown-multiselect'
    ]);

})();
