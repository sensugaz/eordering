(function() {
  'use strict';

  angular
    .module('app')
    .controller('NonePasswordController', NonePasswordController);

  /** @ngInject */
  function NonePasswordController($scope, $uibModalInstance, AuthService) {
    init();

    function init() {
      /**
       * define
       */
      $scope.btnLoading = false;
      $scope.input = {};
    }

    $scope.close = function() {
      $uibModalInstance.dismiss('cancel');     
    };

    $scope.onChangePassword = function(isValid) {
      if (isValid) {
        $scope.btnLoading = true;

        var formData = {
          userName: $scope.input.userName,
          password: '',
          email: '',
          newPassword: $scope.input.password,
          resetPasswordKey: ''
        };

        AuthService.changePassword(formData)
          .then(function(res) {
            if (res.data.result == 'SUCCESS') {
              swal('สำเร็จ', 'กรุณาตรวจสอบอีเมลของท่าน', 'success');
              $scope.close();
            } else {
              switch (res.data.validateCode) {
                case '001': 
                  swal('ไม่สำเร็จ', 'ชื่อผู้ใช้งานไม่ถูกต้อง', 'warning');
                  break;
                case '002': 
                  swal('ไม่สำเร็จ', 'รหัสผ่านไม่ถูกต้อง', 'warning');
                  break;
                case '003': 
                  swal('ไม่สำเร็จ', 'อีเมลไม่ถูกต้อง', 'warning');
                  break;
                case '004': 
                  swal('ไม่สำเร็จ', 'ชื่อผู้ใช้งานหรืออีเมลไม่ถูกต้อง', 'warning');
                  break;
                case '009': 
                  swal('เกิดข้อผิดผิดพลาด', res.data.validateCode, 'error');
                  break;
              }
            }

            $scope.btnLoading = true;
            clearForm();
          });
      }
    };

    /**
     * Functions
     */
    function clearForm() {
      $scope.input = {};
      $scope.form.$setPristine();
    }
  }

}());
