(function() {
  'use strict';

  angular
    .module('app')
    .controller('DetectBowserController', DetectBowserController);

  /** @ngInject */
  function DetectBowserController($scope, $uibModalInstance) {
    init();

    function init() {
      var cpuClass = window.navigator.cpuClass;

      $scope.cpuClass = '';

      if (cpuClass != undefined) {
        $scope.cpuCass = cpuClass;
      }
    }

    $scope.close = function() {
      $uibModalInstance.dismiss('cancel');     
    };
  }

}());
