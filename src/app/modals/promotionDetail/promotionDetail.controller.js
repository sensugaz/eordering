(function() {
  'use strict';

  angular
    .module('app')
    .controller('PromotionDetailController', PromotionDetailController);

  /** @ngInject */
  function PromotionDetailController($scope, $loading, $localStorage, $uibModalInstance, btfInfo, PromotionService) {
    init();

    function init() {
      /** define */
      $scope.btfInfo = btfInfo;
      $scope.customerInfo = $localStorage.customerInfo;
      $scope.promotions = [];
      
      $loading.setDefaultOptions({ 
        text: 'กำลังโหลด'
      });
      
      fetchPromotion();
    }

    $scope.close = function() {
      $uibModalInstance.dismiss('cancel');     
    };

    /**
     * Functions
     */
    function fetchPromotion() {
      $loading.start('promotion');

      PromotionService.getPromotionInBTF($scope.customerInfo.customerId, btfInfo.btf)
        .then(function(res) {
          if (res.data.result == 'SUCCESS') {
            $scope.promotions = res.data.data.promotionHDList;
          }
        })
        .finally(function() {
          $loading.finish('promotion');
        });
    }
  }

}());
