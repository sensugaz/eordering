(function () {
  'use strict';

  angular
    .module('app')
    .controller('OrderBillingController', OrderBillingController);

  /** @ngInject */
  function OrderBillingController($scope, orderId, saleOrderNumber, $localStorage, $uibModalInstance, $loading, OrderService, REPORT) {

    init();

    function init() {
      /** define */
      $scope.customerInfo = $localStorage.customerInfo;
      
      $scope.order = {};
      $scope.noBill = [];
      $scope.haveBill = [];

      $scope.ItemAll = [];

      fetchOrder();
    }
    
    $scope.close = function () {
      $uibModalInstance.dismiss('cancel');
    };

    $scope.toPrint = function() {
      window.open(REPORT + 'order/billing?orderId=' + $scope.order.id + '&saleOrderNumber=' + $scope.order.salesDocument, '_blank');
    };

    function fetchOrder() {
      $loading.start('orderInfo');
      $loading.start('orderPrecessInfo');
      
      OrderService.getOrderInfo(orderId)
        .then(function(res) {
          if (res.data.result == 'SUCCESS') {
            $scope.order = res.data.data.order;
            $scope.order.salesDocument = saleOrderNumber;
          }
        })
        .finally(function() {
          $loading.finish('orderInfo');
        });
      
      if (saleOrderNumber) {
        OrderService.getOrderProcessTracking(saleOrderNumber)
          .then(function(res) {
            if (res.data.result == 'SUCCESS') {
              var detail = res.data.data.orderProcessOrderItemList;
              var orderProcessShipmentList = res.data.data.orderProcessShipmentList;
            
              $scope.detail = detail;
              $scope.orderProcessShipmentList = orderProcessShipmentList;
           
              $scope.detail.forEach(function(value, key) {
                var arr = {
                  balaQty: value.balaQty,
                  billQty: value.billQty,
                  deliQty: value.deliQty,
                  freeGoods: value.freeGoods,
                  itmNumber: value.itmNumber,
                  material: value.material,
                  materialDes: value.materialDes,
                  netwr2: value.netwr2,
                  rejeQty: value.rejeQty,
                  salesdocument: value.salesdocument,
                  targetQty: value.targetQty,
                  unit: value.unit,
    
                  billDate: orderProcessShipmentList[key].billDate,
                  billNo: orderProcessShipmentList[key].billNo,
                  custRecDate: orderProcessShipmentList[key].custRecDate,
                  custRecTime: orderProcessShipmentList[key].custRecTime,
                  driveName: orderProcessShipmentList[key].driveName,
                  foragt: orderProcessShipmentList[key].foragt,
                  license: orderProcessShipmentList[key].license,
                  runno: orderProcessShipmentList[key].runno,
                  shipmentDoc: orderProcessShipmentList[key].shipmentDoc,
                  startDat: orderProcessShipmentList[key].startDat,
                  startTime: orderProcessShipmentList[key].startTime,
                    telDrive: orderProcessShipmentList[key].telDrive
                }

                $scope.ItemAll.push(arr);
              });

              $scope.ItemAll.forEach(function(value) {
                var arr = {
                  balaQty: value.balaQty,
                  billQty: value.billQty,
                  deliQty: value.deliQty,
                  freeGoods: value.freeGoods,
                  itmNumber: value.itmNumber,
                  material: value.material,
                  materialDes: value.materialDes,
                  netwr2: value.netwr2,
                  rejeQty: value.rejeQty,
                  salesdocument: value.salesdocument,
                  targetQty: value.targetQty,
                  unit: value.unit,
    
                  billDate: value.billDate,
                  billNo: value.billNo,
                  custRecDate: value.custRecDate,
                  custRecTime: value.custRecTime,
                  driveName: value.driveName,
                  foragt: value.foragt,
                  license: value.license,
                  runno: value.runno,
                  shipmentDoc: value.shipmentDoc,
                  startDat: value.startDat,
                  startTime: value.startTime,
                  telDrive: value.telDrive
                };


                if (value.billNo === '') {
                  $scope.noBill.push(arr);
                } else {
                  $scope.haveBill.push(arr);
      
                  if (value.targetQty - value.billQty > 0) {
                    arr.billQty = value.targetQty - value.billQty;
                    $scope.noBill.push(arr);
                  }
                }
              });
            }
          })
          .finally(function() {
            $loading.finish('orderPrecessInfo');
          });  
      } else {
        $loading.finish('orderPrecessInfo');
      }
    }
  }

}());
