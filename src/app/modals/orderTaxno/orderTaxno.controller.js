(function () {
  'use strict';

  angular
    .module('app')
    .controller('OrderTaxnoController', OrderTaxnoController)

  /** @ngInject */
  function OrderTaxnoController($scope, $loading, $filter, OrderService, orderId, billNo, saleOrderNumber, $uibModalInstance) {

    init();

    function init() {
      $loading.setDefaultOptions({
        text: 'กำลังโหลด'
      });

      $scope.discounts = [];

      fetchOrder();
    }

    $scope.close = function () {
      $uibModalInstance.dismiss('cancel');
    };

    function fetchOrder() {
      $loading.start('orderInfo');
      $loading.start('orderHistory');

      OrderService.getOrderInfo(orderId)
        .then(function (res) {
          if (res.data.result == 'SUCCESS') {
            $scope.order = res.data.data.order;
          }
        })
        .finally(function () {
          $loading.finish('orderInfo');
        });

      OrderService.getOrderPrecessInfo(saleOrderNumber)
        .then(function (res) {
          if (res.data.result == 'SUCCESS') {
            var orderProcessInfo = res.data.data.orderProcessInfo;
            //orderProcessItemList = res.data.data.orderProcessItemList;
            $scope.order.salesDocument = orderProcessInfo.salesDocument;
            
            $scope.sumAmount = orderProcessInfo.sumAmount;
          }
        });

      OrderService.getOrderHistory(saleOrderNumber)
        .then(function (res) {
          if (res.data.result == 'SUCCESS') {
            var head = $filter('filter')(res.data.data.orderHistoryHeaderList, function(item) {
              return item.billno == billNo;
            })[0];

            var detail = res.data.data.orderHistoryDetailList;
            var discountList = res.data.data.prderHistoryDiscountList;

            $scope.total = {};

            //console.log(head);
            $scope.order.purchNoC = head.purchNoC;
            $scope.order.pmnttrms = head.pmnttrms;
            $scope.order.taxNum = head.taxNum;
            $scope.order.billno = head.billno;

            $scope.total.headNetwr2 = head.headNetwr2;
            $scope.total.headVat = head.headVat;

            $scope.totalSumManual = 0.0;
            $scope.totalQty;

            detail.forEach(function (value) {
              $scope.totalSumManual += value.amount;
            });

            // var temp = [];
            // var stringTemp = '';

            // orderProcessItemList.forEach(function (value, key) {
            //   //detail[key].discount = value.discount;
            //   // stringTemp = value.discount;

            //   // temp = value.discount.split('/');

            //   // if (temp.length == 4) {
            //   //   stringTemp = temp[0] + ' / ' + temp[1] + ' / ' + temp[2]
            //   //     + '/ <br>' + temp[3];
            //   // } else if (temp.length == 5) {
            //   //   stringTemp = temp[0] + ' / ' + temp[1] + ' / ' + temp[2]
            //   //     + '/ <br>' + temp[3] + ' / ' + temp[4];
            //   // } else if (temp.length == 6) {
            //   //   stringTemp = temp[0] + ' / ' + temp[1] + ' / ' + temp[2]
            //   //     + '/ <br>' + temp[3] + ' / ' + temp[4] + ' / ' + temp[5];
            //   // } else if (temp.length == 7) {
            //   //   stringTemp = temp[0] + ' / ' + temp[1] + ' / ' + temp[2]
            //   //     + '/ <br>' + temp[3] + ' / ' + temp[4] + ' / ' + temp[5]
            //   //     + '/ <br>' + temp[6];
            //   // } else if (temp.length == 8) {
            //   //   stringTemp = temp[0] + ' / ' + temp[1] + ' / ' + temp[2]
            //   //     + '/ <br>' + temp[3] + ' / ' + temp[4] + ' / ' + temp[5]
            //   //     + '/ <br>' + temp[6] + ' / ' + temp[7];
            //   // } else if (temp.length == 9) {
            //   //   stringTemp = temp[0] + ' / ' + temp[1] + ' / ' + temp[2]
            //   //     + '/ <br>' + temp[3] + ' / ' + temp[4] + ' / ' + temp[5]
            //   //     + '/ <br>' + temp[6] + ' / ' + temp[7] + ' / ' + temp[8];
            //   // } else if (temp.length == 10) {
            //   //   stringTemp = temp[0] + ' / ' + temp[1] + ' / ' + temp[2]
            //   //     + '/ <br>' + temp[3] + ' / ' + temp[4] + ' / ' + temp[5]
            //   //     + '/ <br>' + temp[6] + ' / ' + temp[7] + ' / ' + temp[8] + ' / ' + temp[9];
            //   // }

            //   // detail[key].discount = stringTemp;
            // })
          }

          $scope.detail = detail;
          $scope.discountList = discountList;

          $scope.discounts = discountList;
        })
        .finally(function () {
          $loading.finish('orderHistory');
        });
    }
  }

}());
