(function() {
  'use strict';

  angular
    .module('app')
    .controller('NewsDetailController', NewsDetailController);

  /** @ngInject */
  function NewsDetailController($scope, $uibModalInstance, news) {
    init();

    function init() {
      /** define */
      $scope.newsList = news;
    }
    
    $scope.close = function () {
      $uibModalInstance.dismiss('cancel');
    };
  }

}());
