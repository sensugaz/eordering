(function() {
  'use strict';

  angular
    .module('app')
    .controller('OrderDetailController', OrderDetailController);

  /** @ngInject */
  function OrderDetailController($scope, orderId, saleOrderNumber, OrderService, $filter, $localStorage, $loading, $uibModalInstance) {
    init();

    function init() {
      $scope.order = {};
      $scope.discounts = [];

      $scope.discountSub = [];
      $scope.discountAdd = [];

      $loading.setDefaultOptions({ 
        text: 'กำลังโหลด'
      });

      fetchOrder();
    }

    $scope.close = function() {
      $uibModalInstance.dismiss('cancel');
    };

    /**
     * Functions
     */
    function fetchOrder() {
      $loading.start('orderInfo');
      $loading.start('orderPrecessInfo');
      
      OrderService.getOrderInfo(orderId)
        .then(function(res) {
          if (res.data.result == 'SUCCESS') {
            $scope.order = res.data.data.order;
          }
        })
        .finally(function() {
          $loading.finish('orderInfo');
        });

      OrderService.getOrderPrecessInfo(saleOrderNumber)
        .then(function(res) {
          if (res.data.result == 'SUCCESS') {
            var	detail = res.data.data.orderProcessItemList;
            var discountList = res.data.data.orderProcessDiscountList;
            var orderProcessInfo = res.data.data.orderProcessInfo; 
            
            $scope.order.salesDocument = orderProcessInfo.salesDocument;
            $scope.discounts = discountList;
    
            var temp = [];
            var stringTemp = '';
            
            detail.forEach(function(value) {
              temp = value.discount.split('/');
              
              stringTemp = value.discount;

              if (temp.length == 4) {
                stringTemp = temp[0] + '/' + temp[1] + '/' + temp[2] +
                  '/<br>' + temp[3];
              } else if (temp.length == 5) {
                stringTemp = temp[0] + '/' + temp[1] + '/' + temp[2] +
                  '/<br>' + temp[3] + '/' + temp[4];
              } else if (temp.length == 6) {
                stringTemp = temp[0] + '/' + temp[1] + '/' + temp[2] +
                  '/<br>' + temp[3] + '/' + temp[4] + '/' + temp[5];
              } else if (temp.length == 7) {
                stringTemp = temp[0] + '/' + temp[1] + '/' + temp[2] +
                  '/<br>' + temp[3] + '/' + temp[4] + '/' + temp[5] +
                  '/<br>' + temp[6];
              } else if (temp.length == 8) {
                stringTemp = temp[0] + '/' + temp[1] + '/' + temp[2] +
                  '/<br>' + temp[3] + '/' + temp[4] + '/' + temp[5] +
                  '/<br>' + temp[6] + '/' + temp[7];
              } else if (temp.length == 9) {
                stringTemp = temp[0] + '/' + temp[1] + '/' + temp[2] +
                  '/<br>' + temp[3] + '/' + temp[4] + '/' + temp[5] +
                  '/<br>' + temp[6] + '/' + temp[7] + '/' + temp[8];
              } else if (temp.length == 10) {
                stringTemp = temp[0] + '/' + temp[1] + '/' + temp[2] +
                  '/<br>' + temp[3] + '/' + temp[4] + '/' + temp[5] +
                  '/<br>' + temp[6] + '/' + temp[7] + '/' + temp[8] + '/' + temp[9];
              }
              
              value.discount = stringTemp;
            });

            $scope.products = detail;  
    
            $scope.total = {};
            $scope.total.sumAmount = orderProcessInfo.sumAmount;
            $scope.total.vatAmount = orderProcessInfo.vatAmount;
            $scope.total.netValue2 = orderProcessInfo.netValue2;
            $scope.total.netValue3 = orderProcessInfo.netValue3;
            $scope.total.sumTotalAmount = parseFloat($scope.total.netValue3) + parseFloat($scope.total.vatAmount);

            $scope.order.salesDocument = orderProcessInfo.salesDocument;
    
            $scope.discounts.forEach(function(value) {
              if (value.type == 'หัก') {
                $scope.discountSub.push(value);
              } else {
                $scope.discountAdd.push(value);
              }
            });
          }
        })
        .finally(function() {
          $loading.finish('orderPrecessInfo');
        });
    }
  }

}());
