(function() {
  'use strict';

  angular
    .module('app')
    .controller('CartModalController', CartModalController);

  /** @ngInject */
  function CartModalController($scope, $rootScope, $filter, $state, $localStorage, $loading, $uibModalInstance, CartService) {
    init();

    function init() {
      $scope.userInfo = $localStorage.userInfo;
      $scope.customerInfo = $localStorage.customerInfo;
      $scope.products = [];
      $scope.boms = [];

      $scope.totalAmount = 0;

      $loading.setDefaultOptions({ 
        text: 'กำลังโหลด'
      });

      fetchCart();
    }

    $scope.toCheckout = function() {
      $uibModalInstance.dismiss('cancel');      
      $state.go('checkout');
    };

    $scope.close = function() {
      $uibModalInstance.dismiss('cancel');     
    };

    $scope.edtQty = function(product) {
      if (product.qty == 0 || product.qty == undefined) {
        switch (product.altUnitCode) {
          case 'BOX':
            if (product.isBox) {
              product.qty = product.altUnitAmount;
            } else {
              product.qty = 1;
            }
            break;
          case 'DZ':
            product.qty = product.altUnitAmount;
            break;
          default:
            product.qty = 1;
            break;
        }
      } else if (product.isBox || product.altUnitCode == 'DZ') {
        var textAltUnitAmount = product.altUnitAmount;
        var textAltUnitNameTh = product.unitNameTh;
        
        switch (product.altUnitCode) {
          case 'DZ':
            textAltUnitAmount = 1;
            textAltUnitNameTh = product.altUnitNameTh;
            break;
        }
 
        if (product.qty < product.altUnitAmount) {
          swal('ไม่สำเร็จ', 'กรุณาสั่งซื้ออย่างน้อย ' + textAltUnitAmount + ' ' + textAltUnitNameTh + ' ค่ะ', 'warning');

          product.qty = product.altUnitAmount;
        } else if (product.qty % product.altUnitAmount) {     
          swal('ไม่สำเร็จ', 'ผลิตภัณฑ์ต้องสั่งซื้อทีละ ' + textAltUnitAmount + ' ' + textAltUnitNameTh + ' ค่ะ <br> ระบบจะปรับจำนวนให้อัตโนมัติ', 'warning');

          product.qty = product.altUnitAmount * parseInt(product.qty / product.altUnitAmount);
        }
      }

      var formData = {
        cartList: [{
          customerId: $scope.customerInfo.customerId,
          userName: $scope.userInfo.userName,
          productId: product.productId,
          qty: product.qty
        }]
      };

      CartService.updateCart(formData)
        .then(function() {
          fetchCart();
        });
    };

    $scope.delCart = function(product) {
      var cartList = [];

      if (product.promotionCode != null) {
        $scope.products.forEach(function(value) {
          if (value.promotionId == product.promotionId) {
            cartList.push({
              customerId: $scope.customerInfo.customerId,
              userName: $scope.userInfo.userName,
              productId: value.productId,
              promotionId: value.promotionId
            });
          }
        });
      } else {
        cartList.push({
          customerId: $scope.customerInfo.customerId,
          userName: $scope.userInfo.userName,
          productId: product.productId,
          promotionId: 0
        });
      }

      var formData = {
        cartList: cartList
      };

      CartService.removeCart(formData)
        .then(function(res) {
          if (res.data.result == 'SUCCESS') {
            swal('สำเร็จ', 'ลบสินค้าเรียบร้อย', 'success');
            
            fetchCart();

            $rootScope.$broadcast('updateCartInHeader');   
            $rootScope.$broadcast('updateCartInCheckout');         
          }
        });
    };

    $scope.toCategory = function() {
      $uibModalInstance.dismiss('cancel');
      $state.go('category');
    };

    $scope.incQty = function(product) {
      var qty = 1;

      if (product.altUnitCode == 'BOX') {
        if (product.isBox) {
          qty = product.altUnitAmount;
        } else {
          qty = 1;
        }
      } else if (product.altUnitCode == 'DZ') {
        qty = product.altUnitAmount;
      } else {
        qty = 1;
      }

      var formData = {
        cartList: [{
          userName: $scope.userInfo.userName,
          customerId: $scope.customerInfo.customerId,
          productId: product.productId,
          qty: product.qty += qty
        }]
      };

      CartService.updateCart(formData)
        .then(function() {
          fetchCart();

          $rootScope.$broadcast('updateCartInCheckout'); 
        });
    };

    $scope.decQty = function(product) {
      var _ = {};
      var qty = 1;

      angular.copy(product, _);

      if (product.altUnitCode == 'BOX') {
        if (product.isBox) {
          qty = product.altUnitAmount;
        } else {
          qty = 1;
        }
      } else if (product.altUnitCode == 'DZ') {
        qty = product.altUnitAmount;
      } else {
        qty = 1;
      }
 
      if (_.qty > qty) {
        var formData = {
          cartList: [{
            userName: $scope.userInfo.userName,
            customerId: $scope.customerInfo.customerId,
            productId: product.productId,
            qty: parseInt(_.qty) - parseInt(qty)
          }]
        };

        CartService.updateCart(formData)
          .then(function() {
            fetchCart();

            $rootScope.$broadcast('updateCartInCheckout'); 
          });
      }
    };

    $scope.delCartAll = function() {
      if ($scope.products.length > 0) {
        swal({ 
          title: 'แจ้งเตือน',
          text: 'ต้องการลบสินค้าทั้งหมดในตะกร้าใช่หรือไม่ ?',
          type: 'warning',
          confirmButtonText: 'ตกลง',
          cancelButtonText: 'ยกเลิก',
          showCancelButton: true
        })
        .then(function(result) {
          if (result.value) {
            var formData = {
              cartList: [{
                customerId: $scope.customerInfo.customerId,
                productId: 0,
                userName: $scope.userInfo.userName,
                promotionId: 0
              }]
            };

            CartService.removeCart(formData)
              .then(function(res) {
                if (res.data.result == 'SUCCESS') {
                  swal('สำเร็จ', 'ลบสินค้าเรียบร้อย', 'success');
                  
                  fetchCart();
      
                  // update cart in header
                  $rootScope.$broadcast('updateCartInHeader');  
                  $rootScope.$broadcast('updateCartInCheckout');        
                }
              });
          }
        });
      } else {
        swal('แจ้งเตือน', 'ไม่มีสินค้าในตะกร้า', 'warning');
      }
    };

    /**
     * Function
     */
    function fetchCart() {
      $loading.start('cart');
      
      CartService.getCart($scope.customerInfo.customerId, $scope.userInfo.userName)
        .then(function(res) {
          if (res.data.result == 'SUCCESS') {
            $scope.products = res.data.data.cartList;
            $scope.boms = res.data.data.cartBOMItems;

            $scope.totalAmount = 0;
            
            // var product = [];

            // $filter('filter')($scope.products, function(item) {
            //   if (item.isFreeGoodes == false || item.isPremium == true) {
            //     return item;
            //   }
            // })
            // .forEach(function(value) {
            //   product.push(value);
            // });

            // $filter('filter')($scope.products, function(item) {
            //   if (item.isFreeGoodes == true && item.isPremium == false) {
            //     return item;
            //   }
            // })
            // .forEach(function(value) {
            //   var index = product.findIndex(function(item) {
            //     if (item.promotionCode == value.promotionCode && item.productCode == value.productCode && item.isFreeGoodes == false && item.isPremium == false) {
            //       return item;
            //     }
            //   });

            //   product.insert(index + 1, value);
            // });
            
            $scope.products.forEach(function(pd) {
              $scope.totalAmount += pd.totalAmount;

              $scope.boms.forEach(function(bm) {
                if (bm.productRefCode == pd.productCode) {
                  $scope.totalAmount += bm.price * pd.qty;
                }
              });
            });
          }
        })
        .finally(function() {
          $loading.finish('cart');
        });
    }
  }

  
}());
