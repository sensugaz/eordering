(function () {
  'use strict';

  angular
    .module('app')
    .controller('OrderInvoiceController', OrderInvoiceController);

  /** @ngInject */
  function OrderInvoiceController($scope, $localStorage, $filter, orderId, $loading, $uibModalInstance, OrderService, REPORT) {

    init();

    function init() {
      /** define */
      $scope.customerInfo = $localStorage.customerInfo;
      $scope.order = [];
      $scope.customer = {};

      $scope.totalQty = 0;
      $scope.totalAmount = 0;

      $loading.setDefaultOptions({ 
        text: 'กำลังโหลด'
      });

      fetchOrder();
    }

    $scope.toPrint = function() {
      window.open(REPORT + 'order/invoice?orderId=' + $scope.order.id, '_blank');
    };

    $scope.close = function() {
      $uibModalInstance.dismiss('cancel');
    };

    function fetchOrder() {
      $loading.start('orderInfo');      

      OrderService.getOrderInfo(orderId)
        .then(function(res) {
          if (res.data.result == 'SUCCESS') {
            $scope.order = res.data.data.order;
            $scope.products = res.data.data.orderDetailList;
            $scope.boms = res.data.data.orderBOMItems;

            $scope.totalQty = $filter('filter')($scope.products, {
              isBOM: false
            }).length + $scope.boms.length;

            $scope.totalAmount = 0;

            $scope.products.forEach(function(pd) {
              $scope.totalAmount += pd.totalAmount;
      
              $scope.boms.forEach(function(bm) {
                if (bm.productRefCode == pd.productCode) {
                  $scope.totalAmount += bm.price * pd.qty;
                }
              });
            });
          }
        })
        .finally(function() {
          $loading.finish('orderInfo');
        });
    }
  }

}());
