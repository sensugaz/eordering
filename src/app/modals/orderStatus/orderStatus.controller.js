(function() {
  'use strict';

  angular
    .module('app')
    .controller('OrderStatusController', OrderStatusController);

  /** @ngInject */
  function OrderStatusController($scope, orderId, saleOrderNumber, $localStorage,  $loading, $uibModalInstance, OrderService, REPORT) {
    init();

    function init() {
      /** define */
      $scope.customerInfo = $localStorage.customerInfo;
      $scope.customer = {};
      $scope.products = [];

      $loading.setDefaultOptions({ 
        text: 'กำลังโหลด'
      });

      fetchOrder();
    }

    $scope.toPrint = function() {
      window.open(REPORT + 'order/status?orderId=' + $scope.order.id + '&saleOrderNumber=' + $scope.order.salesDocument, '_blank');
    };

    $scope.close = function() {
      $uibModalInstance.dismiss('cancel');
    };

    /**
     * Functions
     */
    function fetchOrder() {
      $loading.start('orderInfo');
      $loading.start('orderPrecessInfo');

      OrderService.getOrderInfo(orderId)
        .then(function(res) {
          if (res.data.result == 'SUCCESS') {
            $scope.order = res.data.data.order;
          }
        })
        .finally(function() {
          $loading.finish('orderInfo');
        });

      if (saleOrderNumber) {
        OrderService.getOrderPrecessInfo(saleOrderNumber)
        .then(function(res) {
          if (res.data.result == 'SUCCESS') {
            $scope.order.salesDocument = res.data.data.orderProcessInfo.salesDocument;
          }
        });

        OrderService.getOrderProcessTracking(saleOrderNumber)
          .then(function(res) {
            if (res.data.result == 'SUCCESS') {
              $scope.products = res.data.data.orderProcessOrderItemList;

              $scope.total = {};
              $scope.total.targetQty = 0;
              $scope.total.billQty = 0;
              $scope.total.balaQty = 0;
              $scope.total.deliQty = 0;
              $scope.total.balaQty = 0;
              $scope.total.rejeQty = 0;
              
              $scope.products.forEach(function(value) {
                $scope.totalQty += value.qty;
                $scope.totalAmount += value.totalAmount;
                
                $scope.total.targetQty += value.targetQty;
                $scope.total.billQty += value.billQty;
                $scope.total.deliQty  += value.deliQty;
                $scope.total.balaQty  += value.balaQty;
                $scope.total.rejeQty += value.rejeQty;
              });
            }
          })
          .finally(function() {
            $loading.finish('orderPrecessInfo');
          });
      } else {
        $loading.finish('orderPrecessInfo');
      }
    }
  }

}());
