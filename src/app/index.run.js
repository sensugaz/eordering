
(function() {
  'use strict';

  angular
    .module('app')
    .run(chkWebShutDown)
    .run(runBlock)
    .run(loadConfig)
    .run(runSelect2)
    .run(runDetectBowser);
    
  /** @ngInject */
  function runBlock($rootScope, $filter, $state, $cookies, $localStorage) {
    $rootScope.isAuthenticated = $localStorage.isAuthenticated;
    
    $rootScope.$on('$stateChangeStart', function(e, toState) {
      document.body.scrollTop = 0;
      document.documentElement.scrollTop = 0;

      if (toState.data.requiresLogin != undefined) {     
        if ($localStorage.isAuthenticated && toState.data.requiresLogin) {
          if ($cookies.get('expiresLogin') == undefined || $cookies.get('expiresLogin') == '') {
            e.preventDefault();
            $state.go('logout');
          } else {
            var menuCode = toState.data.menuCode;
            
            if (menuCode != undefined) {
              var permission = $filter('filter')($localStorage.permission, function(item) {
                return item.menuCode == menuCode && item.privilegeId != 0;
              });
  
              if (permission != undefined) {
                if (!permission.length) {
                  e.preventDefault();
  
                  switch (menuCode) {
                    case '01100':
                      $state.go('logout');
                      break;
                    default:
                      $state.go('home'); 
                      break;
                  }
                }
              }
            }
          }
        } else if (!toState.data.requiresLogin && toState.name != 'logout') {
          if ($localStorage.isAuthenticated) {
            e.preventDefault();
            $state.go('home');
          }
        } else if (toState.data.requiresLogin) {
          e.preventDefault();
          $state.go('login');
        }
      }
    });
  }

  function loadConfig($rootScope, $localStorage, ConfigService) {
    ConfigService.getConfig()
      .then(function(res) {
        if (res.data.result == 'SUCCESS') {
          $localStorage.config = res.data.data.configList[0];
          $rootScope.config = res.data.data.configList[0];
        }
      });
  }

  function runSelect2() {
    $.fn.select2.defaults.set('theme', 'bootstrap');
    $.fn.select2.defaults.set('language', 'th');
  }

  function runDetectBowser(bowser, $uibModal) {
    if ((bowser.msie && bowser.version <= 11)) {
      var modalInstance = $uibModal.open({
        controller: 'DetectBowserController',
        templateUrl: 'app/modals/detectBowser/view/index.html',
        windowClass: 'modal-flat'
      });

      modalInstance.result.catch(function() { });
    }
  }

  function chkWebShutDown($rootScope, $state, $localStorage, ConfigService) {
    $rootScope.$on('$stateChangeStart', function(e, toState) {
      ConfigService.getConfig()
        .then(function(res) {
          if (res.data.result == 'SUCCESS') {
            var config = res.data.data.configList[0];

            if (config.shutdownSystem) {
              e.preventDefault();
              $state.go('maintenance');
            }

            if (toState.name == 'maintenance') {
              if (!config.shutdownSystem) {
                e.preventDefault();
                $state.go('login');
              }
            } else {
              if (config.shutdownSystem) {
                e.preventDefault();
                $state.go('maintenance');
              }
            }
          }
        });
    });
  }
})();
