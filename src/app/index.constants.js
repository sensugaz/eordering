/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('app')
    .constant('malarkey', malarkey)
    .constant('moment', moment)
    .constant('API_URL', 'http://qasorder2.toagroup.com:8010/API/')
    .constant('API_REPORT', 'http://qasorder2.toagroup.com:8050/api/')
    .constant('REPORT', 'http://qasorder2.toagroup.com:8050/print/');

})();
