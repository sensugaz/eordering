(function() {
  'use strict';

  angular
    .module('app')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider) {
    $stateProvider
      .state('login', {
        url: '/login',
        templateUrl: 'app/components/login/view/index.html',
        controller: 'LoginController',
        controllerAs: 'vm',
        data: {
          requiresLogin: false
        }
      });
  }

})();
