(function() {
  'use strict';

  angular
    .module('app')
    .controller('LoginController', LoginController);

  /** @ngInject */
  function LoginController($scope, $state, $rootScope, $localStorage, $cookies, $uibModal, AuthService, CustomerService, PermissionService, ConfigService) {
    init();

    function init() {
      /** define */
      //$scope.config = $localStorage.config;
      $scope.input = [];
      $scope.btnLoading = false;
      $scope.showPassword = false;

      if ($localStorage.loginFail == undefined) {
        $localStorage.loginFail = 0;
      }

      $scope.loginFail = $localStorage.loginFail;
    }

    $scope.onLogin = function(isValid) {
      if (isValid) {
        $scope.btnLoading = true;

        var formData = {
          userName: $scope.input.username,
          password: $scope.input.password
        };

        AuthService.login(formData)
          .then(function(res) {
            if (res.data.result == 'SUCCESS') {
              var type = res.data.data.userInfo.userTypeDesc;

              $localStorage.userInfo = res.data.data.userInfo;
              $localStorage.isAuthenticated = true;
              
              var today = new Date();
              var expires = new Date(today);

              // set expires login
              if ($scope.input.remember) {
                expires.setMinutes(525600);
              } else {
                expires.setMinutes(today.getMinutes() + $rootScope.config.webTimeout);
              }
           
              $cookies.put('expiresLogin', today.getTime(), {
                expires: expires
              });

              delete $localStorage.loginFail;
              
              // set menu permission
              PermissionService.getPermission(res.data.data.userInfo.roleId)
                .then(function(res) {
                  if (res.data.result == 'SUCCESS') {
                    $localStorage.permission = res.data.data.permissionList;
                  }
                });

              // set config
              ConfigService.getConfig()
                .then(function(res) {
                  if (res.data.result == 'SUCCESS') {
                    $localStorage.config = res.data.data.configList[0];
                  }
                });

              if (type == 'Single') {
                CustomerService.getOneCustomer(res.data.data.customerInfo.customerId)
                  .then(function(res) {
                    if (res.data.result == 'SUCCESS') {
                      $localStorage.customerInfo = res.data.data.customerInfo;

                      $state.go('home');                      
                    }
                  });
              } else {
                $state.go('store');
              }
            } else {
              $scope.loginFail += 1;
              $localStorage.loginFail = $scope.loginFail;
              
              switch (res.data.validateCode) {
                case '001':
                  swal('ไม่สำเร็จ', 'ชื่อผู้ใช้งานไม่ถูกต้อง', 'warning');
                  break;
                case '002':
                  swal('ไม่สำเร็จ', 'รหัสผ่านไม่ถูกต้อง', 'warning');
                  break;
                case '003':
                  $scope.openModalNonePasword();
                  break;
                case '004':
                  swal('เกิดข้อผิดพลาด', 'รหัสผู้ใช้งานถูกระงับการใช้งาน <br> กรุณาติดต่อผู้ดูแลระบบ', 'error');
                  break;
                case '005':
                  swal('เกิดข้อผิดพลาด', res.data.remarkText, 'error');
                  break;
              }
            }    
          })
          .finally(function() {
            $scope.btnLoading = false;
            clearForm();
          });
      }
    };

    $scope.openModalNonePasword = function() {
      var modalInstance = $uibModal.open({
        controller: 'NonePasswordController',
        templateUrl: 'app/modals/nonePassword/view/index.html',
        size: 'lg',
        windowClass: 'modal-flat'
      });

      modalInstance.result.catch(function() { });
    };

    /**
     * Functions
     */
    function clearForm() {
      $scope.input = {};
      $scope.form.$setPristine();
    }
  }

}());
