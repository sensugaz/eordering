(function () {
  'use strict';

  angular
    .module('app')
    .controller('ProblemController', ProblemController);

  /** @ngInject */
  function ProblemController($scope, $state, $localStorage, CustomerService) {
    init();

    function init() {
      $scope.userInfo = $localStorage.userInfo;
      $scope.customerInfo = $localStorage.customerInfo;
      $scope.input = {};
      $scope.btnLoading = false;
      
      if (($scope.customerInfo == undefined || angular.equals({}, $scope.customerInfo)) && $scope.userInfo.userTypeDesc == 'Multi') {
        $state.go('store');
      }
    }

    $scope.onSendProblem = function(isValid) {
      if (isValid) {
        $scope.btnLoading = true;

        var formData = {
          customerId: $scope.customerInfo.customerId,
          customerCode: $scope.customerInfo.customerCode,
          customerName: $scope.customerInfo.customerName,
          problemText: $scope.input.problemText,
          userName: $scope.userInfo.userName,
          customerEmailDC: $scope.customerInfo.customerEmailDC
        };

        CustomerService.customerProblem(formData)
          .then(function(res) {
            if (res.data.result == 'SUCCESS') {
              swal('สำเร็จ', 'ทำการแจ้งปัญหาเรียบร้อย', 'success');
            }
          })
          .finally(function() {
            $scope.btnLoading = false;
            clearForm();
          });
      }
    };

    /**
     * Functions
     */
    function clearForm() {
      $scope.input = {};
      $scope.form.$setPristine();
    }
  }

}());
