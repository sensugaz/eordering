(function() {
  'use strict';

  angular
    .module('app')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider) {
    $stateProvider
      .state('problem', {
        url: '/problem',
        templateUrl: 'app/components/problem/view/index.html',
        controller: 'ProblemController',
        controllerAs: 'vm',
        data: {
          requiresLogin: true,
          menuCode: '01904'
        }
      });
  }

})();