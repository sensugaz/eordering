(function() {
  'use strict';

  angular
    .module('app')
    .service('DocumentService', DocumentService);

  /** @ngInject */
  function DocumentService(API_URL, $http, $q) {
    this.getDocument = function() {
      var def = $q.defer();

      $http.get(API_URL + 'GeneralDocument')
        .then(function(res) {
          def.resolve(res);
        })
        .catch(function() {
          def.reject();
        });

      return def.promise;
    };
  }

}());
