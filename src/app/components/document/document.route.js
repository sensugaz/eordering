(function() {
  'use strict';

  angular
    .module('app')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider) {
    $stateProvider
      .state('document', {
        url: '/document',
        templateUrl: 'app/components/document/view/index.html',
        controller: 'DocumentController',
        controllerAs: 'vm',
        data: {
          requiresLogin: true,
          menuCode: '01600'
        }
      });
  }

})();
