(function() {
  'use strict';

  angular
    .module('app')
    .controller('DocumentController', DocumentsController);

  /** @ngInject */
  function DocumentsController($scope, $state, $filter, $localStorage, $loading, DocumentService) {
    init();

    function init() {
      // default
      $scope.userInfo = $localStorage.userInfo;
      $scope.customerInfo = $localStorage.customerInfo;
      $scope.options = [
        {
          value: '',
          label: 'ทั้งหมด'
        }
      ];

      $scope.type = '';

      if (($scope.customerInfo == undefined || angular.equals({}, $scope.customerInfo)) && $scope.userInfo.userTypeDesc == 'Multi') {
        $state.go('store');
      }

      /** pagination */
      $scope.pageSize = 5;
      $scope.maxSize = 5;
      $scope.currentPage = 1;

      $loading.setDefaultOptions({ 
        text: 'กำลังโหลด'
      });
      

      fetchDocument();
    }

    /**
     * Function
     */
    function fetchDocument() {
      $loading.start('document');

      DocumentService.getDocument()
        .then(function(res) {
          if (res.data.result == 'SUCCESS') {
            $scope.documentList = res.data.data.generalDocumentList;
            
            $scope.documentList.forEach(function(value) {
              var category = $filter('filter')($scope.options, function(item) {
                return item.value == value.categoryCode;
              })[0];

              if (category == undefined) {
                $scope.options.push({
                  value: value.categoryCode,
                  label: value.categoryDesc
                });
              }
            });
          }
        })
        .finally(function() {
          $loading.finish('document');
        });
    }

  }

}());
