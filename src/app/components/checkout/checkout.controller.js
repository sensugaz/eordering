(function () {
  'use strict';

  angular
    .module('app')
    .controller('CheckoutController', CheckoutController);

  /** @ngInject */
  function CheckoutController($scope, $rootScope, $state, $filter, $localStorage, ConfigService, CartService, OrderService, WizardHandler, REPORT) {
    init();

    function init() {
      /** define */
      $scope.config = $localStorage.config;
      $scope.userInfo = $localStorage.userInfo;
      $scope.customerInfo = $localStorage.customerInfo;
      $scope.btnLoading = false;
      $scope.products = [];
      $scope.boms = [];
      $scope.totalItem = 0;
      $scope.totalAmount = 0;
      $scope.freeGoods = [];

      $scope._requestDate = [];

      if (($scope.customerInfo == undefined || angular.equals({}, $scope.customerInfo)) && $scope.userInfo.userTypeDesc == 'Multi') {
        $state.go('store');
      }

      $scope.visible = ($scope.customerInfo.shipCondition == '03') ? false : true;

      $scope.order = {
        requestDateList: [{
          id: '',
          reqDate: '',
          txtDate: ''
        }],
        shipToList: [{
          shipId: '',
          shipCode: '',
          shipName: ''
        }],
        transportList: []
      };

      $scope.ship = '';
      $scope.transport = '';
      $scope.txtRequestDate = '';
      $scope.input = {
        documentDate: moment().format('DD/MM/YYYY'),
        customerPO: '',
        requestDate: '',
        shipTo: '',
        transport: ($scope.customerInfo.shipCondition == '03') ? $scope.customerInfo.transportZoneCode : '',
        isReceive: '',
        paymentTerm: ($scope.customerInfo.paymentTerm == 'CASH' || $scope.customerInfo.paymentTerm == 'CA02') ? ($scope.customerInfo.paymentTerm == 'CA02') ? 'CASH' : $scope.customerInfo.paymentTerm : ''
      };

      $rootScope.$on('updateCartInCheckout', function () {
        fetchCart();
      });

      /** fetch data */
      fetchConfig();
      fetchCart();
      fetchOrder();
      fetchTempRequestDate();
    }

    $scope.toPrint = function () {
      window.open(REPORT + 'cart?customerId=' + $scope.customerInfo.customerId, '_blank');
    };

    $scope.nextStep = function () {
      if ($scope.input.paymentTerm == undefined || $scope.input.paymentTerm == "") {
        swal('แจ้งเตือน', 'กรุณาเลือกการชำระเงิน', 'warning');
      } else {
        if ($scope.customerInfo.shipCondition == '03') {
          $scope.setTransport($scope.input.transport);
        }

        WizardHandler.wizard().next();
      }
    };

    $scope.prevStep = function () {
      WizardHandler.wizard().goTo(0);
    };

    $scope.setShipTo = function (shipId) {
      if (shipId != '') {
        var ship = $filter('filter')($scope.order.shipToList, {
          shipId: shipId
        })[0];

        $scope.ship = ship;

        if ($scope.ship.shipCondition == '03') {
          $scope.input.transport = ship.transportZone;
          $scope.setTransport($scope.input.transport);
        }
      } else {
        $scope.ship = '';
        $scope.input.transport = $scope.customerInfo.transportZoneCode;
      }

      if ($scope.customerInfo.shipCondition == '03' && shipId == '') {
        $scope.visible = false;
      } else if ($scope.customerInfo.shipCondition == '03' && $scope.ship.shipCondition == '03') {
        $scope.visible = false;
      } else if ($scope.customerInfo.shipCondition != '03' && $scope.ship.shipCondition == '03') {
        $scope.visible = false;
      } else {
        $scope.visible = true;

        if ($scope.ship.shipCondition == '03') {
          $scope.visible = false;
        }
      }
    };

    $scope.setTransport = function (transportZone) {
      if (transportZone != '') {
        var transport = $filter('filter')($scope.order.transportList, {
          transportZone: transportZone
        })[0];

        $scope.transport = transport;
      } else {
        $scope.transport = '';
      }
    };

    $scope.setRequestDate = function (requestDate) {
      var rqd = $filter('filter')($scope.order.requestDateList, {
        reqDate: requestDate
      })[0];

      if (rqd != undefined) {
        $scope.txtRequestDate = rqd.txtDate;
      }
    };

    $scope.setReceive = function () {
      //$scope.input.shipTo = '';
      //$scope.input.transport = '';
      //$scope.ship = '';
      //$scope.transport = '';
    };

    $scope.delCart = function (product) {
      var cartList = [];

      if (product.promotionCode != null) {
        $scope.products.forEach(function (value) {
          if (value.promotionId == product.promotionId) {
            cartList.push({
              customerId: $scope.customerInfo.customerId,
              userName: $scope.userInfo.userName,
              productId: value.productId,
              promotionId: value.promotionId
            });
          }
        });
      } else {
        cartList.push({
          customerId: $scope.customerInfo.customerId,
          userName: $scope.userInfo.userName,
          productId: product.productId,
          promotionId: 0
        });
      }

      var formData = {
        cartList: cartList
      };

      CartService.removeCart(formData)
        .then(function (res) {
          if (res.data.result == 'SUCCESS') {
            swal('สำเร็จ', 'ลบสินค้าเรียบร้อย', 'success');

            getTotalItem();
            getTotalAmount();

            fetchCart();

            $rootScope.$broadcast('updateCartInHeader');

            if ($scope.products.length == 0) {
              $state.go('home');
            }
          }
        });
    };

    $scope.addOrder = function () {
      if ($scope.input.paymentTerm == undefined) {
        swal('ไม่สำเร็จ', 'กรุณากรอกการชำระเงิน', 'warning');
        return false;
      }

      var hour = moment().format('H');
      var requestDate = $scope.input.requestDate;

      if ($scope.input.requestDate == '' || $scope.input.requestDate == undefined) {
        if (hour >= 17) {
          // if ($scope.userInfo.userTypeDesc == 'Multi') {
          //   if (moment($scope.order.requestDateList[1].reqDate).isAfter($scope.input.requestDate)) {
          //     requestDate = $scope.order.requestDateList[1].reqDate;
          //   } else {
          //     requestDate = $scope.order.requestDateList[2].reqDate;
          //   }
          // } else {
          //   if (moment($scope.order.requestDateList[1].reqDate).isAfter($scope.input.requestDate)) {
          //     requestDate = $scope.order.requestDateList[1].reqDate;
          //   } else {
          //     requestDate = $scope._requestDate[1].reqDate;
          //   }
          // }
          requestDate = $scope.order.requestDateList[1].reqDate;
        } else {
          requestDate = $scope.order.requestDateList[1].reqDate;
        }
      }

      var shipId = ($scope.ship == '' || $scope.input.isReceive) ? 0 : $scope.ship.shipId;
      var shipCode = ($scope.ship == '' || $scope.input.isReceive) ? '' : $scope.ship.shipCode;
      var shipName = ($scope.ship == '' || $scope.input.isReceive) ? 'รับสินค้าเอง' : $scope.ship.shipName;

      var shipCondition;
      var transportId;
      var transportZone;
      var transportZoneDesc;

      if ($scope.customerInfo.isReceive) {
        if ($scope.input.isReceive) {
          shipCondition = '01';
        } else {
          if ($scope.customerInfo.shipCondition == '03') {
            if ($scope.ship == '') {
              shipCondition = $scope.customerInfo.shipCondition;
            } else {
              shipCondition = $scope.ship.shipCondition;
            }
          } else {
            shipCondition = '';
          }
        }
      } else {
        if ($scope.ship == '') {
          shipCondition = $scope.customerInfo.shipCondition;
        } else {
          shipCondition = $scope.ship.shipCondition;
        }
      }

      if (shipCondition == '01') {
        transportId = 0;
        transportZone = '';
        transportZoneDesc = '';
      } else if (shipCondition == '03') {
        transportId = ($scope.transport == '') ? 0 : $scope.transport.transportId;
        transportZone = ($scope.transport == '') ? $scope.customerInfo.transportZone : $scope.transport.transportZone;
        transportZoneDesc = ($scope.transport == '') ? $scope.customerInfo.transportZoneDesc : $scope.transport.transportZoneDesc;
      } else {
        if ($scope.ship != '') {
          transportId = 0;
          transportZone = $scope.ship.transportZone;
          transportZoneDesc = $scope.ship.transportZoneDesc;
        } else {
          if ($scope.transport != '') {
            transportId = $scope.transport.transportId;
            transportZone = $scope.transport.transportZoneCode;
            transportZoneDesc = $scope.transport.transportZoneDesc;
          } else {
            transportId = 0;
            transportZone = $scope.customerInfo.transportZoneCode;
            transportZoneDesc = $scope.customerInfo.transportZoneDesc;
          }
        }
      }

      var shutdownSystem = false;

      ConfigService.getConfig()
        .then(function(res) {
          if (res.data.result == 'SUCCESS') {
            shutdownSystem = res.data.data.configList[0].shutdownSystem;
          }
        });
      
      if (shutdownSystem) {
        swal('แจ้งเตือน', 'ไม่สามารถทำรายการได้เนื่องจากอยู่นอกเหนือเวลาทำการ', 'warning');
      } else {
        var formData = {
          documentDate: moment().format(),
          userName: $scope.userInfo.userName,
          customerId: $scope.customerInfo.customerId,
          customerName: $scope.customerInfo.customerName,
          customerCode: $scope.customerInfo.customerCode,
          paymentTerm: ($scope.customerInfo.paymentTerm == 'CA02') ? $scope.customerInfo.paymentTerm : $scope.input.paymentTerm,
          isReceive: ($scope.input.isReceive == undefined || $scope.input.isReceive == null || $scope.input.isReceive == '') ? false : $scope.input.isReceive,
          shipCondition: shipCondition,
          shipId: shipId,
          shipCode: shipCode,
          shipName: shipName,
          requestDate: requestDate,
          customerPO: $scope.input.customerPO,
          transportId: transportId,
          transportZone: transportZone,
          transportZoneDesc: transportZoneDesc,
          isWeb: 'Y'
        };
  
        swal({
          title: 'ยืนยัน',
          text: 'คุณต้องการยืนยันใบสั่งซื้อหรือไม่ ?',
          type: 'info',
          confirmButtonText: 'ยืนยัน',
          cancelButtonText: 'ยกเลิก',
          showCancelButton: true,
          reverseButtons: true
        })
          .then(function (result) {
            if (result.value) {
              $scope.btnLoading = true;
  
              OrderService.addOrder(formData)
                .then(function (res) {
                  if (res.data.result == 'SUCCESS') {
                    swal({
                      title: 'ยืนยัน',
                      html: 'ระบบดำเนินการสร้างใบสั่งซื้อ <br> เลขที่ <b>' + res.data.data.order.documentNumber + '</b> เรียบร้อยแล้ว',
                      type: 'info',
                      confirmButtonText: 'ดูรายละเอียดสั่งซื้อ',
                      cancelButtonText: 'กลับสู่หน้าหลัก',
                      showCancelButton: true
                    })
                      .then(function (result) {
                        if (result.value) {
                          $state.go('summary', { orderId: res.data.data.order.orderId });
                        } else {
                          $state.go('home');
                        }
                      });
                  } else {
                    swal('พบข้อผิดพลาด', 'ระบบไม่สามารถบันทึกใบสั่งซื้อได้', 'error');
                  }
  
                  WizardHandler.wizard().next();
                  $scope.btnLoading = false;
                });
            }
          });
      }
    };

    // $scope.chkShowPromotion = function(product) {
    //   if (product.isFreeGoodes) {
    //     var freeGoods = $filter('filter')($scope.products, function(item) {
    //       if (!item.isFreeGoodes) {
    //         if (item.productCode == product.productCode) {
    //           return item;
    //         }
    //       }
    //     });
        
    //     if (freeGoods.length > 0) {
    //       return false;
    //     } else {
    //       return true;
    //     }
    //   } else {
    //     return true;
    //   }
    // };

    // $scope.chkHidePromotion = function(product) {
    //   var freeGoods = $filter('filter')($scope.products, function(item) {
    //     if (!item.isFreeGoodes) {
    //       if (item.productCode == product.productCode) {
    //         return item;
    //       }
    //     }
    //   });

    //   if (freeGoods.length > 0) {
    //     return true;
    //   } else {
    //     return false;
    //   }
    // };

    /**
     * Functions
     */
    function fetchConfig() {
      ConfigService.getConfig()
        .then(function (res) {
          if (res.data.result == 'SUCCESS') {
            $scope.config = res.data.data.configList[0];
          }
        });
    }

    function fetchCart() {
      CartService.getCart($scope.customerInfo.customerId, $scope.userInfo.userName)
        .then(function (res) {
          if (res.data.result == 'SUCCESS') {
            if (res.data.data.cartList.length == 0) {
              $state.go('home');
            }

            $scope.products = res.data.data.cartList;
            $scope.boms = res.data.data.cartBOMItems;
            $scope.products.forEach(function(value) {
              if (value.isFreeGoodes) {
                $scope.freeGoods.push(value);
              }
            });

            getTotalItem();
            getTotalAmount();
          } else {
            $state.go('home');
          }
        });
    }
    

    function fetchOrder() {
      OrderService.getOrder($scope.customerInfo.customerId, $scope.userInfo.userName)
        .then(function (res) {
          if (res.data.result == 'SUCCESS') {
            res.data.data.requestDateList.forEach(function (value) {
              value.txtDate = moment(value.reqDate).format('DD/MM/YYYY');

              $scope.order.requestDateList.push(value);
            });

            res.data.data.shipToList.forEach(function (value) {
              $scope.order.shipToList.push(value);
            });

            res.data.data.transportList.forEach(function (value) {
              $scope.order.transportList.push(value);
            });
          }
        });
    }

    function getTotalItem() {
      $scope.totalItem = $filter('filter')($scope.products, {
        isBOM: false
      }).length + $scope.boms.length;
    }

    function getTotalAmount() {
      $scope.totalAmount = 0;

      $scope.products.forEach(function (pd) {
        $scope.totalAmount += pd.totalAmount;

        $scope.boms.forEach(function (bm) {
          if (bm.productRefCode == pd.productCode) {
            $scope.totalAmount += bm.price * pd.qty;
          }
        });
      });
    }

    function fetchTempRequestDate() {
      OrderService.getRequestDate()
        .then(function (res) {
          if (res.data.result == 'SUCCESS') {
            $scope._requestDate = res.data.data.requestDateList;
          }
        });
    }
  }

}());
