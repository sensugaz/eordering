(function() {
  'use strict';

  angular
    .module('app')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider) {
    $stateProvider
      .state('checkout', {
        url: '/checkout',
        templateUrl: 'app/components/checkout/view/index.html',
        controller: 'CheckoutController',
        controllerAs: 'vm',
        data: {
          requiresLogin: true
        }
      });
  }

})();
