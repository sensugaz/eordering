(function() {
  'use strict';

  angular
    .module('app')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider) {
    $stateProvider
      .state('contact', {
        url: '/contact',
        templateUrl: 'app/components/contact/view/index.html',
        controller: 'ContactController',
        controllerAs: 'vm',
        data: {
          requiresLogin: true,
          menuCode: '01902'
        }
      });
  }

})();
