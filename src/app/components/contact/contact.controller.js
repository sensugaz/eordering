(function () {
  'use strict';

  angular
    .module('app')
    .controller('ContactController', ContactController);

  /** @ngInject */
  function ContactController($scope, $state, $localStorage) {
    init();

    function init() {
      $scope.userInfo = $localStorage.userInfo;
      $scope.customerInfo = $localStorage.customerInfo;
      $scope.customer = $localStorage.customerInfo;

      if (($scope.customerInfo == undefined || angular.equals({}, $scope.customerInfo)) && $scope.userInfo.userTypeDesc == 'Multi') {
        $state.go('store');
      }
    }
  }

}());
