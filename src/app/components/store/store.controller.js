(function() {
  'use strict';

  angular
    .module('app')
    .controller('StoreController', StoreController);

  /** @ngInject */
  function StoreController($scope, $filter, $state, $localStorage, $loading, CustomerService) {
    init();

    function init() {
      /** define */
      $scope.userInfo = $localStorage.userInfo;
      //$scope.customers = [];
      
      /** pagination */
      $scope.pageSize = 12;
      $scope.maxSize = 5;
      $scope.currentPage = 1;

      $loading.setDefaultOptions({ 
        text: 'กำลังโหลด',
        overlay: false
      });

      if ($scope.userInfo.userTypeDesc == 'Single') {
        $state.go('home');
      }

      fetchCustomer();
    }

    $scope.selectStore = function(customerId) {
      CustomerService.getOneCustomer(customerId)
        .then(function(res) {
          if (res.data.result == 'SUCCESS') {
            $localStorage.customerInfo = res.data.data.customerInfo;            
            $state.go('home');            
          }
        });
    };

    $scope.permission = function(menuCode) {
      var menu = $filter('filter')($localStorage.permission, function(item) {
        return item.menuCode == menuCode && item.privilegeId != 0;
      });

      return (menu != undefined) ? (menu.length) ? menu[0] : false : false;
    };

    /**
     * Functions
     */
    function fetchCustomer() {       
      $loading.start('store');
      
      CustomerService.getCustomer($scope.userInfo.userId)
        .then(function(res) {
          if (res.data.result == 'SUCCESS') { 
            $scope.customers = res.data.data.customerList;
          }
        })
        .finally(function() {
          $loading.finish('store');
        });
    }
  }

}());
