(function() {
  'use strict';

  angular
    .module('app')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider) {
    $stateProvider
      .state('store', {
        url: '/store',
        templateUrl: 'app/components/store/view/index.html',
        controller: 'StoreController',
        controllerAs: 'vm',
        data: {
          requiresLogin: true,
          menuCode: '01110'
        }
      });
  }

})();
