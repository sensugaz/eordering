(function() {
  'use strict';

  angular
    .module('app')
    .controller('NewsListController', NewsListController);

  /** @ngInject */
  function NewsListController($scope, $state, $localStorage, $loading, $uibModal, $filter, NewsService) {
    init();

    function init() {
      $scope.userInfo = $localStorage.userInfo;
      $scope.customerInfo = $localStorage.customerInfo;

      if (($scope.customerInfo == undefined || angular.equals({}, $scope.customerInfo)) && $scope.userInfo.userTypeDesc == 'Multi') {
        $state.go('store');
      }

      $loading.setDefaultOptions({ 
        text: 'กำลังโหลด'
      });

      /** pagination */
      $scope.pageSize = 8;
      $scope.maxSize = 5;
      $scope.currentPage = 1;

      fetchNews();
    }

    $scope.openModalNewsDetail = function(news) {
      var modalInstance = $uibModal.open({
        controller: 'NewsDetailController',
        templateUrl: 'app/modals/newsDetail/view/index.html',
        windowClass: 'modal-flat news-modal',
        resolve: {
          news: function() {
            return $filter('filter')($scope.newsListDT, function(value) {
              if (value.newsId == news.newsId) {
                return value;
              }
            });
          }
        }
      });

      modalInstance.result.catch(function() { });
    };

    /**
     * Function
     */
    function fetchNews() {
      $loading.start('news');

      NewsService.getNews()
        .then(function(res) {
          if (res.data.result == 'SUCCESS') {
            $scope.newsList = res.data.data.newsActivityHDList;
            $scope.newsListDT = res.data.data.newsActivityDTList;
          }
        })
        .finally(function() {
          $loading.finish('news');          
        });
    }
  }

}());
