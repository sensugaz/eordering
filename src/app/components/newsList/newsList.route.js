(function() {
  'use strict';

  angular
    .module('app')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider) {
    $stateProvider
      .state('newsList', {
        url: '/news/list',
        templateUrl: 'app/components/newsList/view/index.html',
        controller: 'NewsListController',
        controllerAs: 'vm',
        data: {
          requiresLogin: true
        }
      });
  }
})();
