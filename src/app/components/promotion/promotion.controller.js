(function() {
  'use strict';

  angular
    .module('app')
    .controller('PromotionController', PromotionController);

  /** @ngInject */
  function PromotionController($scope, $state, $filter, $stateParams, $loading, $localStorage, $rootScope, PromotionService, ProductService, CartService, CustomerService) {
    init();

    function init() {
      /** define */
      $scope.userInfo = $localStorage.userInfo;
      $scope.customerInfo = $localStorage.customerInfo;
      $scope.config = $localStorage.config;
      $scope.promotionHD = {};
      $scope.promotionDT = [];
      $scope.promotionDT_Sel = [];
      $scope.promotionFG = [];
      $scope.promotionFG_Sel = [];
      $scope.promotionDealer = [];

      $scope.productInDt = [];
      $scope.btfInDt = [];

      $scope.freeGoods = [];
      $scope.productInFreeGoods = [];

      $scope.btfs = [];
      $scope.sizes = [];
      $scope.colors = [];

      $scope.sizeFreeGoods = [];
      $scope.colorFreeGoods = [];

      $scope.input = {
        promotionSetValue: 1,
        salesGroup: ''
      };

      $scope.countPromotionSet = 0;
      $scope.promotionSetValue = 0;

      $scope.promotionId = null;

      $scope.hideFreeGoodsAll = false;
      $scope.hideFreeGoodsSel = false;
      $scope.hideAddPromotionSet = false;

      $scope.temps = {
        promotionHD: null,
        promotionDT: [],
        promotionDT_Sel: []
      };

      $scope.stepCount = 1;

      $loading.setDefaultOptions({ 
        text: 'กำลังโหลด'
      });    

      if ($scope.config.promotionHide == 'Y') {
        $state.go('home');
      }

      $scope.countOrder = 0;

      $scope.btnLoading = false;

      chkCustomerBlock();

      /** fetch data */
      fetchPromotion();
    }

    $scope.refreshForm = function() {
      $state.reload();
    };

    $scope.incProductQty = function(promotionDT) {
      var product = promotionDT.productData;
      var qty = 1;

      switch (product.altUnit1Code) {
        case 'BOX':
          if (product.isBox) {
            qty = product.altUnit1Amount;
          } else {
            qty = 1;
          }
          break;
        case 'DZ':
          qty = product.altUnit1Amount;
          break;
        default:
          qty = 1;
          break;
      }
      
      // if (promotionDT.maxQty != 0) {
      //   if (promotionDT.qty > promotionDT.maxQty) {
      //     promotionDT.qty = promotionDT.maxQty;
      //   }
      // }

      if ($scope.promotionHD.isPromotionSet) {
        var limit = getLimitPromotionSet(promotionDT);

        if (promotionDT.qty < limit) {
          promotionDT.qty += qty;
        }
      } else {
        promotionDT.qty = parseInt(promotionDT.qty) + parseInt(qty);
      }
    };

    $scope.decProductQty = function(promotionDT) {
      var product = promotionDT.productData;
      var qty = 1;

      switch (product.altUnit1Code) {
        case 'BOX':
          if (product.isBox) {
            qty = product.altUnit1Amount;
          } else {
            qty = 1;
          }
          break;
        case 'DZ':
          qty = product.altUnit1Amount;
          break;
        default:
          qty = 1;
          break;
      }
      
      if (promotionDT.qty > qty) {
        promotionDT.qty -= qty;
      }
    };

    $scope.edtProductQty = function(promotionDT) {
      if ($scope.promotionHD.isPromotionSet) {
        var limit = getLimitPromotionSet(promotionDT);

        if (promotionDT.qty > limit) {
          promotionDT.qty = limit;
        }
      }

      // if (promotionDT.qty < promotionDT.minQty) {
      //   promotionDT.qty = promotionDT.minQty;
      // } else if ((promotionDT.qty > promotionDT.maxQty) && promotionDT.maxQty != 0) {
      //   promotionDT.qty = promotionDT.maxQty;
      // }
      if (promotionDT.qty == 0 || promotionDT.qty == undefined) {
        switch (promotionDT.productData.altUnit1Code) {
          case 'BOX':
            if (promotionDT.productData.isBox) {
              promotionDT.qty = promotionDT.productData.altUnit1Amount;
            } else {
              promotionDT.qty = 1;
            }
            break;
          case 'DZ':
            promotionDT.qty = promotionDT.productData.altUnit1Amount;
            break;
          default:
            promotionDT.qty = 1;
            break;
        }
      } else if (promotionDT.productData.isBox || promotionDT.productData.altUnit1Code == 'DZ') {
        var textAltUnit1Amount = promotionDT.productData.altUnit1Amount;
        var textAltUnit1NameTh = promotionDT.productData.unitNameTh;

        switch (promotionDT.productData.altUnit1Code) {
          case 'DZ':
            textAltUnit1Amount = 1;
            textAltUnit1NameTh = promotionDT.productData.altUnit1NameTh;
            break;
        }

        if (promotionDT.qty < promotionDT.productData.altUnit1Amount) {
          swal('ไม่สำเร็จ', 'กรุณาสั่งซื้ออย่างน้อย ' + textAltUnit1Amount + ' ' + textAltUnit1NameTh + ' ค่ะ', 'warning');

          promotionDT.qty = promotionDT.productData.altUnit1Amount;   
        } else if (promotionDT.qty % promotionDT.productData.altUnit1Amount) {
          swal('ไม่สำเร็จ', 'ผลิตภัณฑ์ต้องสั่งซื้อทีละ ' + textAltUnit1Amount + ' ' + textAltUnit1NameTh + ' ค่ะ <br> ระบบจะปรับจำนวนให้อัตโนมัติ', 'warning');

          promotionDT.qty = promotionDT.productData.altUnit1Amount * parseInt(promotionDT.qty / promotionDT.productData.altUnit1Amount);
        }
      }
    };

    $scope.incFreeGoodQty = function(promotionFG) {
      if (promotionFG.qty < promotionFG.showQty) {
        var qty = 1;

        switch(promotionFG.productFreeGoodsData.altUnit1Code) {
          case 'BOX':
            qty = promotionFG.productFreeGoodsData.altUnit1Amount;
            break;
          default:
            qty = 1;
            break;
        } 

        promotionFG.qty = parseInt(promotionFG.qty) + parseInt(qty);
      }
    };

    $scope.decFreeGoodQty = function(promotionFG) {
      var qty = 1;

      switch(promotionFG.productFreeGoodsData.altUnit1Code) {
        case 'BOX':
          qty = promotionFG.productFreeGoodsData.altUnit1Amount;
          break;
        default:
          qty = 1;
          break;
      } 

      if (promotionFG.qty > qty) {
        promotionFG.qty -= qty;
      }
    };

    $scope.edtFreeGoodQty = function(promotionFG) {
      if (promotionFG.qty == 0 || promotionFG.qty == undefined) {
        switch(promotionFG.productFreeGoodsData.altUnit1Code) {
          case 'BOX':
            promotionFG.qty = promotionFG.productFreeGoodsData.altUnit1Amount;
            break;
          default:
            promotionFG.qty = 1;
            break;
        } 
      } else {
        if (promotionFG.qty < promotionFG.productFreeGoodsData.altUnit1Amount || promotionFG.qty > promotionFG.showQty) {
          promotionFG.qty = promotionFG.productFreeGoodsData.altUnit1Amount;
        } else if (promotionFG.qty % promotionFG.productFreeGoodsData.altUnit1Amount) {
          promotionFG.qty = (promotionFG.productFreeGoodsData.altUnit1Amount * parseInt(promotionFG.qty / promotionFG.productFreeGoodsData.altUnit1Amount));
        }
      }
    };

    $scope.setBtf = function(promotionDT, btfInDt) {
      $loading.start('promotion');
     
      getProductInBtf(btfInDt.btfWeb, null).then(function(res) {
        promotionDT.productData = res;
      })
      .finally(function() {
        $loading.finish('promotion');
      });

      promotionDT.btfInDt = btfInDt;
      promotionDT.productName = btfInDt.btfWebDescTh;

      // reload size
      getSize(promotionDT.format, promotionDT, promotionDT.btfInDt.btf);
      
      // reload color
      getColor(promotionDT.format, promotionDT, promotionDT.btfInDt.btf);

      promotionDT.qty = promotionDT.minQty;
    };

    $scope.setSize = function(promotionDT, size) {      
      if (promotionDT.format == 'MG' || promotionDT.format == 'B') {
        $loading.start('promotion');

        getProductInBtf(size.btfCode, size.productCode).then(function(res) {
          promotionDT.productData = res;
        })
        .finally(function() {
          $loading.finish('promotion');
        });
        
        promotionDT.qty = promotionDT.minQty;
      } else {
        promotionDT.productData = $filter('filter')($scope.productInDt, { productCode: size.productCode })[0];
      }
    };

    $scope.setSizeFreeGoods = function(promotionFG, size) {              
      promotionFG.productFreeGoodsData = $filter('filter')($scope.productInFreeGoods, { productCode: size.productCode })[0];  
    };

    $scope.setColor = function(promotionDT, color) {
      if (promotionDT.format == 'MG' || promotionDT.format == 'B') {
        $loading.start('promotion');

        getProductInBtf(color.btfCode, color.productCode).then(function(res) {
          promotionDT.productData = res;
        })
        .finally(function() {
          $loading.finish('promotion');
        });
        
        promotionDT.qty = promotionDT.minQty;
      } else {
        promotionDT.productData = $filter('filter')($scope.productInDt, { productCode: color.productCode })[0];
      }
    };

    $scope.setColorFreeGoods = function(promotionFG, color) {
      promotionFG.productFreeGoodsData = $filter('filter')($scope.productInFreeGoods, { productCode: color.productCode })[0];
    };

    $scope.addProduct = function(promotionDT) {
      var chkProductExists = $filter('filter')($scope.promotionDT_Sel, function(item) {
        return item.productData.productCode == promotionDT.productData.productCode;
      })[0] != undefined;

      var productData = promotionDT.productData;
      var productForm = angular.copy(promotionDT);

      if (promotionDT.format == 'MG' || promotionDT.format == 'B') {  
        productForm.totalAmount = parseInt(productData.productPrice) * parseInt(productForm.qty);   
      } else {
        productForm.totalAmount = parseInt(productData.productPrice) * parseInt(productForm.qty);  
      }
      
      $scope.promotionFG = [];
      $scope.promotionFG_Sel = [];

      var index;

      if ($scope.promotionHD.isPromotionSet) {
        $scope.temps.promotionDT_Sel.push(angular.copy(promotionDT));

        if (!chkProductExists) {
          productForm.buyQty = productForm.qty;
          promotionDT.qty = productForm.qty;

          $scope.promotionDT_Sel.push(productForm);   
        } else {
          index = $scope.promotionDT_Sel.findIndex(function(item) {
            if (item.productData.productCode == promotionDT.productData.productCode) {
              return item;
            }
          });

          productForm.qty = parseInt(productForm.qty) + parseInt($scope.promotionDT_Sel[index].qty);
          productForm.buyQty = productForm.qty;

          $scope.promotionDT_Sel[index] = productForm;
        }

        /** ตรวจสอบและตั้งค่าข้อมูลเมื่อทำการซื้อสินค้าครบเซ็ท */
        if (chkPromotionSet()) {
          $scope.input.promotionSetValue = 1;
          $scope.promotionSetValue = 0;

          $scope.temps.promotionDT_Sel = [];

          promotionDT.qty = productForm.minQty;
          $scope.hideAddPromotionSet = true;
          
          if ($scope.promotionHD.promotionSetValue && $scope.promotionHD.promotionSetValue != 1) {
            $scope.hideAddPromotionSet = false; 
          }
        }
      } else {
        if (!chkProductExists) {
          productForm.buyQty = productForm.qty;
          
          $scope.promotionDT_Sel.push(productForm);
        } else {
          index = $scope.promotionDT_Sel.findIndex(function(item) {
            if (item.productData.productCode == promotionDT.productData.productCode) {
              return item;
            }
          });

          productForm.qty = parseInt(productForm.qty) + parseInt($scope.promotionDT_Sel[index].qty);
          productForm.buyQty = productForm.qty;
          
          $scope.promotionDT_Sel[index] = productForm;
        }
      }
    };

    $scope.delProduct = function(promotionDT) {
      var index = $scope.promotionDT_Sel.indexOf(promotionDT);

      $scope.promotionDT_Sel.splice(index, 1);
      $scope.promotionFG = [];
      $scope.promotionFG_Sel = [];   
      
      if ($scope.promotionHD.isPromotionSet) {
        if (!$scope.promotionDT_Sel.length) {
          $scope.countPromotionSet = 0;
        }
      }
    };

    $scope.calculate = function() {
      var cartList = [];
      var chkPromotionSet = false;
      var dummy = true;

      if ($scope.temps.promotionHD.dummyType == 'Qty') {
        if (getTotalQtyForDummy() >= $scope.temps.promotionHD.dummyTotal) {
          if ($scope.temps.promotionHD.dummyTotalLimit > 0 && getTotalQtyForDummy() <= $scope.temps.promotionHD.dummyTotalLimit) {
            dummy = true;
          } else if ($scope.promotionHD.dummyTotalLimit > 0 && getTotalQtyForDummy() > $scope.temps.promotionHD.dummyTotalLimit) {
            dummy = false;
          } else if ($scope.promotionHD.dummyTotalLimit == 0) {
            dummy = true;
          } 
        } else {
          dummy = false;
        }
      } else if ($scope.temps.promotionHD.dummyType == 'Amount') {
        if (getTotalAmountForDummy() >= $scope.temps.promotionHD.dummyTotal) {
          if ($scope.temps.promotionHD.dummyTotalLimit > 0 && getTotalAmountForDummy() <= $scope.temps.promotionHD.dummyTotalLimit) {
            dummy = true;
          } else if ($scope.temps.promotionHD.dummyTotalLimit > 0 && getTotalAmountForDummy() > $scope.temps.promotionHD.dummyTotalLimit) {
            dummy = false;
          } else if ($scope.temps.promotionHD.dummyTotalLimit == 0) {
            dummy = true;
          } 
        } else {
          dummy = false;
        }
      } else {
        dummy = true;
      }

      if (dummy) {
        $filter('filter')($scope.promotionDT_Sel, function(item) {
          if (item.promotionId == $scope.temps.promotionHD.promotionId) {
            return item;
          }
        })
        .forEach(function(value) {
          cartList.push({
            customerId: $scope.customerInfo.customerId,
            productId: value.productData.productId,
            qty: value.qty
          });
        });
        
        var formData = {
          promotionId: $scope.temps.promotionHD.promotionId,
          cartList: cartList
        };

        var result = '';
  
        PromotionService.chkPromotionValidate(formData)
          .then(function(res) {
            result = res.data.result;
            
            if (res.data.result == 'SUCCESS') {
              $scope.promotionFG = [];
              $scope.promotionFG_Sel = [];
 
              /** discount */
              if ($scope.promotionHD.promotionType == 'Discount' && !$scope.freeGoods.length) {
                $scope.addCart();
              } else {
                
                if (!$scope.promotionHD.checkStepBySKU) {
                  chkPromotionSet = true;
                } else {
                  if (($scope.freeGoods[0].productQty + $scope.freeGoods[0].freeGoodsQty) > 0) {
                    if ($scope.getTotalQty() % ($scope.freeGoods[0].productQty + $scope.freeGoods[0].freeGoodsQty) == 0) {
                      chkPromotionSet = true;
                    } else {
                      chkPromotionSet = false;
                    }
                  } else {
                    chkPromotionSet = false;
                  }
                }
  
                if ($scope.promotionHD.keepBox && res.data.productList.length > 0) {
                  res.data.productList.forEach(function(value) {
                    var product = $filter('filter')($scope.promotionDT_Sel, function(item) {
                      if (item.promotionId == value.promotionId && item.productData.productId == value.productId) {
                        return item;
                      }
                    })[0];
                    
                    if (chkDealerExists()) {
                      if (!$scope.promotionHD.checkStepBySKU) {
                        product.buyQty = value.productQty;
                      }
                    } else {
                      if (chkPromotionSet) {
                        product.buyQty = value.productQty;
                      }
                    }
                  });
                }
    

                $scope.promotionFG.forEach(function(value, key) {
                  if (value.promotionId != $scope.promotionHD.promotionId) {
                    $scope.promotionFG.splice(key, 1);
                  }
                });
    
                if (chkPromotionSet) {
                  res.data.freeGoodsList.forEach(function(value) {              
                    if (value.validFlag) {
                      var freeGoods = angular.copy($filter('filter')($scope.freeGoods, { freeGoodsId: value.freeGoodsId })[0]);
                     
                      // case 5.1
                      if ($scope.promotionHD.checkStepBySKU == false) {
                        if ($scope.promotionHD.promotionType == 'Free' && $scope.promotionHD.dummyType == 'Amount' && $scope.promotionHD.isStep && $scope.promotionHD.isAutostep) {
                          value.freeGoodsQty *= parseInt($scope.getTotalAmount() / $scope.promotionHD.dummyTotal);
                        }
                      }

                      if (freeGoods != undefined) {
                        freeGoods.editSize = false;
                        freeGoods.editColor = false;
                        freeGoods.qty = value.freeGoodsQty;
                        freeGoods.showQty = value.freeGoodsQty;
                        freeGoods.freeGoodsMatId = value.freeGoodsMatId;
                        
                        if ($scope.promotionHD.isPromotionSet) {
                          freeGoods.qty = value.freeGoodsQty * (($scope.countPromotionSet == 0) ? 1 : $scope.countPromotionSet);
                          freeGoods.showQty = value.freeGoodsQty * (($scope.countPromotionSet == 0) ? 1 : $scope.countPromotionSet);
                        } else {
                          freeGoods.qty = value.freeGoodsQty;
                          freeGoods.showQty = value.freeGoodsQty;
                        }

                        if (value.freeGoodsQty > 0) {
                          $scope.promotionFG.push(freeGoods);
                        }
                      }
                      
                      setFreeGoods(null, null, $scope.promotionFG);
                    }
                  });
             

                  if ($scope.promotionHD.checkStepBySKU || chkDealerExists()) {
                    $scope.promotionFG.forEach(function(value) {
                      $scope.addFreeGoods(value);
                    });

                    if (chkDealerExists()) {
                      swal('สำเร็จ', 'รายการสินค้าตรงกับโปรโมชั่นที่กำหนด', 'success');
                    }
                  } else {
                    var isHideFreeGoodSel = $filter('filter')($scope.promotionFG, function(item) {
                      if (item.format != 'SKU') {
                        return item;
                      }
                    });

                    if (isHideFreeGoodSel.length == 0) {
                      $scope.hideFreeGoodsSel = true;
                    }

                    $scope.promotionFG.forEach(function(value) {
                      if (value.format == 'SKU') {
                        $scope.addFreeGoods(value);
                      }
                    });
                  }
                } else {
                  swal('ไม่สำเร็จ', 'จำนวนสั่งซื้อไม่ครบชุดโปรโมชั่น', 'warning');
                }
              }
            } else if (res.data.result == 'WARINIG') {
              if ($scope.promotionHD.checkStepBySKU) {
                swal('ไม่สำเร็จ', 'รายการสินค้าต้องมีจำนวนสั่งซื้อรวม <br> อย่างน้อย ' + (parseInt($scope.freeGoods[0].productQty) + parseInt($scope.freeGoods[0].freeGoodsQty)) + ' หน่วย', 'warning');
              } else {
                if ($scope.promotionHD.promotionType == 'Discount' && !$scope.freeGoods.length) {
                  swal('ไม่สำเร็จ', 'โปรดตรวจสอบรายการสั่งซื้อ เพื่อรับโปรโมชั่น', 'warning');
                } else {
                  swal('ไม่สำเร็จ', res.data.invalidList[0], 'warning');
                }
              }
            } else {
              swal('ไม่สำเร็จ', res.data.remarkText, 'error');
            }
          })
          .then(function() {
            if (chkPromotionSet && result == 'SUCCESS') {
              if ($scope.promotionHD.promotionNextId) {  
                calculatePromotionNext($scope.promotionHD.promotionNextId);
              }
            }
          });
      } else {
        swal('แจ้งเตือน', 'รายการสินค้าต้องมีจำนวน (สินค้า/เงิน) <br> อย่างน้อย ' + $scope.temps.promotionHD.dummyTotal + ' ' + (($scope.temps.promotionHD.dummyType == 'Amount') ? 'บาท' : '') , 'warning');
      }
    };

    $scope.addFreeGoods = function(promotionFG) {
      var chkFreeGoodsExists = $filter('filter')($scope.promotionFG_Sel, function(item) {
        if ((item.freeGoodsId == promotionFG.freeGoodsId) && (item.productFreeGoodsData.productCode == promotionFG.productFreeGoodsData.productCode)) {
          return item;
        }
      })[0] != undefined;

      var productForm = angular.copy(promotionFG, productForm);
      
      if ($scope.promotionHD.numFreeGoods > 0) {
        if (!$scope.promotionHD.checkStepBySKU && ($scope.promotionFG_Sel.length + 1 > $scope.promotionHD.numFreeGoods)) {
          return swal('แจ้งเตือน', 'ไม่สามารถเลือกของแถมได้เกิน ' + $scope.promotionHD.numFreeGoods + ' รายการ', 'warning');
        }
      }

      if (!$scope.promotionHD.checkStepBySKU) {
        var qty = 0;
        var frg = $filter('filter')($scope.promotionFG_Sel, function(item) {
          return item.freeGoodsId == promotionFG.freeGoodsId;
        });

        if (frg.length > 0) {
          frg.forEach(function(value) {
            qty += parseInt(value.qty);
          });

          if ((parseInt(qty) + parseInt(promotionFG.qty)) > promotionFG.showQty) {
            return swal('แจ้งเตือน', 'จำนวนแถมเกินจำนวนแถมที่ได้รับ', 'warning');
          }
        }
      }

      if (!chkFreeGoodsExists) {
        $scope.promotionFG_Sel.push(productForm);
      } else {
        var index = $scope.promotionFG_Sel.findIndex(function(item) {
          if (item.freeGoodsMatId == promotionFG.freeGoodsMatId && item.freeGoodsId == promotionFG.freeGoodsId) {
            return item;
          }
        });

        var qty = parseInt($scope.promotionFG_Sel[index].qty);
        
        qty +=  parseInt(promotionFG.qty);

        productForm.qty = qty;
       
        $scope.promotionFG_Sel[index] = productForm;
      }
    };

    $scope.delFreeGoods = function(promotionFG) {
      var index = $scope.promotionFG_Sel.indexOf(promotionFG);

      $scope.promotionFG_Sel.splice(index, 1);
    };

    $scope.addCart = function() {
      $scope.btnLoading = true;

      var cartList = [];
      var promotionList = [];
      
      if ($scope.pricingConditions != undefined) {
        if ($scope.pricingConditions.conditionTypeCode == 'ZW81' || $scope.pricingConditions.conditionTypeCode == 'ZW82') {
          if ($scope.promotionHD.compound == 'Amount') {
            if ($scope.getTotalAmount() < $scope.pricingConditions.conditionPUnit) {
              swal('แจ้งเตือน', 'จำนวนสั่งซื้อไม่ครบชุดโปรโมชั่น', 'warning');
  
              $scope.btnLoading = false;
  
              return false;
            }
          } else if ($scope.promotionHD.compound == 'Qty') {
            if ($scope.getTotalBuyQty() < $scope.pricingConditions.conditionPUnit) {
              swal('แจ้งเตือน', 'จำนวนสั่งซื้อไม่ครบชุดโปรโมชั่น', 'warning');
  
              $scope.btnLoading = false;
              
              return false;
            }
          }
        }
      }

      $scope.promotionDT_Sel.forEach(function(value) {
        cartList.push({
          promotionDTId: value.promotionDtId,
          customerId: $scope.customerInfo.customerId,
          productId: value.productData.productId,
          qty: value.buyQty,
          userName: $scope.userInfo.userName,
          conditionType: ($scope.pricingConditions == undefined) ? '' : $scope.pricingConditions.conditionTypeCode
        });
      });
   
      if ($scope.temps.promotionHD.promotionType == 'Discount' && $scope.freeGoods.length == 0) {
        promotionList = [{ 
          promotionId: $scope.promotionHD.promotionId,
          freeGoodsId: 0,
          freeProductId: 0,
          qty: 0
        }];
      } else if ($scope.temps.promotionHD.promotionType == 'Free' && $scope.temps.promotionHD.redeem == 'Y') {
        promotionList = [{ 
          promotionId: $scope.promotionHD.promotionId,
          freeGoodsId: 0,
          freeProductId: 0,
          qty: 0
        }];
      } else {
        $scope.promotionFG_Sel.forEach(function(value) {
          if (value.qty > 0) {
            promotionList.push({
              promotionId: $scope.promotionHD.promotionId,
              freeGoodId: value.freeGoodsId,
              freeProductId: value.productFreeGoodsData.productId,
              qty: value.qty
            });
          }
        });
      }

      promotionList.sort(function(t,e) {
        return t.freeGoodId - e.freeGoodId;
      });
      
      var formData = {
        cartList: cartList,
        promotionList: (chkDealerExists()) ? [{ 
          promotionId: $scope.promotionHD.promotionId,
          freeGoodsId: 0,
          freeProductId: 0,
          qty: 0
        }] : promotionList
      };

      CartService.getCart($scope.customerInfo.customerId, $scope.userInfo.userName)
        .then(function(res1) {
          if (res1.data.result == 'SUCCESS') {
            var zwFilter = $filter('filter')(res1.data.data.cartList, function(item) {
              if (item.conditionType == 'ZW81' || item.conditionType == 'ZW82') {
                return item;
              }
            });

            var isZW = false;

            if ($scope.pricingConditions != undefined) {
              if ($scope.pricingConditions.conditionTypeCode == 'ZW81' || $scope.pricingConditions.conditionTypeCode == 'ZW82') {
                if (res1.data.data.cartList.length > 0) {
                  isZW = true;
                }
              }
            }

            if (zwFilter.length > 0 || isZW) {
              swal('แจ้งเตือน','สินค้าโปรโมชั่นพิเศษ ไม่สามารถสั่งซื้อรวมกับสินค้าอื่น <br> กรุณากดยืนยันเฉพาะเอกสารสั่งซื้อนี้','warning');
            } else {
              CartService.addCart(formData)
                .then(function(res2) {
                  if (res2.data.result == 'SUCCESS') {
                    swal('สำเร็จ', 'เพิ่มสินค้าลงในตะกร้าเรียบร้อย', 'success');
              
                    $scope.promotionDT = [];
                    $scope.promotionDT_Sel = [];
                    $scope.promotionFG = [];
                    $scope.promotionFG_Sel = [];
                    $scope.countPromotionSet = 0;
              
                    $scope.temps = {
                      promotionHD: null,
                      promotionDT: [],
                      promotionDT_Sel: []
                    };
                          
                    fetchPromotion();
                          
                    $scope.hideFreeGoodsAll = false;
                    $scope.hideFreeGoodsSel = false;
                          
                    $rootScope.$broadcast('updateCartInHeader');
                  }
                });
            }

            $scope.btnLoading = false;
          }
        });
    };

    $scope.edtPromotionSetValue = function() {
      if ($scope.input.promotionSetValue < 0) {
        $scope.input.promotionSetValue = 0;
      }

      if ($scope.promotionHD.promotionSetValue == 1) {
        if ($scope.input.promotionSetValue > $scope.promotionHD.promotionSetValue) {
          $scope.input.promotionSetValue = $scope.promotionHD.promotionSetValue;
        }
      }
    };

    $scope.addPromotionSetValue = function() {
      $scope.promotionSetValue = parseInt($scope.input.promotionSetValue);
      $scope.countPromotionSet += parseInt($scope.promotionSetValue);

      $scope.input.promotionSetValue = parseInt($scope.promotionSetValue);
    };

    /** Get Qty and Amount */
    $scope.getTotalQty = function() {
      var qty = 0;

      if ($scope.promotionDT_Sel.length > 0) {
        $scope.promotionDT_Sel.forEach(function(value) {
          qty += parseInt(value.qty);
        });
      }

      return qty;
    };

    $scope.getTotalBuyQty = function() {
      var qty = 0;

      if ($scope.promotionDT_Sel.length > 0) {
        $scope.promotionDT_Sel.forEach(function(value) {
          qty += parseInt((value.buyQty == 0) ? value.qty : value.buyQty);
        });
      }

      return qty;
    };

    $scope.getTotalAmount = function() {
      var amount = 0;

      $scope.promotionDT_Sel.forEach(function(value) {
        if ($scope.promotionHD.promotionType == 'Discount' && !$scope.freeGoods.length) {
          amount += (value.productData.productPrice * value.qty);
        } else {
          amount += (value.productData.productPrice * ((value.buyQty == 0) ? value.qty : value.buyQty));
        }
      });

      return amount;
    };
    
    $scope.getTotalFreeGoodQty = function() {
      var qty = 0;

      $scope.promotionFG_Sel.forEach(function(value) {
        qty += parseInt(value.qty);
      });
      
      return qty;
    };

    /**
     * Check Button
     */
    $scope.chkBtnCalculate = function() {
      var disable = false;

      if ($scope.promotionHD.isPromotionSet) {
        var qty = 0;
        var qty_Sel = 0;

        $scope.promotionDT.forEach(function(value) {
          qty += value.showQty * $scope.countPromotionSet;
        });

        $scope.promotionDT_Sel.forEach(function(value) {
          qty_Sel += value.qty;
        });
        
        if (!$scope.promotionDT_Sel.length.length && (qty != qty_Sel)) {
          disable = true;
        }
      } else {
        disable = ($scope.promotionDT_Sel.length > 0) ? false : true;
      }

      return disable;
    };

    $scope.chkBtnAddProduct = function(promotionDT) {
      var disable = false;
      
      if ($scope.promotionHD.isPromotionSet) {
        var limit = promotionDT.showQty * $scope.promotionSetValue;
        var total = 0;

        $scope.temps.promotionDT_Sel.forEach(function(value) {
          if (value.promotionDtId == promotionDT.promotionDtId) {
            total += value.qty;
          }
        });

        if (promotionDT.qty == 0 || (total >= limit)) {
          disable = true;
        }
      } else if ($scope.promotionHD.isPromotionSet && $scope.promotionSetValue == 0) {
        disable = true;
      } if (promotionDT.qty == 0) {
        disable = true;
      }

      return disable;
    };

    $scope.chkBtnAddCart = function() {
      if (($scope.promotionHD.promotionType == 'Free' && $scope.promotionHD.redeem != 'Y') || ($scope.promotionHD.promotionType == 'Discount' && $scope.freeGoods.length > 0)) {
        if ($scope.promotionFG_Sel.length == 0) {
          return true;
        }

        var fgQty = 0;
        var fgSelQty = 0;

        $scope.promotionFG.forEach(function(value) {
          fgQty += parseInt(value.showQty);
        });

        $scope.promotionFG_Sel.forEach(function(value) {
          fgSelQty += parseInt(value.qty);
        });

        if (fgQty != fgSelQty) {
          return true;
        }
      } else if ($scope.promotionHD.promotionType == 'Free' && $scope.promotionHD.redeem == 'Y') {
        if ($scope.temps.promotionHD != null) {
          if ($scope.temps.promotionHD.promotionNextId != null) {
            return true;
          } else if ($scope.temps.promotionHD.promotionNextId == null) {
            var chkProductSelExists = $filter('filter')($scope.promotionDT_Sel, function(item) {
              if (item.promotionId == $scope.temps.promotionHD.promotionId) {
                return item;
              }
            });
  
            if (chkProductSelExists.length == 0) {
              return true;
            }
          }
        } else {
          return true;
        }
      } else if ($scope.promotionHD.promotionType == 'Discount' && $scope.freeGoods.length == 0) {
        if ($scope.promotionDT_Sel.length == 0) {
          return true;
        }
      }

      return false;
    };

    $scope.getTotalSelFreeGoodQty = function() {
      var sum1 = 0;
      var sum2 = 0;

      $scope.promotionFG.forEach(function(value) {
        if (value.format != "SKU") {
          sum1 += parseInt(value.showQty);
        }
      });

      $scope.promotionFG_Sel.forEach(function(value) {
        if (value.format != "SKU") {
          sum2 += parseInt(value.qty);
        }
      });

      return sum1 - sum2;
    }

    /**
     * Function
     */
    function fetchPromotion() {
      $loading.start('promotion');

      PromotionService.getPromotionInfo($stateParams.promotionId)
        .then(function(res) {
          if (res.data.result == 'SUCCESS') {
            $scope.promotionHD = res.data.data.promotionHDList[0];
            $scope.promotionDT = res.data.data.promotionDTList;
            $scope.productInDt = res.data.data.productInDtList;
            $scope.btfInDt = res.data.data.btfInDtList;
            $scope.freeGoods = res.data.data.freeGoodsList;
            $scope.productInFreeGoods = res.data.data.productInFreeGoodsList;
            $scope.pricingConditions = res.data.data.pricingConditionList[0];
            $scope.promotionDealer = res.data.data.promotionDealerList;
            
            $scope.temps.promotionHD = $scope.promotionHD;
            
            setPromotionSetValue();
            altPromotionCountOrder();

            $scope.promotionDT.forEach(function(value) {
              value.editBtf = false;
              value.editSize = false;
              value.editColor = false;
              value.qty = 0;
            });
          
            setPromotion($scope.promotionDT);
            
          } else {
            $state.go('home');
          }
        })
        .then(function() {
          $scope.chkDealerExists = chkDealerExists();
        })
        .then(function() {
          if ($scope.promotionHD.promotionNextId != null) {
            fetchPromotionNext($scope.promotionHD.promotionNextId);
          }
        })
        .then(function() {
          $loading.finish('promotion');
        });
    }

    function fetchPromotionNext(promotionNextId) {
      var promotionHD;

      PromotionService.getPromotionInfo(promotionNextId)
        .then(function(res) {
          if (res.data.result == 'SUCCESS') {
            promotionHD = res.data.data.promotionHDList[0];
         
            res.data.data.freeGoodsList.forEach(function(value) {
              $scope.freeGoods.push(value);
            });

            res.data.data.productInFreeGoodsList.forEach(function(value) {
              $scope.productInFreeGoods.push(value);
            });
          }
        })
        .then(function() {
          if (promotionHD.promotionNextId != null) {
            fetchPromotionNext(promotionHD.promotionNextId);
          }
        });
    }

    function setPromotion(promotionDT) {
      var productInDt = null;

      promotionDT.forEach(function(value) {
        var qty = 0;

        value.stepCount = $scope.stepCount;

        switch (value.format) {
          case 'MG':
          case 'B':
            var btfInDt = getBtfInDt(value.promotionDtId);

            getSize(value.format, value, btfInDt.btf);
            getColor(value.format, value, btfInDt.btf);

            $loading.start('promotion');

            getProductInBtf(btfInDt.btfWeb, null).then(function(res) {
              value.productData = res;
            })
            .finally(function() {
              $loading.finish('promotion');
            });

            value.editBtf = true;
            value.editSize = true;
            value.editColor = true;

            value.productName = btfInDt.btfWebDescTh;
            value.btfInDt = btfInDt;

            qty = 0;

            if (value.minQty <= 300) {
              if (!$scope.promotionHD.checkStepBySKU) {
                qty = value.minQty;
              } else if ($scope.promotionHD.checkStepBySKU) {
                qty = value.minQty;
              }
            }
            
            value.qty = qty;
            
            if ($scope.promotionHD.isPromotionSet) {
              value.buyQty = qty;
            }
            break;
          case 'BTF':
            productInDt = getProductInDt(value.promotionDtId);

            getSize(value.format, value, null);
            getColor(value.format, value, null);

            value.editSize = true;
            value.editColor = true;
            
            value.productName = productInDt.btfWebDescTh;
            value.productData = productInDt;
            
            if (value.minQty <= 300) {
              if (!$scope.promotionHD.checkStepBySKU) {
                qty = value.minQty;
              } else if ($scope.promotionHD.checkStepBySKU) {
                qty = value.minQty;
              }
            }
            
            value.qty = qty;
            
            if ($scope.promotionHD.isPromotionSet) {
              value.showQty = qty;
            }
            break;
          case 'BTFS':
            productInDt = getProductInDt(value.promotionDtId);

            getSize(value.format, value, null);
            getColor(value.format, value, null);

            value.editColor = true;

            value.productName = productInDt.btfWebDescTh;
            value.productData = productInDt;
            
            if (value.minQty <= 300) {
              if (!$scope.promotionHD.checkStepBySKU) {
                qty = value.minQty;
              } else if ($scope.promotionHD.checkStepBySKU) {
                qty = value.minQty;
              }
            }
            
            value.qty = qty;

            if ($scope.promotionHD.isPromotionSet) {
              value.showQty = qty;
            }
            break;
          case 'SKU':
            productInDt = getProductInDt(value.promotionDtId);

            getSize(value.format, value, null);
            getColor(value.format, value, null);

            value.productName = productInDt.productNameTh;
            value.productData = productInDt;

            if (value.minQty <= 300) {
              if (!$scope.promotionHD.checkStepBySKU) {
                qty = value.minQty;
              } else if ($scope.promotionHD.checkStepBySKU) {
                qty = value.minQty;
              }
            }
            
            value.qty = qty;

            if ($scope.promotionHD.isPromotionSet) {
              value.showQty = qty;
            }
            break;
        }
      });
    }

    function setFreeGoods(type, promotionHD, promotionFG) {
      var productInFreeGoods = null;

      promotionFG.forEach(function(value) {
        switch (value.format) {
          case 'BTF':
            if ((type != 'next') ? $scope.promotionHD.checkStepBySKU : promotionHD.checkStepBySKU) {
              productInFreeGoods = getProductInDtByProductId(value.freeGoodsMatId);
            } else {
              productInFreeGoods = getProductInFreeGoods(value.format, value.freeGoodsId);
            }

            getSizeFreeGoods(value.format, value);
            getColorFreeGoods(value.format, value);
  
            value.editSize = true;
            value.editColor = true;

            if ((type != 'next') ? !$scope.promotionHD.checkStepBySKU : !promotionHD.checkStepBySKU) {
              value.editQty = true;
            }
  
            value.productName = productInFreeGoods.btfWebDescTh;
            value.productFreeGoodsData = productInFreeGoods;
            
            break;
          case 'BTFS':
            if ((type != 'next') ? $scope.promotionHD.checkStepBySKU : promotionHD.checkStepBySKU) {
              productInFreeGoods = getProductInDtByProductId(value.freeGoodsMatId);
            } else {
              productInFreeGoods = getProductInFreeGoods(value.format, value.freeGoodsId);
            }
    
            getSizeFreeGoods(value.format, value);
            getColorFreeGoods(value.format, value);

            value.editColor = true;

            if ((type != 'next') ? !$scope.promotionHD.checkStepBySKU : !promotionHD.checkStepBySKU) {
              value.editQty = true;
            }

            value.productName = productInFreeGoods.btfWebDescTh;
            value.productFreeGoodsData = productInFreeGoods;

            break;
          case 'SKU':
            if ((type != 'next') ? $scope.promotionHD.checkStepBySKU : promotionHD.checkStepBySKU) {
              productInFreeGoods = getProductInDtByProductId(value.freeGoodsMatId);
            } else {
              productInFreeGoods = getProductInFreeGoods(value.format, value.freeGoodsId);
            }

            getSizeFreeGoods(value.format, value);
            getColorFreeGoods(value.format, value);

            var productCode = productInFreeGoods.productCode;

            if (productCode.length == 18) {
              productInFreeGoods.productCode = productCode.substring(12);
            }

            value.productName = productInFreeGoods.productNameTh;
            value.productFreeGoodsData = productInFreeGoods;

            break;
        }        
      });
    }

    function getSize(format, promotionDT, btf) {
      if (format == 'MG' || format == 'B') {
        $loading.start('promotion');

        ProductService.getOneProduct($scope.customerInfo.customerId, btf)
          .then(function(res) {
            if (res.data.result == 'SUCCESS') {
              res.data.data.productList.forEach(function(value) {
                var chkSize = $filter('filter')($scope.sizes, function(item) {
                  return (item.btfCode == btf) && (item.productCode == value.productCode);
                })[0] != undefined;
                
                if (!chkSize) {
                  $scope.sizes.push({
                    sizeCode: value.sizeCode,
                    sizeName: value.sizeName,
                    btfCode: btf,
                    promotionDtId: promotionDT.promotionDtId,
                    productId: value.productId,
                    productCode: value.productCode
                  });
                }
              });
            }
          })
          .finally(function() {
            $loading.finish('promotion');
          });
      } else {
        $scope.productInDt.forEach(function(value) {
          if (promotionDT.promotionDtId == value.promotionDtId) {
            $scope.sizes.push({
              sizeCode: value.sizeCode,
              sizeName: value.sizeName,
              btfCode: value.btf,
              promotionId: promotionDT.promotionId,
              productId: value.productId,
              productCode: value.productCode
            });
          }
        });
      }
    }

    function getColor(format, promotionDT, btf) {
      if (format == 'MG' || format == 'B') {
        $loading.start('promotion');

        ProductService.getOneProduct($scope.customerInfo.customerId, btf)
          .then(function(res) {
            if (res.data.result == 'SUCCESS') {
              res.data.data.productList.forEach(function(value) {
                var chkColor = $filter('filter')($scope.colors, function(item) {
                  return (item.btfCode == btf) && (item.productCode == value.productCode);
                })[0] != undefined;

                if (!chkColor) {
                  $scope.colors.push({
                    colorCode: value.colorCode,
                    rgbCode: value.rgbCode,
                    btfCode: btf,
                    promotionDtId: promotionDT.promotionDtId,
                    productId: value.productId,
                    productCode: value.productCode,
                    sizeCode: value.sizeCode
                  });
                }
              });
            }
          })
          .finally(function() {
            $loading.finish('promotion');
          });
      } else {
        $scope.productInDt.forEach(function(value) {
          if (promotionDT.promotionDtId == value.promotionDtId) {
            $scope.colors.push({
              colorCode: value.colorCode,
              rgbCode: value.rgbCode,
              btfCode: value.btf,
              productCode: value.productCode,
              sizeCode: value.sizeCode,
              promotionDtId: value.promotionDtId
            });
          }
        });
      }
    }

    function getSizeFreeGoods(format, promotionFG) {
      $scope.productInFreeGoods.forEach(function(value) {
        if (promotionFG.freeGoodId == value.freeGoodId) {
          $scope.sizeFreeGoods.push({
            sizeCode: value.sizeCode,
            sizeName: value.sizeName,
            btfCode: value.btf,
            freeGoodsId: promotionFG.freeGoodsId,
            productId: value.productId,
            productCode: value.productCode
          });
        }
      });
    }

    function getColorFreeGoods(format, promotionFG) {
      var colors = [];

      $scope.promotionDT_Sel.forEach(function(value) {
        colors.push({
          colorCode: value.productData.colorCode
        });
      });

      $scope.productInFreeGoods.forEach(function(value) {
        if (promotionFG.freeGoodId == value.freeGoodId) {
          colors.forEach(function(color) {
            if (color.colorCode == value.colorCode) {
              $scope.colorFreeGoods.push({
                colorCode: value.colorCode,
                rgbCode: value.rgbCode,
                btfCode: value.btf,
                freeGoodsId: promotionFG.freeGoodsId,
                productId: value.productId,
                productCode: value.productCode,
                sizeCode: value.sizeCode
              });
            }
          });
        }
      });
    }

    function chkPromotionSet() {
      var totalSetQty = 0;
      var totalSetQty_Sel = 0;

      $scope.promotionDT.forEach(function(value) {
        totalSetQty += parseInt(value.showQty * $scope.promotionSetValue);
      });

      $scope.temps.promotionDT_Sel.forEach(function(value) {
        totalSetQty_Sel += value.qty;
      });

      if (totalSetQty == totalSetQty_Sel) {
        return true;
      } else {
        return false;
      }
    }

    function chkDealerExists() {
      return $filter('filter')($scope.promotionDealer, function(item) {
        if (item.customerCode == $scope.customerInfo.customerCode) {
          return item;
        }
      })[0] != undefined;
    }

    function calculatePromotionNext(promotionNextId) {
      $loading.start('freegoods');
      $scope.stepCount++;
      
      var cartList = [];
      var promotionHD;
      var result = '';

      $scope.promotionDT_Sel.forEach(function(value) {
        cartList.push({
          customerId: $scope.customerInfo.customerId,
          productId: value.productData.productId,
          qty: value.qty
          //qty: ($scope.promotionHD.checkStepBySKU) ? ((value.buyQty == 0) ? value.qty : value.buyQty) : value.qty
        });
      });

      var formData = {
        promotionId: promotionNextId,
        cartList: cartList
      };
 
      PromotionService.getPromotionInfo(promotionNextId)
        .then(function(res) {          
          if (res.data.result == 'SUCCESS') {
            promotionHD = res.data.data.promotionHDList[0];
            
            $scope.promotionId = promotionHD.promotionId;

            if ($scope.promotionHD.redeem == 'Y') {
              var chkPromotionDtExists = $filter('filter')($scope.promotionDT, function(item) {
                if (item.promotionId == promotionHD.promotionId) {
                  return item;
                }
              });

              if (chkPromotionDtExists.length < res.data.data.promotionDTList.length) {
                swal('แจ้งเตือน', 'คุณมีสิทธิ์แลกซื้อรายการสินค้าตามเงื่อนไขโปรโมชั่น', 'warning');

                res.data.data.promotionDTList.forEach(function(value) {
                  value.redeem = 'Y';
                  
                  $scope.promotionDT.push(value);
                });

                res.data.data.productInDtList.forEach(function(value) {
                  $scope.productInDt.push(value);
                })
                
                res.data.data.btfInDtList.forEach(function(value) {
                  $scope.btfInDt.push(value);
                });

                setPromotion($scope.promotionDT);
            
                $scope.temps.promotionHD = promotionHD;
              }              
            }
          }
        })
        .then(function() {
          PromotionService.chkPromotionValidate(formData)
            .then(function(res) {
              result = res.data.result;

              if (res.data.result == 'SUCCESS') {
                if (promotionHD.promotionType != 'Discount') {                  
                  res.data.freeGoodsList.forEach(function(value) { 
                    if (value.validFlag) {
                      var freeGoods = angular.copy($filter('filter')($scope.freeGoods, { freeGoodsId: value.freeGoodsId })[0]);
                    
                      if (freeGoods != undefined) {
                        freeGoods.editBtf = false;
                        freeGoods.editSize = false;
                        freeGoods.editColor = false;
                        freeGoods.qty = value.freeGoodsQty;
                        freeGoods.showQty = value.freeGoodsQty;
                        freeGoods.freeGoodsMatId = value.freeGoodsMatId;
    
                        if (promotionHD.isPromotionSet) {
                          freeGoods.qty = value.freeGoodsQty * $scope.input.promotionSetValue;
                          freeGoods.showQty = value.freeGoodsQty * $scope.input.promotionSetValue;
                        } else {
                          freeGoods.qty = value.freeGoodsQty;
                          freeGoods.showQty = value.freeGoodsQty;
                        }
    
                        if (value.freeGoodsQty > 0) {
                          $scope.promotionFG.push(freeGoods);

                          if (freeGoods.format === 'SKU') {
                            $scope.promotionFG_Sel.push(freeGoods);
                          }
                        }
                      }
    
                      setFreeGoods('next', promotionHD, $scope.promotionFG);
                    }
                  });

                  // $scope.promotionFG.forEach(function(value) {
                  //   if (value.promotionId != $scope.promotionHD.promotionId) {
                  //     $scope.addFreeGoods(value);
                  //   }
                  // });
                }
              }
            })
            .then(function() {              
              if (promotionHD.promotionNextId != null && result == 'SUCCESS') {
                calculatePromotionNext(promotionHD.promotionNextId);
              } 

              $loading.finish('freegoods');
            });
        });
    }

    /**
     * Find Data
     */
    function getProductInDt(promotionDtId) {
      return $filter('filter')($scope.productInDt, function(item) {
        return item.promotionDtId == promotionDtId;
      })[0];
    }

    function getBtfInDt(promotionDtId) {
      return $filter('filter')($scope.btfInDt, function(item) {
        return item.promotionDtId == promotionDtId;
      })[0];
    }

    function getProductInBtf(btf, productCode) {
      return ProductService.getOneProduct($scope.customerInfo.customerId, btf)
        .then(function(res) {
          if (res.data.result == 'SUCCESS') {
            if (productCode == null) {
              return res.data.data.productList[0];
            } else {
              return $filter('filter')(res.data.data.productList, { productCode: productCode })[0];
            }
          }
        });
    }

    function getProductInDtByProductId(productId) {
      return $filter('filter')($scope.productInDt, function(item) {
        return item.productId == productId;
      })[0];
    }

    function getProductInFreeGoods(format, freeGoodsId) {
      return $filter('filter')($scope.productInFreeGoods, function(item) {
        return (format == 'SKU') ? item.freeGoodsId == freeGoodsId : item.freeGoodsId == freeGoodsId && item.colorCode == $scope.promotionDT_Sel[0].productData.colorCode;
      })[0];
    }

    function getTotalQtyForDummy() {
      var qty = 0;

      if ($scope.promotionDT_Sel.length > 0) {
        $filter('filter')($scope.promotionDT_Sel, function(item) {
          if (item.promotionId == $scope.temps.promotionHD.promotionId) {
            return item;
          }
        })
        .forEach(function(value) {
          qty += parseInt(value.qty);
        });
      }

      return qty;
    }

    function getTotalAmountForDummy() {
      var amount = 0;

      $filter('filter')($scope.promotionDT_Sel, function(item) {
        if (item.promotionId == $scope.temps.promotionHD.promotionId) {
          return item;
        }
      })
      .forEach(function(value) {
        if ($scope.temps.promotionHD.promotionType == 'Discount' && !$scope.freeGoods.length) {
          amount += (value.productData.productPrice * value.qty);
        } else {
          amount += (value.productData.productPrice * ((value.buyQty == 0) ? value.qty : value.buyQty));
        }
      });

      return amount;
    }

    function getLimitPromotionSet(promotionDT) {
      var limit = promotionDT.showQty * $scope.promotionSetValue;
      var total = 0;

      $scope.temps.promotionDT_Sel.forEach(function(value) {
        if (value.promotionDtId == promotionDT.promotionDtId) {
          total += value.qty;
        }
      });

      return limit - total;
    }

    function chkCustomerBlock() {
      CustomerService.customerBlock('00' + $scope.customerInfo.customerCode)
        .then(function(res) {
          if (res.data.result == 'SUCCESS') {
            if (res.data.data.block == 'X') {
              $state.go('home');
            }
          }
        });
    }

    function altPromotionCountOrder() {
      PromotionService.getPromotionOrderCount($scope.promotionHD.promotionId, $scope.customerInfo.customerId)
        .then(function(res) {
          if (res.data.result == 'SUCCESS') {
            $scope.countOrder = res.data.data.dealerreceivehd[0].countOrder;

            if ($scope.countOrder) {
              return swal('แจ้งเตือน', 'คุณได้ใช้สิทธิโปรโมชั่นเรียบร้อยแล้ว', 'warning');
            }
          }
        });
    }

    function setPromotionSetValue() {
      CartService.getCart($scope.customerInfo.customerId, $scope.userInfo.userName)
        .then(function(res) {
          if (res.data.result == 'SUCCESS') {
            var isProductExists = $filter('filter')(res.data.data.cartList, function(item) {
              if (item.promotionId == $scope.promotionHD.promotionId) {
                return item;
              }
            });

            if ($scope.promotionHD.isPromotionSet && $scope.promotionHD.promotionSetValue == 1) {
              if (isProductExists.length > 0) {
                $scope.promotionSetValue = 0;
                $scope.input.promotionSetValue = 0;
                $scope.hideAddPromotionSet = true;
              } else {
                $scope.hideAddPromotionSet = false;
              }
            }
          }
        });
    }
  }
}());