(function() {
  'use strict';

  angular
    .module('app')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider) {
    $stateProvider
      .state('promotion', {
        url: '/promotion/{promotionId}',
        templateUrl: 'app/components/promotion/view/index.html',
        controller: 'PromotionController',
        controllerAs: 'vm',
        data: {
          requiresLogin: true
        }
      });
  }

})();
