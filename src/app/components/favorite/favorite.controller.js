(function() {
  'use strict';

  angular
    .module('app')
    .controller('FavoriteController', FavoriteController);

  /** @ngInject */
  function FavoriteController($scope, $state, $localStorage, $loading, FavoriteService) {
    init();

    function init() {
      /** define */
      $scope.userInfo = $localStorage.userInfo;
      $scope.customerInfo = $localStorage.customerInfo;
      
      if (($scope.customerInfo == undefined || angular.equals({}, $scope.customerInfo)) && $scope.userInfo.userTypeDesc == 'Multi') {
        $state.go('store');
      }

      /** pagination */
      $scope.pageSize = 8;
      $scope.maxSize = 5;
      $scope.currentPage = 1;

      $loading.setDefaultOptions({ 
        text: 'กำลังโหลด'
      });

      fetchProducts();
    }

    $scope.onRemoveFavorite = function(product) {
      var formData = {
        customerId: $scope.customerInfo.customerId,
        userName: $scope.userInfo.userName,
        btfCode: product.btf
      };

      FavoriteService.removeFavorite(formData)
        .then(function(res) {
          if (res.data.result == 'SUCCESS') {
            var index = $scope.products.indexOf(product);
            $scope.products.splice(index, 1);
            swal('สำเร็จ', 'ลบรายการโปรดเรียบร้อย', 'success');
          } else {
            swal('ไม่สำเร็จ', 'ไม่สามารถลบรายการโปรดได้', 'warning');
          }
        });
    };

    /**
     * Function
     */
    function fetchProducts() {
      $loading.start('favorite');

      FavoriteService.getFavorite($scope.customerInfo.customerId)
        .then(function(res) {
          if (res.data.result == 'SUCCESS') {
            $scope.products = res.data.data.favoriteList;
          }
        })
        .finally(function() {
          $loading.finish('favorite');
        });
    }
  }

}());
