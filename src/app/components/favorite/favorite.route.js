(function() {
  'use strict';

  angular
    .module('app')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider) {
    $stateProvider
      .state('favorite', {
        url: '/favorite',
        templateUrl: 'app/components/favorite/view/index.html',
        controller: 'FavoriteController',
        controllerAs: 'vm',
        data: {
          requiresLogin: true,
          menuCode: '01300'
        }
      });
  }

})();