(function() {
  'use strict';

  angular
    .module('app')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider) {
    $stateProvider
      .state('home', {
        url: '/home',
        templateUrl: 'app/components/home/view/index.html',
        controller: 'HomeController',
        controllerAs: 'vm',
        data: {
          requiresLogin: true,
          menuCode: '01100'
        }
      });
  }

})();
