(function() {
  'use strict';

  angular
    .module('app')
    .controller('HomeController', HomeController);

  /** @ngInject */
  function HomeController($scope, $state, $rootScope, $localStorage, $loading, MarketingService, PromotionService, CustomerService) {
    init();

    function init() {
      /** define */
      $scope.config = $localStorage.config;
      $scope.userInfo = $localStorage.userInfo;
      $scope.customerInfo = $localStorage.customerInfo;
      $scope.marketings = [];
      $scope.brands = [];
      $scope.marketingSelects = [];
      $scope.marketingModels = [];

      $scope.multiSelectTexts = {
        checkAll: 'เลือกทั้งหมด',
        uncheckAll: 'ยกเลิกทั้งหมด',
        dynamicButtonTextSuffix: 'รายการ',
        buttonDefaultText: 'เลือกกลุ่มผลิตภัณฑ์'
      };

      /** pagination */
      $scope.pageSize = 6;
      $scope.maxSize = 5;
      $scope.currentPage = 1;

      $scope.promotions = [];

      $scope.multiSelectEvents = {
        onItemSelect: function() {
          fetchPromotion();
        },
        onItemDeselect: function() {
          fetchPromotion();
        },
        onSelectAll: function() {
          fetchPromotion();
        },
        onDeselectAll: function() {
          fetchPromotion();
        }
      };

      if (($scope.customerInfo == undefined || angular.equals({}, $scope.customerInfo)) && $scope.userInfo.userTypeDesc == 'Multi') {
        $state.go('store');
      }
      
      $loading.setDefaultOptions({ 
        text: 'กำลังโหลด'
      });
      
      /** fetch date */
      fetchMarketings();

      if ($scope.config.promotionHide != 'Y') {
        fetchPromotion();
      }
    }

    $scope.toPromotion = function(promotion) {
      $loading.start('promotion');

      CustomerService.customerBlock('00' + $scope.customerInfo.customerCode)
        .then(function(res) {
          if (res.data.result == 'SUCCESS') {
            if (res.data.data.block == 'X') {
              swal('ไม่สำเร็จ', 'ท่านไม่สามารถสั่งซื้อสินค้า online <br> บนระบบ TOA e-Ordering ได้ <br> เนื่องจาก ติดปัญหาการชำระบัญชี <br><br> กรุณาติดต่อ <br> ผู้แทนขาย หรือ ฝ่ายสินเชื่อและลูกหนี้ <br> บริษัท ทีโอเอ เพ้นท์ (ประเทศไทย) จำกัด (มหาชน)', 'error');
            } else {
              $state.go('promotion', { promotionId: promotion.promotionId });
            }
          } else {
            swal('ไม่สำเร็จ', 'เกิดข้อผิดพลาด', 'error');
          }
        })
        .finally(function() {
          $loading.finish('promotion');
        });
    };
    
    /**
     * Function
     */
    function fetchMarketings() {
      $loading.start('marketing');

      MarketingService.getMarketing($scope.customerInfo.customerId)
        .then(function(res) {
          $scope.marketings = res.data.data.marketingList;
          $scope.brands = res.data.data.brandList;

          res.data.data.marketingList.forEach(function(value) {
            $scope.marketingSelects.push({
              id: value.marketingCode,
              label: value.marketingDesc
            });
          });
        })
        .finally(function() {
          $loading.finish('marketing');
        });
    }

    function fetchPromotion() {
      $loading.start('promotion');

      var marketingCodeList = [];

      $scope.marketingModels.forEach(function(value) {
        marketingCodeList.push(value.id);
      });

      PromotionService.getPromotion($scope.customerInfo.customerId, marketingCodeList)
        .then(function(res) {
          if (res.data.result == 'SUCCESS') {
            $scope.promotions = res.data.data.promotionHDList;
          }
        })
        .finally(function() {
          $loading.finish('promotion');
        });
    }
  }

}());
