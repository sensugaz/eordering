(function() {
  'use strict';

  angular
    .module('app')
    .service('HistoryService', HistoryService);

  /** @ngInject */
  function HistoryService(API_URL, $http, $q) {
    this.getProduct = function(customerId) {
      var def = $q.defer();

      $http.get(API_URL + 'ProductInOrder', { params: { customerId: customerId }})
        .then(function(res) {
          def.resolve(res);
        })
        .catch(function() {
          def.reject();
        });

      return def.promise;
    };
  }
}());
