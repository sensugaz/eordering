(function() {
  'use strict';

  angular
    .module('app')
    .controller('HistoryController', HistoryController);

  /** @ngInject */
  function HistoryController($scope, $state, $localStorage, $loading, HistoryService) {
    init();

    function init() {
      $scope.userInfo = $localStorage.userInfo;
      $scope.customerInfo = $localStorage.customerInfo;
      //$scope.products = [];
      if (($scope.customerInfo == undefined || angular.equals({}, $scope.customerInfo)) && $scope.userInfo.userTypeDesc == 'Multi') {
        $state.go('store');
      }
     
      /** pagination */
      $scope.pageSize = 10;
      $scope.maxSize = 5;
      $scope.currentPage = 1;
      
      $loading.setDefaultOptions({ 
        text: 'กำลังโหลด'
      });

      fetchProduct();
    }

    /**
     * Function
     */
    function fetchProduct() {
      $loading.start('history');

      HistoryService.getProduct($scope.customerInfo.customerId)
        .then(function(res) {
          if (res.data.result == 'SUCCESS') {
            //$scope.products = res.data.data.productInOrderList;
            var product = [];
            
            res.data.data.productInOrderList.forEach(function(value) {
              var _ = product.filter(function(item) {
                if ((item.btfWeb == value.btfWeb) && (item.documentDate == value.documentDate)) {
                  return item;
                }
              })[0];

              if (_ == undefined) {
                product.push(value);
              }
            });

            $scope.products = product;
          }
        })
        .finally(function() {
          $loading.finish('history');
        });
    }

  }

}());
