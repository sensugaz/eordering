(function() {
  'use strict';

  angular
    .module('app')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider) {
    $stateProvider
      .state('history', {
        url: '/history',
        templateUrl: 'app/components/history/view/index.html',
        controller: 'HistoryController',
        controllerAs: 'vm',
        data: {
          requiresLogin: true,
          menuCode: '01400'
        }
      });
  }

})();
