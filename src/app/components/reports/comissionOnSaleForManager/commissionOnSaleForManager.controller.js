(function() {
  'use strict';

  angular
    .module('app')
    .controller('CommissionOnSaleForManager', CommissionOnSaleForManager);

  /** @ngInject */
  function CommissionOnSaleForManager($scope, $state, $localStorage, $loading, $filter, ReportService) {
    init();

    function init() {
      $scope.userInfo = $localStorage.userInfo;
      $scope.customerInfo = $localStorage.customerInfo;
      
      if (($scope.customerInfo == undefined || angular.equals({}, $scope.customerInfo)) && $scope.userInfo.userTypeDesc == 'Multi') {
        $state.go('store');
      }

      /** pagination */
      $scope.pageSize = 2;
      $scope.maxSize = 5;
      $scope.currentPage = 1;

      $scope.reports = [];

      $loading.setDefaultOptions({ 
        text: 'กำลังโหลด'
      });

      $scope.input = {
        year: null,
        quarter: null,
        month: null,
        salesGroup: null
      };

      $scope.years = [];
      $scope.quarters = [];
      $scope.months = [];
      $scope.salesGroups = [];

      /** select2 */
      $('.select2').select2();

      /** fetch data */
      fetchFilter();
    }

    $scope.setQuarter = function(year, quarters) {
      $scope.input.quarter = $filter('filter')(quarters, function(item) {
        return year == item.year
      })[0].quarter;
      
      $scope.setMonth(year, $scope.input.quarter, $scope.months);
    };

    $scope.setMonth = function(year, quarter, months) {
      $scope.input.month = $filter('filter')(months, function(item) {
        return year == item.year && quarter == item.quarter
      })[0].month;
    };

    $scope.showReport = function(isValid) {
      if (isValid) {
        var formData = {
          YEAR: $scope.input.year,
          QUARTER: $scope.input.quarter,
          MONTH: $scope.input.month,
          SALES_GROUP: $scope.input.salesGroup
        };

        $loading.start('report');

        ReportService.getReportComissionOnSaleForManager(formData)
          .then(function(res) {
            if (res.data.data.length > 0) {
              var data = [];

              res.data.data.forEach(function(value) {
                var filter = $filter('filter')(data, function(item) {
                  if (item.salesGroupKey == value.salesGroupKey) {
                    return item;
                  }
                });

                if (!filter.length) {
                  data.push({
                    salesGroup: value.salesGroup,
                    salesGroupKey: value.salesGroupKey,
                    year: value.year,
                    month: value.month,
                    quarter: value.quarter,
                    data: [
                      {
                        customerAreaKey: value.customerAreaKey,
                        customerAreaName: value.customerAreaName,
                        teamSales: value.teamSales,
                        targetAmt: {
                          decorative: value.targetAmtByDecorative,
                          none_decorative: value.targetAmtByNoneDecorative,
                          other: value.targetAmtByOther,
                          month: value.targetAmtByMonth,
                          quarter: value.targetAmtByQuarter
                        },
                        netAmt: {
                          decorative: value.netAmtByDecorative,
                          none_decorative: value.netAmtByNoneDecorative,
                          other: value.netAmtByOther,
                          month: value.netAmtByMonth,
                          quarter: value.netAmtByQuarter
                        }
                      }
                    ],
                    targetAmt: {
                      decorative: 0,
                      none_decorative: 0,
                      other: 0,
                      month: 0,
                      quarter: 0
                    },
                    netAmt: {
                      decorative: 0,
                      none_decorative: 0,
                      other: 0,
                      month: 0,
                      quarter: 0
                    }
                  });
                } else {
                  var index = data.findIndex(function(item) {
                    if (item.salesGroupKey == value.salesGroupKey) {
                      return item;
                    }
                  });
  
                  data[index].data.push({
                    customerAreaKey: value.customerAreaKey,
                    customerAreaName: value.customerAreaName,
                    teamSales: value.teamSales,
                    targetAmt: {
                      decorative: value.targetAmtByDecorative,
                      none_decorative: value.targetAmtByNoneDecorative,
                      other: value.targetAmtByOther,
                      month: value.targetAmtByMonth,
                      quarter: value.targetAmtByQuarter
                    },
                    netAmt: {
                      decorative: value.netAmtByDecorative,
                      none_decorative: value.netAmtByNoneDecorative,
                      other: value.netAmtByOther,
                      month: value.netAmtByMonth,
                      quarter: value.netAmtByQuarter
                    }
                  });
                }
              });

              data.forEach(function(value) {
                value.data.forEach(function(value2) {
                  value.targetAmt.decorative += value2.targetAmt.decorative;
                  value.targetAmt.none_decorative += value2.targetAmt.none_decorative;
                  value.targetAmt.other += value2.targetAmt.other;
                  value.targetAmt.month += value2.targetAmt.month;
                  value.targetAmt.quarter += value2.targetAmt.quarter;
  
                  value.netAmt.decorative += value2.netAmt.decorative;
                  value.netAmt.none_decorative += value2.netAmt.none_decorative;
                  value.netAmt.other += value2.netAmt.other;
                  value.netAmt.month += value2.netAmt.month;
                  value.netAmt.quarter += value2.netAmt.quarter;
                });
              });  
              
              $scope.reports = data;
            } else {
              swal('แจ้งเตือน', 'ไม่พบข้อมูล', 'warning');
            }
          })
          .finally(function() {
            $loading.finish('report');
          });
      }
    };

    /**
     * Function
     */
    function fetchFilter() {
      $loading.start('report');

      ReportService.getReportComissionOnSaleFilter()
        .then(function(res) {
          if (res.data.result == 'SUCCESS') {
            $scope.years = res.data.yearList;
            $scope.months = res.data.monthList;
            $scope.quarters = res.data.quarterList;
            $scope.salesGroups = res.data.salesGroupList;

            $scope.input.year = $scope.years[0].year;
            $scope.input.salesGroup = 'ALL';

            $scope.setQuarter($scope.input.year, $scope.quarters);
            $scope.setMonth($scope.input.year, $scope.input.quarter, $scope.months);
          }
        })
        .finally(function() {
          $loading.finish('report');
        });
    }
  }

}());