(function() {
  'use strict';

  angular
    .module('app')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider) {
    $stateProvider
      .state('commissionOnSaleForManager', {
        url: '/report/commissionOnSale/manager',
        templateUrl: 'app/components/reports/comissionOnSaleForManager/view/index.html',
        controller: 'CommissionOnSaleForManager',
        controllerAs: 'vm',
        data: {
          requiresLogin: true,
          //menuCode: '01702'
        }
      });
  }

})();