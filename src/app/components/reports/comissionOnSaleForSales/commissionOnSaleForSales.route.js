(function() {
  'use strict';

  angular
    .module('app')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider) {
    $stateProvider
      .state('commissionOnSaleForSales', {
        url: '/report/commissionOnSale/sales',
        templateUrl: 'app/components/reports/comissionOnSaleForSales/view/index.html',
        controller: 'CommissionOnSaleForSales',
        controllerAs: 'vm',
        data: {
          requiresLogin: true,
          //menuCode: '01702'
        }
      });
  }

})();