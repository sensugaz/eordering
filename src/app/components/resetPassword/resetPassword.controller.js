(function() {
  'use strict';

  angular
    .module('app')
    .controller('ResetPasswordController', ResetPasswordController);

  /** @ngInject */
  function ResetPasswordController($scope, $state, $stateParams, AuthService) {
    init();

    function init() {
      /** define */
      $scope.userInfo = {};
      $scope.btnLoading = false;
      $scope.input = {};

      if ($stateParams.key == undefined) {
        $state.go('login');
      }

      AuthService.forgotPassword({ userName: '', email: '', key: $stateParams.key })
        .then(function(res) {
          if (res.data.result == 'SUCCESS') {
            $scope.userInfo = res.data.data.userInfo;
          } else {
            swal('แจ้งเตือน', 'คีย์นี้ถูกใช้ไปแล้วกรุณากดลืมรหัสผ่านใหม่', 'warning');
            $state.go('forgotPassword');
          }
        });
    }

    $scope.onChangePassword = function(isValid) {
      if (isValid) {
        $scope.btnLoading = true;

        var formData = {
          userName: $scope.userInfo.userName,
          newPassword: $scope.input.password,
          resetPasswordKey: $scope.userInfo.resetPasswordKey,
          email: '',
          password: ''
        };

        AuthService.changePassword(formData)
          .then(function(res) {
            if (res.data.result == 'SUCCESS') {
              swal('สำเร็จ', 'เปลี่ยนรหัสผ่านสำเร็จ', 'success');
              
            } else {
              switch (res.data.validateCode) {
                case '001':
                  swal('ไม่สำเร็จ', 'ไม่รู้ชื่อผู้ใช้งาน', 'warning');
                  break;
                case '002':
                  swal('ไม่สำเร็จ', 'คีย์ไม่ถูกต้อง', 'warning');
                  break;
              }
            }

            $scope.btnLoading = false;
            $state.go('login');
          });
      }
    };

  }

}());
