(function() {
  'use strict';

  angular
    .module('app')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider) {
    $stateProvider
      .state('resetPassword', {
        url: '/reset_password?key',
        templateUrl: 'app/components/resetPassword/view/index.html',
        controller: 'ResetPasswordController',
        controllerAs: 'vm',
        data: {
          requiresLogin: false
        }
      });
  }

})();
