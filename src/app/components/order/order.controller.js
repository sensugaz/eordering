(function() {
  'use strict';

  angular
    .module('app')
    .controller('OrderController', OrderController);

  /** @ngInject */
  function OrderController($scope, $rootScope, $state, $filter, $localStorage, $uibModal, $loading, OrderService) {
    init();

    function init() {
      /** define */
      $scope.userInfo = $localStorage.userInfo;
      $scope.customerInfo = $localStorage.customerInfo;
      $scope.orders = [];
      $scope.startDate = moment().subtract($localStorage.config.weborderdaterang, 'days');
      $scope.endDate = moment();
    
      if (($scope.customerInfo == undefined || angular.equals({}, $scope.customerInfo)) && $scope.userInfo.userTypeDesc == 'Multi') {
        $state.go('store');
      }
      
      fetchOrder();      
    }
     
    $scope.updateOrders = function() {
      fetchOrder();
    }

    $scope.setYearCollapsed = function(year) {
      year.isCollapsed = !year.isCollapsed;

      if (year.isCollapsed == true) {
        if (year.months.length > 0) {
          year.months.forEach(function(value) {
            value.isCollapsed = true;
          });
        }
      }
    };

    $scope.setMonthCollapsed = function(month) {
      month.isCollapsed = !month.isCollapsed;
    };

    $scope.setBillCollapsed = function(order) {
      order.bill.isCollapsed = !order.bill.isCollapsed;

      if (order.bill.isCollapsed == false) {
        fetchOrderBilling(order);
      }
    };

    $scope.openModalOrderInvoice = function(order) {
      var modalInstance = $uibModal.open({
        controller: 'OrderInvoiceController',
        templateUrl: 'app/modals/orderInvoice/view/index.html',
        windowClass: 'order',
        resolve: {
          orderId: function() {
            return order.orderId
          }
        }
      });

      modalInstance.result.catch(function() { });
    };

    $scope.openModalOrderDetail = function(order) {
      var modalInstance = $uibModal.open({
        controller: 'OrderDetailController',
        templateUrl: 'app/modals/orderDetail/view/index.html',
        windowClass: 'order',
        resolve: {
          orderId: function() {
            return order.orderId
          },
          saleOrderNumber: function() {
            return order.salesOrderNumber;
          }
        }
      });

      modalInstance.result.catch(function() { });
    };


    $scope.openModalOrderStatus = function(order) {
      var modalInstance = $uibModal.open({
        controller: 'OrderStatusController',
        templateUrl: 'app/modals/orderStatus/view/index.html',
        windowClass: 'order',
        resolve: {
          orderId: function() {
            return order.orderId
          },
          saleOrderNumber: function() {
            return order.salesOrderNumber;
          }
        }
      });

      modalInstance.result.catch(function() { });
    };

    $scope.openModalOrderBilling = function(order) {
      var modalInstance = $uibModal.open({
        controller: 'OrderBillingController',     
        templateUrl: 'app/modals/orderBilling/view/index.html',        
        windowClass: 'order',
        resolve: {
          orderId: function() {
            return order.orderId
          },
          saleOrderNumber: function() {
            return order.salesOrderNumber;
          }
        }
      });

      modalInstance.result.catch(function() { });
    };

    $scope.openModalOrderTaxno = function(order, bill) {
      var modalInstance = $uibModal.open({
        controller: 'OrderTaxnoController',     
        templateUrl: 'app/modals/orderTaxno/view/index.html',        
        windowClass: 'order',
        resolve: {
          orderId: function() {
            return order.orderId
          },
          billNo: function() {
            return bill.billno;
          },
          saleOrderNumber: function() {
            return order.salesOrderNumber;
          }
        }
      });

      modalInstance.result.catch(function() { });
    };

    /**
     * Functions
     */
    function fetchOrder() {
      $loading.start('orders');

      OrderService.getOrderPrecess($scope.customerInfo.customerId, $scope.startDate.format(), $scope.endDate.format())
        .then(function(res) {
          if (res.data.result == 'SUCCESS') {
            $scope.orders = [];

            /** ปี */
            res.data.data.orderProcessList.forEach(function(value) {
              var year = moment(value.createDate).format('Y');

              var chkYearExists = $filter('filter')($scope.orders, function(item) {
                if (item.year == year) {
                  return item;
                }
              })[0] != undefined;

              if (chkYearExists == false) {
                $scope.orders.push({ 
                  year: year, 
                  isCollapsed: true,
                  months: [] });
              }
            });

            /** เดือน */
            res.data.data.orderProcessList.forEach(function(value) {
              var year = moment(value.createDate).format('Y');
              var month = moment(value.createDate).format('M');

              var index = $scope.orders.findIndex(function(item) {
                if (item.year == year) {
                  return item;
                }
              });
              
              var chkMonthExists = $filter('filter')($scope.orders[index].months, function(item) {
                if (item.month == month) {
                  return item;
                }
              })[0] != undefined;

              if (chkMonthExists == false) {
                $scope.orders[index].months.push({ 
                  month: month, 
                  isCollapsed: true,
                  name: getMonthNameTH(month),
                  orders: []
                });
              }
            });

            /** รายการคำสั่งซื้อ */
            res.data.data.orderProcessList.forEach(function(value) {
              var year = moment(value.createDate).format('Y');
              var month = moment(value.createDate).format('M');

              var idxy = $scope.orders.findIndex(function(item) {
                if (item.year == year) {
                  return item;
                }
              });

              var idxm = $scope.orders[idxy].months.findIndex(function(item) {
                if (item.month == month) {
                  return item;
                }
              });

              /** ถ้ายกเลิกรายการให้จำนวนเงินเป็น 0 */
              if (value.rejectHStatus == 'C') {
                value.netAmount = 0;
              }  

              value.bill = {
                isCollapsed: true,
                bills: []
              };

              $scope.orders[idxy].months[idxm].orders.push(value);
            });

            /** แสดงรายการของเดือนปัจจุบัน */
            if (res.data.data.orderProcessList.length > 0) {
              var year = moment().format('Y');
              var month = moment().format('M');

              var idxy = $scope.orders.findIndex(function(item) {
                if (item.year == year) {
                  return item;
                }
              });

             $scope.orders[idxy].isCollapsed = false;
             
              if ($scope.orders[idxy].months.length > 0) {
                var idxm = $scope.orders[idxy].months.findIndex(function(item) {
                  if (item.month == month) {
                    return item;
                  }
                });

                $scope.orders[idxy].months[idxm].isCollapsed = false;
              }
            }
          }
        })
        .finally(function() {
          $loading.finish('orders');
        });
    }
    
    function fetchOrderBilling(order) {
      OrderService.getOrderHistory(order.salesOrderNumber)
        .then(function(res) {
          if (res.data.result == 'SUCCESS') {
            var billing = res.data.data.orderHistoryHeaderList;

            if (billing.length > 0) {
              if (order.bill.bills.length == 0) {
                billing.forEach(function(value) {
                  order.bill.bills.push(value);
                });
              }
            }
          }
        });
    }

    function getMonthNameTH(month) {
      switch (parseInt(month)) {
        case 1:
          return 'มกราคม';
        case 2:
          return 'กุมภาพันธ์';
        case 3:
          return 'มีนาคม';
        case 4:
          return 'เมษายน';
        case 5:
          return 'พฤษภาคม';
        case 6:
          return 'มิถุนายน';
        case 7:
          return 'กรกฎาคม';
        case 8:
          return 'สิงหาคม';
        case 9:
          return 'กันยายน';
        case 10:
          return 'ตุลาคม';
        case 11:
          return 'พฤศจิกายน';
        case 12:
          return 'ธันวาคม';
      }
    }
  }

}());