
(function() {
  'use strict';

  angular
    .module('app')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider) {
    $stateProvider
      .state('order', {
        url: '/order',
        templateUrl: 'app/components/order/view/index.html',
        controller: 'OrderController',
        controllerAs: 'vm',
        data: {
          requiresLogin: true,
          menuCode: '01200'
        }
      });
  }

})();