(function() {
  'use strict';

  angular
    .module('app')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider) {
    $stateProvider
      .state('reportMonthly', {
        url: '/report/monthly',
        templateUrl: 'app/components/reportMonthly/view/index.html',
        controller: 'ReportMonthlyController',
        controllerAs: 'vm',
        data: {
          requiresLogin: true,
          menuCode: '01701'
        }
      });
  }

})();
