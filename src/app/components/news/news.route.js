(function() {
  'use strict';

  angular
    .module('app')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider) {
    $stateProvider
      .state('news', {
        url: '/news',
        templateUrl: 'app/components/news/view/index.html',
        controller: 'NewsController',
        controllerAs: 'vm',
        data: {
          requiresLogin: true,
          menuCode: '01500'
        }
      });
  }

})();
