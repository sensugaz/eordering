(function() {
  'use strict';

  angular
    .module('app')
    .controller('NewsController', NewsController);

  /** @ngInject */
  function NewsController($scope, $state, $localStorage, $loading) {
    init();

    function init() {
      $scope.userInfo = $localStorage.userInfo;
      $scope.customerInfo = $localStorage.customerInfo;
      
      if (($scope.customerInfo == undefined || angular.equals({}, $scope.customerInfo)) && $scope.userInfo.userTypeDesc == 'Multi') {
        $state.go('store');
      }

      $loading.setDefaultOptions({ 
        text: 'กำลังโหลด'
      });
    }

    $scope.toTOA = function() {
      window.open('https://toagroup.com/news-and-events/list', '_blank');
    };
  }

}());
