(function() {
  'use strict';

  angular
    .module('app')
    .controller('LogoutController', LogoutController);

  /** @ngInject */
  function LogoutController($state, $cookies, $localStorage) {
    init();

    function init() {
      delete $localStorage.userInfo;
      delete $localStorage.customerInfo;
      delete $localStorage.isAuthenticated;
      delete $localStorage.permission;
      
      delete $localStorage.marketingCodes;
      delete $localStorage.brandCodes;
      delete $localStorage.typeCodes;
      delete $localStorage.config;

      //$cookies.remove('expiresLogin');

      $state.go('login');
    }

  }

}());
