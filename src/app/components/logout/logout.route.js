(function() {
  'use strict';

  angular
    .module('app')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider) {
    $stateProvider
      .state('logout', {
        url: '/logout',
        controller: 'LogoutController',
        controllerAs: 'vm',
        data: {
          requiresLogin: false
        }
      });
  }

})();