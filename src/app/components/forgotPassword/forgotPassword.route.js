(function() {
  'use strict';

  angular
    .module('app')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider) {
    $stateProvider
      .state('forgotPassword', {
        url: '/forgot_password',
        templateUrl: 'app/components/forgotPassword/view/index.html',
        controller: 'ForgotPasswordController',
        controllerAs: 'vm',
        data: {
          requiresLogin: false
        }
      });
  }

})();