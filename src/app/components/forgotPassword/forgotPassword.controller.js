(function () {
  'use strict';

  angular
    .module('app')
    .controller('ForgotPasswordController', ForgotPasswordController);

  /** @ngInject */
  function ForgotPasswordController($scope, AuthService) {
    init();

    function init() {
      /** define */
      $scope.input = {};
      $scope.btnLoading = false;
    }

    $scope.onForgotPassword = function(isValid) {
      if (isValid) {
        $scope.btnLoading = true;
        
        var formData = {
          userName: $scope.input.username,
          email: '',
          key: ''
        };
        
        AuthService.forgotPassword(formData)
          .then(function(res) {
            if (res.data.result == 'SUCCESS') {
              swal('สำเร็จ', 'กรุณาตรวจสอบอีเมลของท่าน', 'success');
            } else {
              swal('ไม่สำเร็จ', 'ชื่อผู้ใช้งานไม่ถูกต้อง', 'warning');
            }
          })
          .finally(function() {
            $scope.btnLoading = false;
            clearForm();
          });
      }
    };

    /**
     * Functions
     */
    function clearForm() {
      $scope.input = {};
      $scope.form.$setPristine();
    }
  }

}());
