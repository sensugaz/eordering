(function () {
  'use strict';

  angular
    .module('app')
    .controller('CategoryController', CategoryController);

  /** @ngInject */
  function CategoryController($scope, $state, $filter, $stateParams, $localStorage, $loading, $uibModal, MarketingService, ProductService, FavoriteService, CustomerService) {
    init();

    function init() {
      /** define */
      $scope.userInfo = $localStorage.userInfo;
      $scope.customerInfo = $localStorage.customerInfo;
      $scope.marketing = {};
      $scope.marketings = [];
      $scope.brands = [];
      $scope.types = [];
      $scope.products = [];

      if (($scope.customerInfo == undefined || angular.equals({}, $scope.customerInfo)) && $scope.userInfo.userTypeDesc == 'Multi') {
        $state.go('store');
      }

      /** pagination */
      $scope.pageSize = 16;
      $scope.maxSize = 5;
      $scope.currentPage = 1;

      /** checkbox */
      $scope.marketingCodes = [];
      $scope.brandCodes = [];
      $scope.typeCodes = [];

      $scope.filterBy = [];

      $scope.search = $stateParams.search;

      $loading.setDefaultOptions({
        text: 'กำลังโหลด'
      });

      /** fetch date */
      fetchMarketings();
      
      if ($stateParams.search != undefined) {
        $localStorage.marketingCodes = [];
        $localStorage.brandCodes = [];
        $localStorage.typeCodes = [];
      }

      if ($stateParams.marketing != undefined) {
        $localStorage.marketingCodes = [];
        $localStorage.brandCodes = [];
        $localStorage.typeCodes = [];

        $localStorage.marketingCodes.push($stateParams.marketing);
        $scope.marketingCodes = $localStorage.marketingCodes;

        fetchProducts();
      } else if ($stateParams.marketing == undefined && $stateParams.brand != undefined) {
        $localStorage.marketingCodes = [];
        $localStorage.brandCodes = [];
        $localStorage.typeCodes = [];

        $localStorage.brandCodes.push($stateParams.brand);
        $scope.brandCodes = $localStorage.brandCodes;

        fetchProducts();
      } else if ($stateParams.marketing == undefined && $stateParams.brand == undefined) {
        $scope.marketingCodes = $localStorage.marketingCodes;
        $scope.brandCodes = $localStorage.brandCodes;
        $scope.typeCodes = $localStorage.typeCodes;

        fetchProducts();
      }
    }

    $scope.onSelectMarketing = function (marketingCode) {
      var i = $.inArray(marketingCode, $scope.marketingCodes);

      if (i > -1) {
        $localStorage.marketingCodes.splice(i, 1);
        $scope.clearBrand();
      } else {
        $localStorage.marketingCodes.push(marketingCode);
      }

      $scope.marketingCodes = $localStorage.marketingCodes;
      
      $scope.clearType();

      fetchProducts();
    };

    $scope.onSelectBrand = function(brandCode) {
      var i = $.inArray(brandCode, $scope.brandCodes);

      if (i > -1) {
        $localStorage.brandCodes.splice(i, 1);
      } else {
        $localStorage.brandCodes.push(brandCode);
      }

      $scope.brandCodes = $localStorage.brandCodes;

      fetchProducts();
    };

    $scope.onSelectType = function (typeCode) {
      var i = $.inArray(typeCode, $scope.typeCodes);

      if (i > -1) {
        $localStorage.typeCodes.splice(i, 1);
      } else {
        $localStorage.typeCodes.push(typeCode);
      }

      $scope.typeCodes = $localStorage.typeCodes;
      
      fetchProducts();
    };

    $scope.clearMarketing = function () {
      $localStorage.marketingCodes = [];
      $scope.marketingCodes = [];

      $scope.clearBrand();
      $scope.clearType();

      fetchProducts();
    };

    $scope.clearBrand = function () {
      $localStorage.brandCodes = [];
      $scope.brandCodes = [];

      fetchProducts();
    };

    $scope.clearType = function () {
      $localStorage.typeCodes = [];
      $scope.typeCodes = [];

      fetchProducts();
    };

    $scope.toProduct = function (btfCode) {
      $loading.start('product');

      CustomerService.customerBlock('00' + $scope.customerInfo.customerCode)
        .then(function(res) {
          if (res.data.result == 'SUCCESS') {
            if (res.data.data.block == 'X') {
              swal('ไม่สำเร็จ', 'ท่านไม่สามารถสั่งซื้อสินค้า online <br> บนระบบ TOA e-Ordering ได้ <br> เนื่องจาก ติดปัญหาการชำระบัญชี <br><br> กรุณาติดต่อ <br> ผู้แทนขาย หรือ ฝ่ายสินเชื่อและลูกหนี้ <br> บริษัท ทีโอเอ เพ้นท์ (ประเทศไทย) จำกัด (มหาชน)', 'error');
            } else {
              $state.go('product', { btfCode: btfCode });
            }
          } else {
            swal('ไม่สำเร็จ', 'เกิดข้อผิดพลาด', 'error');
          }
        })
        .finally(function () {
          $loading.finish('product');
        });
    };

    $scope.onAddFavorite = function (product) {
      var formData = {
        customerId: $scope.customerInfo.customerId,
        userName: $scope.userInfo.userName,
        btfCode: product.btf
      };

      FavoriteService.addFavorite(formData)
        .then(function (res) {
          if (res.data.result == 'SUCCESS') {
            product.isFavorite = true;

            swal('สำเร็จ', 'เพิ่มรายการโปรดเรียบร้อย', 'success');
          } else {
            swal('ไม่สำเร็จ', 'ไม่สามารถเพิ่มรายการโปรดได้', 'warning');
          }
        });
    };

    $scope.onRemoveFavorite = function (product) {
      var formData = {
        customerId: $scope.customerInfo.customerId,
        userName: $scope.userInfo.userName,
        btfCode: product.btf
      };

      FavoriteService.removeFavorite(formData)
        .then(function (res) {
          if (res.data.result == 'SUCCESS') {
            product.isFavorite = false;

            swal('สำเร็จ', 'ลบรายการโปรดเรียบร้อย', 'success');
          } else {
            swal('ไม่สำเร็จ', 'ไม่สามารถลบรายการโปรดได้', 'warning');
          }
        });
    };

    $scope.getMarketing = function (marketingCode) {
      return $filter('filter')($scope.marketings, function (value) {
        if (value.marketingCode == marketingCode) {
          return marketingCode;
        }
      })[0];
    };

    $scope.getBrand = function (brandCode) {
      return $filter('filter')($scope.brands, function (value) {
        if (value.brandCode == brandCode) {
          return brandCode;
        }
      })[0];
    };

    $scope.getType = function (typeCode) {
      return $filter('filter')($scope.types, function (value) {
        if (value.typeCode == typeCode) {
          return typeCode;
        }
      })[0];
    };


    $scope.removeMarketingFilter = function (marketingcode) {
      var index = $scope.marketingCodes.indexOf(marketingcode);
      $scope.marketingCodes.splice(index, 1);

      fetchProducts();
    }
    
    $scope.removeBrandFilter = function (brandcode) {
      var index = $scope.brandCodes.indexOf(brandcode);
      $scope.brandCodes.splice(index, 1);

      fetchProducts();
    }

    $scope.removeTypeFilter = function (typecode) {
      var index = $scope.typeCodes.indexOf(typecode);
      $scope.typeCodes.splice(index, 1);

      fetchProducts();
    }

    $scope.removeSearch = function() {
      $state.go('category', { search: undefined });
    }

    /**
     * Function
     */
    function fetchMarketings() {
      $loading.start('filter');

      MarketingService.getMarketing($scope.customerInfo.customerId)
        .then(function (res) {
          $scope.marketings = res.data.data.marketingList;
          $scope.brands = res.data.data.brandList;
          $scope.types = res.data.data.typeList;
        })
        .finally(function () {
          $loading.finish('filter');
        });
    }

    function fetchProducts() {
      var params = {
        customerId: $scope.customerInfo.customerId,
        marketingCodeList: $scope.marketingCodes,
        brandCodeList: $scope.brandCodes,
        typeCodeList: $scope.typeCodes,
        isBTFView: true
      };

      $loading.start('product');
      $loading.start('filter');

      ProductService.getProduct(params)
        .then(function (res) {
          if (res.data.result == 'SUCCESS') {
            if ($stateParams.search == undefined) {
              $scope.products = res.data.data.productList;
            } else {
              $scope.products = $filter('filter')(res.data.data.productList, function(item) {
                if (item.btfWebDescTh.includes($stateParams.search.toUpperCase()) || item.btfWeb.includes($stateParams.search)) {
                  return item;
                }
              });
              
              if ($scope.products.length == 0) {
                swal('แจ้งเตือน', 'สินค้ารายการนี้ ไม่สามารถสั่งซื้อ บนระบบ TOA eOrdering <br> กรุณาสั่งซื้อผ่าน ผู้แทนขาย หรือ ฝ่ายบริการร้านค้า <br> บริษัท ทีโอเอ เพ้นท์ (ประเทศไทย) เท่านั้น', 'warning');
              }
            }
          }
        })
        .finally(function () {
          $loading.finish('product');
          $loading.finish('filter');
        });
    }
  }

}());
