(function() {
  'use strict';

  angular
    .module('app')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider) {
    $stateProvider
      .state('category', {
        url: '/category?brand?marketing?search',
        templateUrl: 'app/components/category/view/index.html',
        controller: 'CategoryController',
        controllerAs: 'vm',
        data: {
          requiresLogin: true
        }
      });
  }

})();
