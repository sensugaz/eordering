(function () {
  'use strict';

  angular
    .module('app')
    .controller('ProfileController', ProfileController);

  /** @ngInject */
  function ProfileController($scope, $state, $localStorage) {
    init();

    function init() {
      $scope.userInfo = $localStorage.userInfo;
      $scope.customerInfo = $localStorage.customerInfo;

      if (($scope.customerInfo == undefined || angular.equals({}, $scope.customerInfo)) && $scope.userInfo.userTypeDesc == 'Multi') {
        $state.go('store');
      }

      $scope.input = {
        customerCode: $scope.customerInfo.customerCode,
        customerName: $scope.customerInfo.customerName,
        address: $scope.customerInfo.address,
        street: $scope.customerInfo.street,
        subDistrictName: $scope.customerInfo.subDistrictName,
        districtName: $scope.customerInfo.districtName,
        cityName: $scope.customerInfo.cityName,
        postCode: $scope.customerInfo.postCode,
        telNo: $scope.customerInfo.telNo,
        email: $scope.customerInfo.email,
        transportZoneDesc: $scope.customerInfo.transportZoneCode + ' ' + $scope.customerInfo.transportZoneDesc,
        bblNo: $scope.customerInfo.bblNo,
        bblNameEng: $scope.customerInfo.bblNameEng,
        priceList: $scope.customerInfo.priceList,
        mobileDC: $scope.customerInfo.mobileDC,
        customerEmailDC: $scope.customerInfo.customerEmailDC
      };
    }
  }

}());
