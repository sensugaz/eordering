(function() {
  'use strict';

  angular
    .module('app')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider) {
    $stateProvider
      .state('profile', {
        url: '/profile',
        templateUrl: 'app/components/profile/view/index.html',
        controller: 'ProfileController',
        controllerAs: 'vm',
        data: {
          requiresLogin: true,
          menuCode: '01100'
        }
      });
  }

})();