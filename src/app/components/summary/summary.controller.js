(function() {
  'use strict';

  angular
    .module('app')
    .controller('SummaryController', SummaryController);

  /** @ngInject */
  function SummaryController($scope, $state, $filter, $stateParams, $localStorage, $loading, OrderService) {

    init();

    function init() {
      /** define */
      $scope.userInfo = $localStorage.userInfo;
      $scope.customerInfo = $localStorage.customerInfo;
      $scope.order = {};
      $scope.products = [];
      $scope.boms = [];
      $scope.totalItem = 0;
      $scope.totalAmount = 0;
      $scope.ship = {};

      if (($scope.customerInfo == undefined || angular.equals({}, $scope.customerInfo)) && $scope.userInfo.userTypeDesc == 'Multi') {
        $state.go('store');
      }

      $loading.setDefaultOptions({ 
        text: 'กำลังโหลด'
      });
      
      fetchOrder(); 
    }

    function fetchOrder() {
      $loading.start('summary');

      OrderService.getOrderInfo($stateParams.orderId)
        .then(function(res) {
          if (res.data.result == 'SUCCESS') {
            res.data.data.order.requestDate = moment(res.data.data.order.requestDate).format('L');
            
            $scope.order = res.data.data.order;
            $scope.products = res.data.data.orderDetailList;
            $scope.boms = res.data.data.orderBOMItems;

            $scope.totalAmount = 0;

            $scope.products.forEach(function(pd) {
              $scope.totalAmount += pd.totalAmount;
            });
            
            $scope.totalItem = $filter('filter')($scope.products, {
              isBOM: false
            }).length + $scope.boms.length;
            
            fetchShip();
          } else {
            $state.go('home');
          }
        })
        .finally(function() {
          $loading.finish('summary');
        });
    }

    function fetchShip() {
      OrderService.getOrder($scope.customerInfo.customerId, $scope.userInfo.userName)
        .then(function(res) {
          if (res.data.result == 'SUCCESS') {
            if ($scope.order.shipId != '') {
              $scope.ship = $filter('filter')(res.data.data.shipToList, {
                shipId: $scope.order.shipId
              })[0];
            }
          }
        });
    }

  }

}());
