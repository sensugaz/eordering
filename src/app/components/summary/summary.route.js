(function() {
  'use strict';

  angular
    .module('app')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider) {
    $stateProvider
      .state('summary', {
        url: '/summary/{orderId}',
        templateUrl: 'app/components/summary/view/index.html',
        controller: 'SummaryController',
        controllerAs: 'vm',
        data: {
          requiresLogin: true
        }
      });
  }

})();
