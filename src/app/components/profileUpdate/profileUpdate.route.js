(function() {
  'use strict';

  angular
    .module('app')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider) {
    $stateProvider
      .state('profileUpdate', {
        url: '/profile/update',
        templateUrl: 'app/components/profileUpdate/view/index.html',
        controller: 'ProfileUpdateController',
        controllerAs: 'vm',
        data: {
          requiresLogin: true,
          menuCode: '01903'
        }
      });
  }

})();