(function () {
  'use strict';

  angular
    .module('app')
    .controller('ProfileUpdateController', ProfileUpdateController);

  /** @ngInject */
  function ProfileUpdateController($scope, $state, $localStorage, CustomerService) {
    init();

    function init() {
      $scope.userInfo = $localStorage.userInfo;
      $scope.customerInfo = $localStorage.customerInfo;
      $scope.btnLoading = false;

      if (($scope.customerInfo == undefined || angular.equals({}, $scope.customerInfo)) && $scope.userInfo.userTypeDesc == 'Multi') {
        $state.go('store');
      }

      fetchCustomer();
    }

    $scope.onUpdateProfile = function() {
      $scope.btnLoading = true;

      var formData = {
        customerId: $scope.customerInfo.customerId,
        customerCode: $scope.customerInfo.customerCode,
        customerName: $scope.input.customerName,
        address: $scope.input.address,
        street: $scope.input.street,
        subDistinct: $scope.input.subDistrictName,
        distinct: $scope.input.districtName,
        city: $scope.input.cityName,
        postCode: $scope.input.postCode,
        phoneNumber: $scope.input.telNo,
        email: $scope.input.email,
        shipToText: $scope.input.transportZoneDesc,
        remark: '', 
        userName: $scope.userInfo.userName,
      };

      CustomerService.customerUpdate(formData)
        .then(function(res) {
          if (res.data.result == 'SUCCESS') {
            swal('สำเร็จ', 'แจ้งเปลี่ยนแปลงข้อมูลสำเร็จ', 'success');
          }
        })
        .finally(function() {
          $scope.btnLoading = false;
        });
    };

    /**
     * Functions
     */
    function fetchCustomer() {
      $scope.input = {
        customerCode: $scope.customerInfo.customerCode,
        customerName: $scope.customerInfo.customerName,
        address: $scope.customerInfo.address,
        street: $scope.customerInfo.street,
        subDistrictName: $scope.customerInfo.subDistrictName,
        districtName: $scope.customerInfo.districtName,
        cityName: $scope.customerInfo.cityName,
        postCode: $scope.customerInfo.postCode,
        telNo: $scope.customerInfo.telNo,
        email: $scope.customerInfo.email,
        transportZoneDesc: $scope.customerInfo.transportZoneCode + ' ' + $scope.customerInfo.transportZoneDesc,
        mobileDC: $scope.customerInfo.mobileDC,
        customerEmailDC: $scope.customerInfo.customerEmailDC
      };
    }

  }

}());
