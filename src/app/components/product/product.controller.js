(function() {
  'use strict';

  angular
    .module('app')
    .controller('ProductController', ProductController);

  /** @ngInject */
  function ProductController($scope, $rootScope, $state, $filter, $stateParams, $localStorage, $uibModal, $loading, ProductService, CartService, FavoriteService, PromotionService, CustomerService) {
    init();

    function init() {
      /** define */
      $scope.userInfo = $localStorage.userInfo;
      $scope.customerInfo = $localStorage.customerInfo;
      $scope.btf = {};
      $scope.boms = [];
      $scope.colors = [];
      $scope.products = [];
      $scope.relatedProduct = [];
      $scope.sizes = [];

      if (($scope.customerInfo == undefined || angular.equals({}, $scope.customerInfo)) && $scope.userInfo.userTypeDesc == 'Multi') {
        $state.go('store');
      }
      
      /** pagination */
      $scope.pageSize = 4;
      $scope.maxSize = 3;
      $scope.currentPage = 1;

      $scope.product = {};
      $scope.btnLoading = false;

      $loading.setDefaultOptions({ 
        text: 'กำลังโหลด'
      });

      /** default value  */
      $scope.input = {
        qty: 1,
        price: 0,
        size: '',
        colorCode: '',
        rgbCode: ''
      };

      if ($scope.customerInfo == undefined || angular.equals({}, $scope.userInfo)) {
        $state.go('store');
      }

      chkCustomerBlock();

      /** fetch data */
      fetchProduct();
    }

    $scope.addCart = function(isValid) {
      if (isValid) {
        var checkQty = false;
        var promotions = [];
        
        $scope.btnLoading = true;

        if ($scope.input.qty == 0 || $scope.input.qty == undefined) {
          switch ($scope.product.altUnit1Code) {
            case 'BOX':
              if ($scope.product.isBox) {
                $scope.input.qty = 1;
              } else {
                $scope.input.qty = $scope.product.altUnit1Amount;
              }
              break;
            case 'DZ':
              $scope.input.qty = $scope.product.altUnit1Amount;
              break;
            default:
              $scope.input.qty = 1;
              break;
          }
        } else if ($scope.product.isBox || $scope.product.altUnit1Code == 'DZ') {
          var textAltUnit1Amount = $scope.product.altUnit1Amount;
          var textAltUnit1NameTh = $scope.product.unitNameTh;

          switch ($scope.product.altUnit1Code) {
            case 'DZ':
              textAltUnit1Amount = 1;
              textAltUnit1NameTh = $scope.product.altUnit1NameTh;
              break;
          }

          if ($scope.input.qty < $scope.product.altUnit1Amount) {
            swal('ไม่สำเร็จ', 'กรุณาสั่งซื้ออย่างน้อย ' + textAltUnit1Amount + ' ' + textAltUnit1NameTh + ' ค่ะ', 'warning');

            $scope.input.qty = $scope.product.altUnit1Amount;   
          } else if ($scope.input.qty % $scope.product.altUnit1Amount) {
            swal('ไม่สำเร็จ', 'ผลิตภัณฑ์ต้องสั่งซื้อทีละ ' + textAltUnit1Amount + ' ' + textAltUnit1NameTh + ' ค่ะ <br> ระบบจะปรับจำนวนให้อัตโนมัติ ', 'warning');

            $scope.input.qty = $scope.product.altUnit1Amount * parseInt($scope.input.qty / $scope.product.altUnit1Amount);
          } else {
            checkQty = true;
          }
        } else {
          checkQty = true;
        }

        if (checkQty) {
          CartService.getCart($scope.customerInfo.customerId, $scope.userInfo.userName)
            .then(function(res) {
              if (res.data.result == 'SUCCESS') {
                var zwFilter = $filter('filter')(res.data.data.cartList, function(item) {
                  if (item.conditionType == 'ZW81' || item.conditionType == 'ZW82') {
                    return item;
                  }
                });

                if (zwFilter.length > 0) {
                  swal('แจ้งเตือน','สินค้าโปรโมชั่นพิเศษ ไม่สามารถสั่งซื้อรวมกับสินค้าอื่น <br> กรุณากดยืนยันเฉพาะเอกสารสั่งซื้อนี้','warning');
                } else {
                  var exists = $filter('filter')(res.data.data.cartList, function(item) {
                    if (item.productId == $scope.product.productId && item.customerId == $scope.customerInfo.customerId) {
                      return item;
                    }
                  })[0];
    
                  var formData = {};
  
                  if (exists == undefined) {
                    formData = {
                      cartList: [{
                        promotionDTId: 0,
                        customerId: $scope.customerInfo.customerId,
                        userName: $scope.userInfo.userName,
                        productId: $scope.product.productId,
                        qty: $scope.input.qty
                      }],
                      promotionList: promotions
                    };
  
                    CartService.addCart(formData)
                      .then(function(res) {
                        if (res.data.result == 'SUCCESS') {
                          swal('สำเร็จ', 'เพิ่มสินค้าลงในตะกร้าเรียบร้อย', 'success');
                          
                          $scope.getProduct();   
                          
                          // update cart in header
                          $rootScope.$broadcast('updateCartInHeader');
                        } else {
                          swal('ไม่สำเร็จ', 'ไม่สามารถเพิ่มสินค้าลงในตะกร้าได้', 'warning');
                        }
                      });
                  } else {
                    formData = {
                      cartList: [{
                        promotionDTId: 0,
                        customerId: $scope.customerInfo.customerId,
                        userName: $scope.userInfo.userName,
                        productId: $scope.product.productId,
                        qty: parseInt(exists.qty) + parseInt($scope.input.qty)
                      }],
                      promotionList: promotions
                    };
  
                    CartService.updateCart(formData)
                      .then(function(res) {
                        if (res.data.result == 'SUCCESS') {
                          swal('สำเร็จ', 'เพิ่มสินค้าลงในตะกร้าเรียบร้อย', 'success');
                          
                          $scope.getProduct();      
  
                          // update cart in header
                          $rootScope.$broadcast('updateCartInHeader');
                        } else {
                          swal('ไม่สำเร็จ', 'ไม่สามารถเพิ่มสินค้าลงในตะกร้าได้', 'error');
                        }
                      });
                  }
                }
              }
            })
            .finally(function() {
              $scope.btnLoading = false;
            });
        } else {
          $scope.btnLoading = false;
        }
      }
    };

    $scope.incQty = function() {
      var qty = 1;
      /*
      if ($scope.product.altUnit1Code == 'BOX') {
        if ($scope.product.isBox) {
          qty = $scope.product.altUnit1Amount;
        } else {
          qty = 1;
        }
      } else if ($scope.product.altUnit1Code == 'DZ') {
        qty = $scope.product.altUnit1Amount;
      } else {
        qty = 1;
      }
      */
      switch ($scope.product.altUnit1Code) {
        case 'BOX':
          if ($scope.product.isBox) {
            qty = $scope.product.altUnit1Amount;
          } else {
            qty = 1;
          }
          break;
        case 'DZ':
          qty = $scope.product.altUnit1Amount;
          break;
        default:
          qty = 1;
          break;
      }

      $scope.input.qty = parseInt($scope.input.qty) + parseInt(qty);
    };

    $scope.decQty = function() {
      var qty = 1;

      switch ($scope.product.altUnit1Code) {
        case 'BOX':
          if ($scope.product.isBox) {
            qty = $scope.product.altUnit1Amount;
          } else {
            qty = 1;
          }
          break;
        case 'DZ':
          qty = $scope.product.altUnit1Amount;
          break;
        default:
          qty = 1;
          break;
      }

      if ($scope.input.qty > qty) {
        $scope.input.qty -= qty;
      }
    };

    $scope.edtQty = function() {
      if ($scope.input.qty == 0 || $scope.input.qty == undefined) {
        switch ($scope.product.altUnit1Code) {
          case 'BOX':
            if ($scope.product.isBox) {
              $scope.input.qty = 1;
            } else {
              $scope.input.qty = $scope.product.altUnit1Amount;
            }
            break;
          case 'DZ':
            $scope.input.qty = $scope.product.altUnit1Amount;
            break;
          default:
            $scope.input.qty = 1;
            break;
        }
      } else if ($scope.product.isBox || $scope.product.altUnit1Code == 'DZ') {
        var textAltUnit1Amount = $scope.product.altUnit1Amount;
        var textAltUnit1NameTh = $scope.product.unitNameTh;

        switch ($scope.product.altUnit1Code) {
          case 'DZ':
            textAltUnit1Amount = 1;
            textAltUnit1NameTh = $scope.product.altUnit1NameTh;
            break;
        }

        if ($scope.input.qty < $scope.product.altUnit1Amount) {
          swal('ไม่สำเร็จ', 'กรุณาสั่งซื้ออย่างน้อย ' + textAltUnit1Amount + ' ' + textAltUnit1NameTh + ' ค่ะ', 'warning');

          $scope.input.qty = $scope.product.altUnit1Amount;   
        } else if ($scope.input.qty % $scope.product.altUnit1Amount) {
          swal('ไม่สำเร็จ', 'ผลิตภัณฑ์ต้องสั่งซื้อทีละ ' + textAltUnit1Amount + ' ' + textAltUnit1NameTh + ' ค่ะ <br> ระบบจะปรับจำนวนให้อัตโนมัติ', 'warning');

          $scope.input.qty = $scope.product.altUnit1Amount * parseInt($scope.input.qty / $scope.product.altUnit1Amount);
        }
      }
    };

    $scope.getProduct = function() {
      var listColor = [];
      
      $scope.colors.forEach(function(value, key) {
        if ($scope.colors[key].sizeCode == $scope.input.size) {
          listColor.push($scope.colors[key]);
        }
      });
      
      $scope.colorCodeName = listColor[0] ? listColor[0].colorCode : '';
      $scope.input.rgbCode = listColor[0].rgbCode;

      $scope.products.forEach(function(value) {
        if (value.sizeCode == $scope.input.size && value.colorCode == $scope.colorCodeName) {
          $scope.product = value;
          $scope.productId = value.productId;
          $scope.input.qty = ($scope.product.altUnit1Amount > 0) ? $scope.product.altUnit1Amount : 1;
        }
      });
    };

    $scope.setProduct = function(colorCode) {
      $scope.input.colorCode = colorCode;
      $scope.input.rgbCode = $filter('filter')($scope.colors, function(item) {
        if (item.colorCode == colorCode && item.sizeCode == $scope.input.size) {
          return item;
        }
      })[0].rgbCode;

      $scope.colorCodeName = colorCode;

      $scope.products.forEach(function(value) {
        if (value.sizeCode == $scope.input.size && value.colorCode == colorCode) {
          $scope.product = value;
          $scope.input.qty = ($scope.product.altUnit1Amount > 0) ? $scope.product.altUnit1Amount : 1;
        }
      });
    };

    $scope.onAddFavorite = function(product) {    
      var formData = {
        customerId: $scope.customerInfo.customerId,
        userName: $scope.userInfo.userName,
        btfCode: product.btf
      };
      
      FavoriteService.addFavorite(formData)
        .then(function(res) {
          if (res.data.result == 'SUCCESS') {
            product.isFavorite = true;

            swal('สำเร็จ', 'เพิ่มรายการโปรดเรียบร้อย', 'success');
          } else {
            swal('ไม่สำเร็จ', 'ไม่สามารถเพิ่มรายการโปรดได้', 'warning');
          }
        });
    };

    $scope.onRemoveFavorite = function(product) {      
      var formData = {
        customerId: $scope.customerInfo.customerId,
        userName: $scope.userInfo.userName,
        btfCode: product.btf
      };
      
      FavoriteService.removeFavorite(formData)
        .then(function(res) {
          if (res.data.result == 'SUCCESS') {
            product.isFavorite = false;

            swal('สำเร็จ', 'ลบรายการโปรดเรียบร้อย', 'success');
          } else {
            swal('ไม่สำเร็จ', 'ไม่สามารถลบรายการโปรดได้', 'warning');
          }
        });
    };

    $scope.openModalPromotionDetail = function(btfInfo) {
      var modalInstance = $uibModal.open({
        controller: 'PromotionDetailController',
        templateUrl: 'app/modals/promotionDetail/view/index.html',
        windowClass: 'modal-flat',
        size: 'lg',
        resolve: {
          btfInfo: function() {
            return btfInfo;
          }
        }
      });

      modalInstance.result.catch(function() { });
    };

    $scope.toCategory = function() {
      $state.go('category');
    };

    /**
     * Function
     */
    function fetchProduct() {
      $loading.start('product');

      ProductService.getOneProduct($scope.customerInfo.customerId, $stateParams.btfCode)
        .then(function(res) {
          if (res.data.result == 'SUCCESS') {
            $scope.btf = res.data.data.btfInfo;
            $scope.boms = res.data.data.productInBOMList;
            $scope.products = res.data.data.productList;

            $scope.products.forEach(function(value) {
              $scope.sizes.push({
                sizeCode: value.sizeCode,
                sizeName: value.sizeName,
                productCode: value.productCode
              });

              $scope.colors.push({
                colorCode: value.colorCode,
                colorNameTh: value.colorNameTh,
                rgbCode: value.rgbCode,
                sizeCode: value.sizeCode,
                productCode: value.productCode
              });
            });
            
            if ($scope.products[0].isBox) {
              $scope.input.qty = $scope.products[0].altUnit1Amount;
            }

            $scope.sizeFlag = $scope.products[0].sizeFlag;
            $scope.colorFlag = $scope.products[0].colorFlag;

            $scope.input.size = $scope.sizes[0].sizeCode;
            $scope.input.code = $scope.sizes[0].productCode;
            
            $scope.input.rgbCode = $scope.colors[0].rgbCode;
            $scope.colorCodeName = $scope.colors[0] ? $scope.colors[0]['colorCode'] : '';
            
            PromotionService.getPromotionInBTF($scope.customerInfo.customerId, $scope.btf.btf)
              .then(function(res) {
                if (res.data.result == 'SUCCESS') {
                  $scope.promotions = res.data.data.promotionHDList;

                  if ($scope.promotions.length > 0) {
                    $scope.openModalPromotionDetail($scope.btf);
                  }
                }
              });
            
            $scope.getProduct();

            getRelatedProduct();
          } else {
            $state.go('category');
          }          
        })
        .finally(function() {
          $loading.finish('product');
        });
    }

    function getRelatedProduct() {
      $loading.start('relatedProduct');
      
      ProductService.getProduct({ customerId: $scope.customerInfo.customerId })
        .then(function(res) {
          if (res.data.result == 'SUCCESS') {
            var product = $filter('filter')(res.data.data.productList, {
              btf: $scope.btf.btf
            })[0];
            
            $scope.relatedProduct = $filter('filter')(res.data.data.productList, function(value) {
              if (value.brandCode == product.brandCode && value.btf != product.btf) {
                return value;
              }
            });
          }
        })
        .finally(function() {
          $loading.finish('relatedProduct');
        });
    }

    function chkCustomerBlock() {
      CustomerService.customerBlock('00' + $scope.customerInfo.customerCode)
        .then(function(res) {
          if (res.data.result == 'SUCCESS') {
            if (res.data.data.block == 'X') {
              $state.go('home');
            }
          }
        });
    }
  }
}());
