(function() {
  'use strict';

  angular
    .module('app')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider) {
    $stateProvider
      .state('product', {
        url: '/product/{btfCode}',
        templateUrl: 'app/components/product/view/index.html',
        controller: 'ProductController',
        controllerAs: 'vm',
        data: {
          requiresLogin: true
        }
      });
  }

})();
