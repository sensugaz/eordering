(function() {
  'use strict';

  angular
    .module('app')
    .controller('ReportQuarterController', ReportQuarterController);

  /** @ngInject */
  function ReportQuarterController($scope, $state, $localStorage, $loading, $filter, ReportService) {
    init();

    function init() {
      $scope.userInfo = $localStorage.userInfo;
      $scope.customerInfo = $localStorage.customerInfo;
      
      if (($scope.customerInfo == undefined || angular.equals({}, $scope.customerInfo)) && $scope.userInfo.userTypeDesc == 'Multi') {
        $state.go('store');
      }
      
      $scope.input = {
        salesGroup: ''
      };

      $scope.files = [];
      
      /** pagination */
      $scope.pageSize = 2;
      $scope.maxSize = 5;
      $scope.currentPage = 1;

      $scope.reports = [];
      $scope.reportList = [];

      $loading.setDefaultOptions({ 
        text: 'กำลังโหลด'
      });

      /** select2 */
      $('.select2').select2();

      $scope.areaNo = [];
      $scope.salesGroup = [];

      /** fetch data */
      fetchReportFile();
    }

    $scope.onSelectFile = function() {
      $loading.start('report');
      
      $scope.reportList = [];

      var params = {
        userName: $scope.userInfo.userName,     
        deptId: $scope.userInfo.deptId
      }

      ReportService.getReportQuarter($scope.input.fileName, params)
        .then(function(res) {
          if (res.data.result == 'SUCCESS') {
            $scope.reports = res.data.data;
            $scope.input.salesGroup = '';

            switch ($scope.userInfo.deptId) {
              case 2:
                $scope.input.salesGroup = $scope.userInfo.userName.replace('sa_', '');
                break;           
              case 3: 
                $scope.input.areaNo = $scope.userInfo.userName.replace('sa_', '').toUpperCase();
                break;
            }

            $scope.input.period = $scope.reports[0].period;
          }
        })
        .finally(function() {
          $loading.finish('report');
        });
    };

    $scope.onShowReport = function(isValid) {
      if (isValid) {
        var temp;
        if ($scope.input.salesGroup != 'ALL') {
          temp = $filter('filter')($scope.reports, {
            sales_group: $scope.input.salesGroup
          });
        } else {
          temp = $scope.reports;
        }
        
        if (temp.length == 0) {
          swal('แจ้งเตือน', 'ไม่พบข้อมูล', 'warning');
        }

        var data = [];

        temp.forEach(function(value) {
          var _ = $filter('filter')(data, function(f) {
            switch ($scope.userInfo.deptId) {
              case 1:
              case 5:
              case 6:
              case 7:
              case 8:
              case 9:
                if (f.area_no == value.area_no && f.sales_group == value.sales_group) {
                  return value;
                }
              break;
              case 2: 
                if (f.area_no == value.area_no) {
                  return value;
                }
                break;
            }
          });

          if (_.length <= 0) {
            data.push({
              period: value.period,
              commission_type: value.commission_type,
              sales_group: value.sales_group,
              employee_code: value.employee_code, 
              employee_name: value.employee_name,
              area_no: value.area_no,
              supervisor_code: value.supervisor_code,
              supervisor_name: value.supervisor_name,
              orders: [{
                product: value.product,
                net_amount: value.net_amount,
                target_amount: value.target_amount,
                percent: value.percent,
                reward: value.reward,
                actual_shop: value.actual_shop,
                target_shop: value.target_shop
              }],
              total: {
                net_amount: 0,
                target_amount: 0,
                percent: 0,
                reward: 0,
                actual_shop: 0,
                target_shop: 0
              }
            });
          } else {
            var index = data.findIndex(function(i) {
              if (i.area_no == value.area_no && i.sales_group == value.sales_group) {
                return i;
              }
            });

            data[index].orders.push({
              product: value.product,
              net_amount: value.net_amount,
              target_amount: value.target_amount,
              percent: value.percent,
              reward: value.reward,
              actual_shop: value.actual_shop,
              target_shop: value.target_shop
            });
          }
        });

        data.forEach(function(value) {
          value.orders.forEach(function(o) {
            value.total.net_amount += parseInt(o.net_amount);
            value.total.target_amount += parseInt(o.target_amount);
            value.total.percent += parseInt(o.percent);
            value.total.reward += parseInt(o.reward);
            value.total.actual_shop += parseInt(o.actual_shop);
            value.total.target_shop += parseInt(o.target_shop);
          });
        });

        $scope.reportList = data;
      }
    };

    /**
     * Function
     */
    function fetchReportFile() {
      $loading.start('report');
      
      ReportService.getReportQuarterFile()
        .then(function(res) {
          if (res.data.result == 'SUCCESS') {
            $scope.files = res.data.data;
          }
        })
        .finally(function() {
          $loading.finish('report');
        });
    }
  }

}());