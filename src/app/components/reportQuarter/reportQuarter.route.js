(function() {
  'use strict';

  angular
    .module('app')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider) {
    $stateProvider
      .state('reportQuarter', {
        url: '/report/quarter',
        templateUrl: 'app/components/reportQuarter/view/index.html',
        controller: 'ReportQuarterController',
        controllerAs: 'vm',
        data: {
          requiresLogin: true,
          menuCode: '01702'
        }
      });
  }

})();