(function() {
  'use strict';

  angular
    .module('app')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider) {
    $stateProvider
      .state('changePassword', {
        url: '/change_password',
        templateUrl: 'app/components/changePassword/view/index.html',
        controller: 'ChangePasswordController',
        controllerAs: 'vm',
        data: {
          requiresLogin: true,
          menuCode: '01905'
        }
      });
  }

})();
