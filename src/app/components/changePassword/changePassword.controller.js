(function() {
  'use strict';

  angular
    .module('app')
    .controller('ChangePasswordController', ChangePasswordController);

  /** @ngInject */
  function ChangePasswordController($scope, $localStorage, AuthService) {
    init();

    function init() {
      /** define */
      $scope.userInfo = $localStorage.userInfo;
      $scope.input = {};
    }

    $scope.onChangePassword = function(isValid) {
      if (isValid) {
        $scope.btnLoading = true;

        var formData = {
          userName: $scope.userInfo.userName,
          password: $scope.input.oldpassword,
          newPassword: $scope.input.newpassword
        };

        AuthService.changePassword(formData)
          .then(function(res) {
            if (res.data.result == 'SUCCESS') {
              swal('สำเร็จ', 'เปลี่ยนรหัสผ่านสำเร็จ', 'success');
            } else {
              switch (res.data.validateCode) {
                case '002':
                  swal('ไม่สำเร็จ', 'รหัสผ่านเก่าไม่ถูกต้อง', 'warning');
                  break;
              }
            }
            
            $scope.btnLoading = false;
            clearForm();            
          });
      }
    };

    function clearForm() {
      $scope.input = {};
      $scope.form.$setPristine();
    }
  }

}());
