(function () {
  'use strict';

  angular
    .module('app')
    .controller('MaintenanceController', MaintenanceController);

  /** @ngInject */
  function MaintenanceController($scope, $state, ConfigService) {
    init();

    function init() {
      ConfigService.getConfig()
        .then(function (res) {
          if (res.data.result == 'SUCCESS') {
            $scope.config = res.data.data.configList[0];
          }
        });
    }
  }

}());