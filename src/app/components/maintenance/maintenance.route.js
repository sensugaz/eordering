(function() {
  'use strict';

  angular
    .module('app')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider) {
    $stateProvider
      .state('maintenance', {
        url: '/maintenance',
        templateUrl: 'app/components/maintenance/view/index.html',
        controller: 'MaintenanceController',
        controllerAs: 'vm',
        data: { }
      });
  }

})();
