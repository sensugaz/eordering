(function() {
  'use strict';

  angular
    .module('app')
    .directive('footer', footer);


  /** @ngInject */
  function footer() {
    return {
      templateUrl: 'app/layouts/footer/view/index.html'
    }
  }

}());
