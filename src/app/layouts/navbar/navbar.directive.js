(function () {
  'use strict';

  angular
    .module('app')
    .directive('navbar', navbar);


  /** @ngInject */
  function navbar() {
    function NavbarController($scope, $filter, $localStorage) {

      init();

      function init() {
        $scope.userInfo = $localStorage.userInfo;
      }

      $scope.permission = function (menuCode) {
        var menu = $filter('filter')($localStorage.permission, function (item) {
          return item.menuCode == menuCode && item.privilegeId != 0;
        });

        return (menu != undefined) ? (menu.length) ? menu[0] : false : false;
      };
    }
    return {
      templateUrl: 'app/layouts/navbar/view/index.html',
      bindToController: true,
      controller: NavbarController,
      restrict: 'AE',
      scope: {},
    }
  }

}());
