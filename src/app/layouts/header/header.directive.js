(function() {
  'use strict';

  angular
    .module('app')
    .directive('header', header);


  /** @ngInject */
  function header() {

    function HeaderController($scope, $state, $timeout, $stateParams, $rootScope, $filter, $localStorage, $uibModal, ProductService, CartService) {

      init();

      function init() {
        /** define */
        $scope.userInfo = $localStorage.userInfo;
        $scope.customerInfo = $localStorage.customerInfo;
        $scope.totalItem = 0;
        $scope.input = {};

        if (($scope.customerInfo == undefined || angular.equals({}, $scope.customerInfo)) && $scope.userInfo.userTypeDesc == 'Multi') {
          $state.go('store');
        }
        
        $rootScope.$on('updateCartInHeader', function() {          
           updateCartItem();
        });

        /** fetch data */
        fetchProduct();
        updateCartItem();

        if ($stateParams.search != undefined) {
          $scope.input.search = $stateParams.search;
        }
      }

      $scope.searchProduct = function() {
        $state.go('category', { search: $scope.input.search , marketing: undefined, brand: undefined });
      };

      $scope.openCart = function() {
        var modalInstance = $uibModal.open({
          templateUrl: 'app/modals/cart/view/index.html',
          controller: 'CartModalController',
          windowClass: 'cart right fade'
        });

        modalInstance.result.catch(function() { });
      };

      $scope.permission = function(menuCode) {
        var menu = $filter('filter')($localStorage.permission, function(item) {
          return item.menuCode == menuCode && item.privilegeId != 0;
        });

        return (menu != undefined) ? (menu.length) ? menu[0] : false : false;
      };

      $scope.gotoProduct = function($item) {
        $state.go('product', { btfCode: $item.btfWeb });
      };

      /**
       * Function
       */   
      function updateCartItem() {
        CartService.getCart($scope.customerInfo.customerId, $scope.userInfo.userName)
          .then(function(res) {
            if (res.data.result == 'SUCCESS') {
              var p = $filter('filter')(res.data.data.cartList, {
                isBOM: false
              });

              var b = res.data.data.cartBOMItems;
                
              $scope.totalQty = ((p == undefined) ? 0 : p.length) + ((b == undefined) ? 0 : b.length);
            }
          });        
      }

      function fetchProduct() {
        ProductService.getBTFW($scope.customerInfo.customerId)
          .then(function(res) {
            if (res.data.result == 'SUCCESS') {
              $scope.products = res.data.data.btfwList;
            }
          });
      }
    }

    return {
      bindToController: true,
      controller: HeaderController,
      templateUrl: 'app/layouts/header/view/index.html',
      controllerAs: 'Ctrl',
      restrict: 'AE',
      scope: {},
    }
  }

}());
