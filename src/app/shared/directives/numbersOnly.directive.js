(function() {
  'use strict';

  angular
    .module('app')
    .directive('numbersOnly', numbersOnly);


  /** @ngInject */
  function numbersOnly() {
    function link(scope, element, attr, ctrl) {
      function fromUser(text) {
        if (text) {
          var transformedInput = text.replace(/[^0-9]/g, '');

          if (transformedInput !== text) {
            ctrl.$setViewValue(transformedInput);
            ctrl.$render();
          }

          return transformedInput;
        }

        return undefined;
      }

      ctrl.$parsers.push(fromUser);
    }

    return {
      require: 'ngModel',
      link: link,
      restrict: 'AE',
      scope: {},
    }
  }

}());
