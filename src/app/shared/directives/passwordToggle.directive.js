(function() {
  'use strict';

  angular
    .module('app')
    .directive('passwordToggle', passwordToggle);


  /** @ngInject */
  function passwordToggle($compile) {
    function link(scope, elem) {
      scope.tgl = function(){ elem.attr('type',(elem.attr('type')==='text'?'password':'text')); }
      
      var lnk = angular.element('<a data-ng-click="tgl()">Toggle</a>');
      
      $compile(lnk)(scope);
      elem.wrap('<div class="password-toggle"/>').after(lnk);
    }

    return {
      link: link,
      restrict: 'AE',
      scope: {},
    }
  }

}());
