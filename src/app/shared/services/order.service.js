(function() {
  'use strict';

  angular
    .module('app')
    .service('OrderService', OrderService);

  /** @ngInject */
  function OrderService(API_URL, $http, $q) {
    this.getOrder = function(customerId, userName) {
      var def = $q.defer();

      $http.get(API_URL + 'OrderPrepare', { params: { customerId: customerId, userName: userName }})
        .then(function(res) {
          def.resolve(res);
        })
        .catch(function() {
          def.reject();
        });

      return def.promise;
    };

    this.getOrderPrecess = function(customerId, startDate, endDate) {
      var def = $q.defer();

      $http.get(API_URL + 'OrderPrecess', { params: { customerId: customerId, startDocumentDate: startDate, endDocumentDate: endDate }})
        .then(function(res) {
          def.resolve(res);
        })
        .catch(function() {
          def.reject();
        });

      return def.promise;
    };

    this.getRequestDate = function() {
      var def = $q.defer();

      $http.get(API_URL + 'RequestDate')
        .then(function(res) {
          def.resolve(res);
        })
        .catch(function() {
          def.reject();
        });

      return def.promise;
    }

    this.addOrder = function(formData) {
      var def = $q.defer();

      $http.post(API_URL + 'Order', { order: formData })
        .then(function(res) {
          def.resolve(res);
        })
        .catch(function() {
          def.reject();
        });

      return def.promise;
    };

    this.getOrderInfo = function(orderId) {
      var def = $q.defer();

      $http.get(API_URL + 'OrderInfo', { params: { orderId: orderId }})
        .then(function(res) {
          def.resolve(res);
        })
        .catch(function() {
          def.reject();
        });

      return def.promise;
    };

    this.getOrderPrecessInfo = function(saleOrderNumber) {
      var def = $q.defer();

      $http.get(API_URL + 'OrderPrecessInfo', { params: { saleOrderNumber: saleOrderNumber }})
        .then(function(res) {
          def.resolve(res);
        })
        .catch(function() {
          def.reject();
        });

      return def.promise;
    };

    this.getOrderHistory = function(saleOrderNumber) {
      var def = $q.defer();

      $http.get(API_URL + 'OrderHistory', { params: { saleOrderNumber: saleOrderNumber}})
        .then(function(res) {
          def.resolve(res);
        })
        .catch(function() {
          def.reject();
        });

      return def.promise;
    };

    this.getOrderProcessTracking = function(saleOrderNumber) {
      var def = $q.defer();

      $http.get(API_URL + 'OrderProcessTracking', { params: { saleOrderNumber: saleOrderNumber}})
        .then(function(res) {
          def.resolve(res);
        })
        .catch(function() {
          def.reject();
        });

      return def.promise;
    };
  }

}());
