(function() {
  'use strict';

  angular
    .module('app')
    .service('PermissionService', PermissionService);

  /** @ngInject */
  function PermissionService(API_URL, $http, $q) {
    this.getPermission = function(roleId) {
      var def = $q.defer();

      $http.get(API_URL + 'Permission', { params: { roleId: roleId }})
        .then(function(res) {
          def.resolve(res);
        })
        .catch(function() {
          def.reject();
        });

      return def.promise;
    };
  }

}());