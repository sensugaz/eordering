(function() {
  'use strict';

  angular
    .module('app')
    .service('CartService', CartService);

  /** @ngInject */
  function CartService(API_URL, $http, $q) {
    this.getCart = function(customerId, userName) {
      var def = $q.defer();

      $http.get(API_URL + 'Cart', { params: { customerId: customerId, userName: userName }})
        .then(function(res) {
          def.resolve(res);
        })
        .catch(function() {
          def.reject();
        });

      return def.promise;
    };

    this.addCart = function(formData) {
      var def = $q.defer();

      $http.post(API_URL + 'Cart', formData)
        .then(function(res) {
          def.resolve(res);
        })
        .catch(function() {
          def.reject();
        });

      return def.promise;
    };

    this.updateCart = function(formData) {
      var def = $q.defer();

      $http.post(API_URL + 'UpdateCart', formData)
        .then(function(res) {
          def.resolve(res);
        })
        .catch(function() {
          def.reject();
        });

      return def.promise;
    };

    this.removeCart = function(formData) {
      var def = $q.defer();

      $http.post(API_URL + 'RemoveCart', formData)
        .then(function(res) {
          def.resolve(res);
        })
        .catch(function() {
          def.reject();
        });

      return def.promise;
    };
  }

}());
