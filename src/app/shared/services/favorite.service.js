(function() {
  'use strict';

  angular
    .module('app')
    .service('FavoriteService', FavoriteService);

  /** @ngInject */
  function FavoriteService(API_URL, $http, $q) {
    this.getFavorite = function(customerId) {
      var def = $q.defer();
      
      $http.get(API_URL + 'Favorites', { params: { customerId: customerId }})
        .then(function(res) {
          def.resolve(res);
        })
        .catch(function() {
          def.reject();
        });

      return def.promise;
    };

    this.addFavorite = function(formData) {
      var def = $q.defer();

      $http.post(API_URL + 'Favorites', { favoriteInfo: formData })
        .then(function(res) {
          def.resolve(res);
        })
        .catch(function() {
          def.reject();
        });

      return def.promise;
    };

    this.removeFavorite = function(formData) {
      var def = $q.defer();

      $http.post(API_URL + 'RemoveFavorites', { favoriteInfo: formData })
        .then(function(res) {
          def.resolve(res);
        })
        .catch(function() {
          def.reject();
        });

      return def.promise;
    };
  }
}());