(function() {
  'use strict';

  angular
    .module('app')
    .service('AuthService', AuthService);

  /** @ngInject */
  function AuthService(API_URL, $http, $q) {
    this.login = function(formData) {
      var def = $q.defer();

      $http.post(API_URL + 'Login', formData)
        .then(function(res) {
          def.resolve(res);
        })
        .catch(function() {
          def.reject();
        });

      return def.promise;
    };
    
    this.forgotPassword = function(formData) {
      var def = $q.defer();

      $http.post(API_URL + 'ForgotPassword', formData)
        .then(function(res) {
          def.resolve(res);
        })
        .catch(function() {
          def.reject();
        });

      return def.promise; 
    };
    
    this.changePassword = function(formData) {
      var def = $q.defer();
      
      $http.post(API_URL + 'ChangePassword', formData)
        .then(function(res) {
          def.resolve(res);
        })
        .catch(function() {
          def.reject();
        });

      return def.promise; 
    };
  }
}());
