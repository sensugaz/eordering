(function() {
  'use strict';

  angular
    .module('app')
    .service('ConfigService', ConfigService);

  /** @ngInject */
  function ConfigService(API_URL, $http, $q) {
    this.getConfig = function() {
      var def = $q.defer();

      $http.get(API_URL + 'Config')
        .then(function(res) {
          def.resolve(res);
        })
        .catch(function() {
          def.reject();
        });

      return def.promise;
    };
  }
}());