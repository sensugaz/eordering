(function() {
  'use strict';

  angular
    .module('app')
    .service('ReportService', ReportService);

  /** @ngInject */
  function ReportService(API_URL, API_REPORT, $http, $q) {
    this.getReportMonthlyFile = function() {
      var def = $q.defer();

      $http.get(API_REPORT + 'report/monthly')
        .then(function(res) {
          def.resolve(res);
        })
        .catch(function() {
          def.reject();
        });

      return def.promise;
    };

    this.getReportMonthly = function(fileName, params) {
      var def = $q.defer();

      $http.get(API_REPORT + 'report/monthly/' + fileName, { params: params })
        .then(function(res) {
          def.resolve(res);
        })
        .catch(function() {
          def.reject();
        });

      return def.promise;
    };

    this.getReportQuarterFile = function() {
      var def = $q.defer();

      $http.get(API_REPORT + 'report/quarter')
        .then(function(res) {
          def.resolve(res);
        })
        .catch(function() {
          def.reject();
        });

      return def.promise;
    };

    this.getReportQuarter = function(fileName, params) {
      var def = $q.defer();

      $http.get(API_REPORT + 'report/quarter/' + fileName, { params: params })
        .then(function(res) {
          def.resolve(res);
        })
        .catch(function() {
          def.reject();
        });

      return def.promise;
    };

    this.getReportSalesmanFile = function() {
      var def = $q.defer();

      $http.get(API_REPORT + 'report/salesman')
        .then(function(res) {
          def.resolve(res);
        })
        .catch(function() {
          def.reject();
        });

      return def.promise;
    };

    this.getReportSalesman = function(fileName, params) {
      var def = $q.defer();

      $http.get(API_REPORT + 'report/salesman/' + fileName, { params: params })
        .then(function(res) {
          def.resolve(res);
        })
        .catch(function() {
          def.reject();
        });

      return def.promise;
    };

    this.getReportManagerFile = function() {
      var def = $q.defer();

      $http.get(API_REPORT + 'report/manager')
        .then(function(res) {
          def.resolve(res);
        })
        .catch(function() {
          def.reject();
        });

      return def.promise;
    };

    this.getReportManager = function(fileName, params) {
      var def = $q.defer();

      $http.get(API_REPORT + 'report/manager/' + fileName, { params: params })
        .then(function(res) {
          def.resolve(res);
        })
        .catch(function() {
          def.reject();
        });

      return def.promise;
    };

    this.getReportBISales = function(fileName, params) {
      var def = $q.defer();

      $http.get(API_REPORT + 'report/biSales', { params: params })
        .then(function(res) {
          def.resolve(res);
        })
        .catch(function() {
          def.reject();
        });

      return def.promise;
    };

    this.getReportBIManager = function(fileName, params) {
      var def = $q.defer();

      $http.get(API_REPORT + 'report/biManager', { params: params })
        .then(function(res) {
          def.resolve(res);
        })
        .catch(function() {
          def.reject();
        });

      return def.promise;
    };
    /*
    this.getReportComissionOnSale = function(params) {
      var def = $q.defer();
  
        $http.get(API_URL + 'report/CommissionOnSale', { params: params })
          .then(function(res) {
            def.resolve(res);
          })
          .catch(function() {
            def.reject();
          });
  
        return def.promise;
    }
    */
   this.getReportComissionOnSaleFilter = function() {
    var def = $q.defer();
  
    $http.get(API_URL + 'report/CommissionOnSale')
      .then(function(res) {
        def.resolve(res);
      })
      .catch(function() {
        def.reject();
      });

    return def.promise;
   }

   this.getReportComissionOnSaleForSale = function(formData) {
    var def = $q.defer();
  
    $http.post(API_URL + 'report/CommissionOnSale/sales', formData)
      .then(function(res) {
        def.resolve(res);
      })
      .catch(function() {
        def.reject();
      });

    return def.promise;
   }
   
   this.getReportComissionOnSaleForManager = function(formData) {
    var def = $q.defer();
  
    $http.post(API_URL + 'report/CommissionOnSale/manager', formData)
      .then(function(res) {
        def.resolve(res);
      })
      .catch(function() {
        def.reject();
      });

    return def.promise;
   }
  }
}());
