(function() {
  'use strict';

  angular
    .module('app')
    .service('ProductService', ProductService);

  /** @ngInject */
  function ProductService(API_URL, $http, $q) {
    this.getProduct = function(params) {
      var def = $q.defer();
      
      $http.get(API_URL + 'Product', { params: params })
        .then(function(res) {
          def.resolve(res);
        })
        .catch(function() {
          def.reject();
        });

      return def.promise;
    };

    this.getOneProduct = function(customerId, btfCode) {
      var def = $q.defer();

      $http.get(API_URL + 'ProductInBTF?customerId=' + customerId + '&btf=' + btfCode)
        .then(function(res) {
          def.resolve(res);
        })
        .catch(function() {
          def.reject();
        });

      return def.promise;
    };

    this.getBTFW = function(customerId) {
      var def = $q.defer();

      $http.get(API_URL + 'BTFW?customerId=' + customerId)
        .then(function(res) {
          def.resolve(res);
        })
        .catch(function() {
          def.reject();
        });

      return def.promise;
    };
  }

}());
