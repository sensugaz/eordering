(function() {
  'use strict';

  angular
    .module('app')
    .service('PromotionService', PromotionService);

  /** @ngInject */
  function PromotionService(API_URL, $http, $q) {
    this.chkPromotionValidate = function(formData) {
      var def = $q.defer();

      $http.post(API_URL + 'PromotionValidate', formData)
        .then(function(res) {
          def.resolve(res);
        })
        .catch(function() {
          def.reject();
        });

      return def.promise;
    }

    this.getPromotion = function(customerId, marketingCodeList) {
      var def = $q.defer();

      $http.get(API_URL + 'Promotion', { params: { customerId: customerId, marketingCodeList: marketingCodeList }})
        .then(function(res) {
          def.resolve(res);
        })
        .catch(function() {
          def.reject();
        });

      return def.promise;
    };

    this.getPromotionInfo = function(promotionId) {
      var def = $q.defer();

      $http.get(API_URL + 'PromotionInfo', { params: { promotionId: promotionId }})
        .then(function(res) {
          def.resolve(res);
        })
        .catch(function() {
          def.reject();
        });

      return def.promise;
    };

    this.getPromotionInBTF = function(customerId, btf) {
      var def = $q.defer();

      $http.get(API_URL + 'PromotionInBTF', { params: { customerId: customerId, btf: btf }})
        .then(function(res) {
          def.resolve(res);
        })
        .catch(function() {
          def.reject();
        });

      return def.promise;
    };

    this.getPromotionOrderCount = function(promotionId, customerId)  {
      var def = $q.defer();

      $http.get(API_URL + 'OrderCountDocument/GetOrderCountDocument', { params: { promotionId: promotionId, customerId: customerId }})
        .then(function(res) {
          def.resolve(res);
        })
        .catch(function() {
          def.reject();
        });

      return def.promise;
    }
  }

}());
