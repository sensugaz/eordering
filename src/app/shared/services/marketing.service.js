(function() {
  'use strict';

  angular
    .module('app')
    .service('MarketingService', MarketingService);

  /** @ngInject */
  function MarketingService(API_URL, $http, $q) {
    this.getMarketing = function(customerId) {
      var def = $q.defer();

      $http.get(API_URL + 'Marketing', { params: {customerId: customerId}})
        .then(function(res) {
          def.resolve(res);
        })
        .catch(function() {
          def.reject();
        });

      return def.promise;
    };
  }

}());
