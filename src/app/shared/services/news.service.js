(function() {
  'use strict';

  angular
    .module('app')
    .service('NewsService', NewsService);

  /** @ngInject */
  function NewsService(API_URL, $http, $q) {
    this.getNews = function() {
      var def = $q.defer();

      $http.get(API_URL + 'News')
        .then(function(res) {
          def.resolve(res);
        })
        .catch(function() {
          def.reject();
        });

      return def.promise;
    };
  }

}());
