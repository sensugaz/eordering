(function() {
  'use strict';

  angular
    .module('app')
    .service('CustomerService', CustomerService);

  /** @ngInject */
  function CustomerService(API_URL, $http, $q) {
    this.getCustomer = function(userId) {
      var def = $q.defer();

      $http.get(API_URL + 'Customer?userId=' + userId)
        .then(function(res) {
          def.resolve(res);
        })
        .catch(function() {
          def.reject();
        });

      return def.promise;
    };

    this.getOneCustomer = function(customerId) {
      var def = $q.defer();

      $http.get(API_URL + 'CustomerInfo?customerId=' + customerId)
        .then(function(res) {
          def.resolve(res);
        })
        .catch(function() {
          def.reject();
        });

      return def.promise;
    }

    this.customerUpdate = function(formData) {
      var def = $q.defer();

      $http.post(API_URL + 'CustomerUpdate', { customer: formData })
        .then(function(res) {
          def.resolve(res);
        })
        .catch(function() {
          def.reject();
        });

      return def.promise;
    };

    this.customerProblem = function(formData) {
      var def = $q.defer();

      $http.post(API_URL + 'CustomerProblem', { problem: formData })
        .then(function(res) {
          def.resolve(res);
        })
        .catch(function() {
          def.reject();
        });

      return def.promise;
    };

    this.customerBlock = function(customerCode) {
      var def = $q.defer();

      $http.get(API_URL + 'CustomerBlock?customercode=' + customerCode)
        .then(function(res) {
          def.resolve(res);
        })
        .catch(function() {
          def.reject();
        });

      return def.promise;
    }
  }

}());
