(function() {
  'use strict';

  angular
    .module('app')
    .filter('uniqueFilter', UniqueFilter);

  function UniqueFilter() {
    function fn(collection, keyname) {
      var output = [];
      var keys = [];

      angular.forEach(collection, function(item) {
        var key = item[keyname];
        if(keys.indexOf(key) === -1) {
          keys.push(key); 
          output.push(item);
        }
      });

      return output;
    }

    return fn;    
  }

}());
