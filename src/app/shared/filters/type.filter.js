(function() {
  'use strict';

  angular
    .module('app')
    .filter('typeFilter', typeFilter);

  function typeFilter() {
    return function(inputArray, filterValue) {
      if (filterValue.length) {
        return inputArray.filter(function(entry) {
          return this.indexOf(entry.marketingCode) !== -1;
        }, filterValue);
      } else {
        return inputArray;
      }
    };
  }

}());
