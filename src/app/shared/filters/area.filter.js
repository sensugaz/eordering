(function() {
  'use strict';

  angular
    .module('app')
    .filter('areaFilter', areaFilter);

  function areaFilter() {
    return function(items, optional1, optional2) {
      var output;

      if (optional1 != undefined && optional1 != 'ALL') {
        output = items.filter(function(value) {
          return value.sales_group.includes(optional1);
        });
      } else {
        output = items;
      }

      if (optional2 != undefined) {
        output = output.filter(function(value) {
          return value.commission_type.includes(optional2);
        });
      }

      return output;
    };
  }

}());