(function() {
    'use strict';
  
    angular
      .module('app')
      .filter('quarterFilter', quarterFilter);
  
    function quarterFilter() {
      return function(items, optional1) {
        var output;
  
        if (optional1 != undefined && optional1 != 'ALL') {
          output = items.filter(function(value) {
            return value.month.includes(optional1);
          });
        } else {
          output = items;
        }
  
        return output;
      };
    }
  
  }());