(function() {
    'use strict';
  
    angular
      .module('app')
      .filter('area2Filter', area2Filter);
  
    function area2Filter() {
      return function(items, optional1) {
        var output;
  
        if (optional1 != undefined && optional1 != 'ALL') {
          output = items.filter(function(value) {
            return value.salesGroupKey.includes(optional1);
          });
        } else {
          output = items;
        }
  
        return output;
      };
    }
  
  }());