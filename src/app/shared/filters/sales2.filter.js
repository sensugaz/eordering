(function() {
    'use strict';
  
    angular
      .module('app')
      .filter('sales2Filter', sales2Filter);
  
    function sales2Filter() {
      return function(items, optional1) {
        var output;
  
        if (optional1 != undefined && optional1 != 'ALL') {
          output = items.filter(function(value) {
            return value.areaCode.includes(optional1);
          });
        } else {
          output = items;
        }
  
        return output;
      };
    }
  
  }());