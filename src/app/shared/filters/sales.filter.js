(function() {
  'use strict';

  angular
    .module('app')
    .filter('salesFilter', salesFilter);

  function salesFilter() {
    return function(items, optional1) {
      var output;

      if (optional1 != undefined && optional1 != 'ALL') {
        output = items.filter(function(value) {
          return value.area_no.includes(optional1);
        });
      } else {
        output = items;
      }

      return output;
    };
  }

}());