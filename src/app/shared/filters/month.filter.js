(function() {
    'use strict';
  
    angular
      .module('app')
      .filter('monthFilter', monthFilter);
  
    function monthFilter() {
      return function(items, optional1) {
        var output;
  
        if (optional1 != undefined && optional1 != 'ALL') {
          output = items.filter(function(value) {
            return value.quarter.includes(optional1);
          });
        } else {
          output = items;
        }
  
        return output;
      };
    }
  
  }());