(function() {
  'use strict';

  angular
    .module('app')
    .config(config)
    .config(configRecaptcha);

  /** @ngInject */
  function config($compileProvider, $logProvider, toastrConfig) {
    $compileProvider.debugInfoEnabled(false);

    // Enable log
    $logProvider.debugEnabled(true);

    // Set options third-party lib
    toastrConfig.allowHtml = true;
    toastrConfig.timeOut = 3000;
    toastrConfig.positionClass = 'toast-top-right';
    toastrConfig.preventDuplicates = true;
    toastrConfig.progressBar = true;
  }

  function configRecaptcha(vcRecaptchaServiceProvider) {
    vcRecaptchaServiceProvider.setDefaults({
      key: '6LeL1jsUAAAAAM3sNHa-tx84kUxdVcgojpaoYR6H',
      lang: 'th',
      size: 'normal'
    })
  }
})();
